package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strconv"
	"sync"
	"time"
)

// coord represents a coordinate in the maze.  i and j are the row and
// column respectively.
type coord struct {
	i, j int
}

// dir represents a direction.  i and j are added to the respective
// fields in a coord.
type dir coord

// maze contains everything needed to track the current state of a maze.
type maze struct {
	// rows and cols are requested maze size (not the dimensions of grid).
	rows, cols int

	// ngrs is the number of goroutines used when creating and solving
	// the maze.
	ngrs int

	// grid represents the maze itself.  It is rows*2+3 x cols*2+3 in order
	// to handle the double size due to walls/corridors, and has extra to
	// hold a sentinel corridor around the entire boundary.  Even rows/cols
	// are corridors and odd rows/cols are walls.  0:empty 1:wall 2:filled
	// 4:solution
	grid [][]int8

	// coords is used to hold a list of coordinates while creating and
	// solving.  While creating, it is a list of coordinates that have been
	// cleared.  When starting a new path a coord is picked at random from
	// coords.  This avoids many of the pitfalls of the normal backtracking
	// maze generation algorithm.  Once there are no available directions to
	// move from a coord it is removed from paths.  While solving, coords
	// is a list of dead ends.  When starting to fill a dead end is picked
	// at random from coords.
	coords []coord

	// coordsOut and coordsIn serialize access to coords.  New coords to be
	// added are written to coordsIn.  Random coords are written to coordsOut.
	// These transactions are handled by pathsHandler while creating and
	// deadEndsHandler while solving.
	coordsOut chan coord
	coordsIn  chan coord

	// Reqs is a channel of chomp/fill requests sent from goroutines to
	// the coordsHandler depending on phase.
	reqs chan req

	// ticker is used to time output.  Every 1/fps seconds ticker fires and
	// a single frame from a single goroutine is printed.
	ticker *time.Ticker

	// writer wraps os.Stdout to enable more efficient buffered output.
	writer *bufio.Writer
}

// req is a chomp/fill request from a goroutine.  It contains the coord
// to chomp/fill depending on phase and the channel to send the chomp/fill
// response back over.
type req struct {
	c    coord
	resp chan resp
}

// resp is a response from coordHandler.  It holds the return values
// from a call to chomp()/fill() depending on phase.
type resp struct {
	c   coord
	err error
}

// dirs represents directions to look when printing wall characters and
// when choosing a direction to chomp.  It is an array, not a slice,
// so that a copy can easily be made for every chomp.
//
// syms maps a bitmap of neighboring wall positions to wall characters.
// The bit positions come from the dirs array above:
//
//       1
//     0 . 2
//       3
//
//     3:down 2:right 1:up 0:left
//
//     0 dot
//     1 left
//     2 up
//     3 up left
//     4 right
//     5 right left
//     6 right up
//     7 right up left
//     8 down
//     9 down left
//     a down up
//     b down up left
//     c down right
//     d down right left
//     e down right up
//     f down right up left
//
// emptySyms holds the runes used to represent different types of empty
// cells: 0:empty, 2:filled, and 4:solution.
var (
	dirs = [...]dir{
		{i: 0, j: -1}, {i: -1, j: 0}, {i: 0, j: 1}, {i: 1, j: 0},
	}
	syms = []rune{
		'·', '╴', '╵', '┘', '╶', '─', '└', '┴',
		'╷', '┐', '│', '┤', '┌', '┬', '├', '┼',
	}
	emptySyms = []rune{
		' ', 0, '░', 0, '█',
	}
)

// move returns a new coord from vector addition c+d.
func (c coord) move(d dir) coord {
	return coord{i: c.i + d.i, j: c.j + d.j}
}

// times returns a new dir from scalar multiplication d*n.
func (d dir) times(n int) dir {
	return dir{i: d.i * n, j: d.j * n}
}

// maze initializes a maze of size rows x cols, and starts a ticker to
// animate a frame every 1/fps seconds.
func newMaze(writer io.Writer, rows, cols, fps, ngrs int) *maze {
	// grid rows and cols larger to hold both walls and corridors, and a
	// sentinel corridor around the outside.
	grows := rows*2 + 3
	gcols := cols*2 + 3
	backing := make([]int8, grows*gcols)
	for i := range backing {
		backing[i] = 1
	}
	g := make([][]int8, grows)
	for i := range g {
		g[i], backing = backing[:gcols], backing[gcols:]
	}
	for i := 0; i < grows; i++ {
		g[i][0] = 0
		g[i][gcols-1] = 0
	}
	for j := 0; j < gcols; j++ {
		g[0][j] = 0
		g[grows-1][j] = 0
	}
	return &maze{
		rows:      rows,
		cols:      cols,
		ngrs:      ngrs,
		grid:      g,
		coordsOut: make(chan coord),
		coordsIn:  make(chan coord),
		reqs:      make(chan req),
		ticker:    time.NewTicker(time.Second / time.Duration(fps)),
		writer:    bufio.NewWriter(writer),
	}
}

// create spawns m.ngrs goroutines to chomp paths through the maze and
// waits for them to finish.
func (m *maze) create() {
	go m.pathsHandler()
	go m.coordHandler(m.chomp)
	m.ed()
	m.printAll()

	// Start on an even row and col, as evens will hold corridors and odds
	// will hold walls. Also avoid sentinels.
	start := coord{i: rand.Intn(m.rows)*2 + 2, j: rand.Intn(m.cols)*2 + 2}
	m.set(start, 0)
	m.coordsIn <- start

	wg := &sync.WaitGroup{}
	wg.Add(m.ngrs)
	for i := 0; i < m.ngrs; i++ {
		go func() {
			for c := range m.coordsOut {
				m.chompPath(c)
			}
			wg.Done()
		}()
	}
	wg.Wait()
	close(m.reqs) // signal coordHandler to end
	m.set(coord{i: 1, j: rand.Intn(m.cols)*2 + 2}, 0)
	m.set(coord{i: m.rows*2 + 1, j: rand.Intn(m.cols)*2 + 2}, 0)
	m.printAll()

	// clean up
	m.coords = []coord{}
	m.reqs = make(chan req)
	m.coordsOut = make(chan coord)
}

// at returns the value of m.grid at coord c.
func (m *maze) at(c coord) int {
	return int(m.grid[c.i][c.j])
}

// cup prints the ANSI CSI sequence CUP, Cursor Position, to move the
// cursor to the supplied coord.
func (m *maze) cup(c coord) {
	fmt.Fprintf(m.writer, "\033[%d;%dH", c.i+1, c.j+1)
}

// ed prints the ANSI CSI sequence ED, Erase Display, with argment 2 to
// clear the entire terminal.
func (m *maze) ed() {
	m.writer.WriteString("\033[2J")
}

// printAll prints the entire maze and flushes the output.
func (m *maze) printAll() {
	m.cup(coord{})
	for i := range m.grid {
		for j := range m.grid[i] {
			m.writer.WriteRune(m.wallRune(coord{i: i, j: j}))
		}
		if i < len(m.grid)-1 {
			m.writer.WriteRune('\n')
		}
	}
	m.writer.Flush() // ignore error
}

// wallRune returns the character that should be printed at coord c
// based on neighboring walls.
func (m *maze) wallRune(c coord) rune {
	if m.at(c)&1 == 0 {
		return emptySyms[m.at(c)]
	}
	s := 0
	for i, d := range dirs {
		s |= (m.at(c.move(d)) & 1) << uint(i) // cast for go<1.13
	}
	return syms[s]
}

// set sets the value at c to v then redraws c and its neighbors.
func (m *maze) set(c coord, v int8) {
	old := m.at(c)
	m.grid[c.i][c.j] = v
	m.cup(c)
	m.writer.WriteRune(m.wallRune(c))

	// didn't change wall vs empty, no need to check neighbors
	// happens during solving, transition 0->2 or 0->4
	if old&1 == int(v)&1 {
		return
	}

	for _, d := range dirs {
		nc := c.move(d)
		m.cup(nc)
		m.writer.WriteRune(m.wallRune(nc))
	}
}

// coordhandler listens for req on m.reqs, calls f, and returns the
// result in a resp on req.resp.  It is used to serialize access to the
// grid and output instead of locking.  During create() f is chomp().
// During solve() f is fill().
func (m *maze) coordHandler(f func(coord) (coord, error)) {
	for req := range m.reqs {
		c, err := f(req.c)
		req.resp <- resp{c: c, err: err}
	}
}

// chomp chooses a direction at random to move from c and clears a path to
// it, moving 2 spaces due to the even/odd corridor/wall setup.  It then
// waits for m.ticker to fire, flushes the output, and returns the cell
// that was chomped to.  If the direction picked results in an invalid
// move, it is removed and another direction is picked.  If no direction
// results in a valid move then chomp returns an error.  During create()
// chomp() is the only place to access m.grid and m.writer.  This access
// is serialized via coordHandler().
func (m *maze) chomp(c coord) (coord, error) {
	da := dirs    // copy
	dirs := da[:] // slice
	for len(dirs) != 0 {
		n := rand.Intn(len(dirs))
		nc := c.move(dirs[n].times(2))
		if m.at(nc) == 1 {
			m.set(nc, 0)
			m.set(c.move(dirs[n]), 0)
			<-m.ticker.C
			m.writer.Flush() // ignore error
			return nc, nil
		}
		dirs[n] = dirs[len(dirs)-1]
		dirs = dirs[:len(dirs)-1]
	}
	return c, errors.New(`cannot chomp`)
}

// pathsHandler adds coords from m.coordsIn to m.coords and sends random
// coords from m.coords over m.coordsOut.  It serializes access to coords
// to avoid locking.
func (m *maze) pathsHandler() {
	// Need to keep track of how many we have cleared so we know when to end.
	// Otherwise we deadlock with goroutines waiting to read from coordsOut.
	cleared := 0
	for cleared < m.rows*m.cols {
		// No paths, can't pick one to send, need to wait for one to come in.
		if len(m.coords) == 0 {
			m.coords = append(m.coords, <-m.coordsIn)
			cleared++
		}
		c, n := m.randCoord()
		select {
		case c := <-m.coordsIn:
			m.coords = append(m.coords, c)
			cleared++
		case m.coordsOut <- c:
			m.rmCoord(n)
		}
	}
	close(m.coordsOut) // signal goroutines to end
}

// randCoord returns a random coord from m.coords and its index
func (m *maze) randCoord() (coord, int) {
	n := rand.Intn(len(m.coords))
	return m.coords[n], n
}

// rmCoord removes m.coords[n].  It does not preserve order.
func (m *maze) rmCoord(n int) {
	m.coords[n] = m.coords[len(m.coords)-1]
	m.coords = m.coords[:len(m.coords)-1]
}

// chompPath chomps from c until it creates a dead end and cannot chomp
// anymore.  Each resulting cleared coord from chomp is sent on coordsIn.
func (m *maze) chompPath(c coord) {
	respChan := make(chan resp)
	req := req{resp: respChan, c: c}
	for {
		m.reqs <- req
		resp := <-respChan
		if resp.err != nil {
			break
		}
		m.coordsIn <- resp.c
		req.c = resp.c
	}
}

// solve spawns m.ngrs goroutines to fill dead ends in the maze and find
// a solution.
func (m *maze) solve() {
	go m.deadEndHandler()
	go m.coordHandler(m.fill)

	wg := &sync.WaitGroup{}
	wg.Add(m.ngrs)
	for i := 0; i < m.ngrs; i++ {
		go func() {
			for c := range m.coordsOut {
				m.fillPath(c)
			}
			wg.Done()
		}()
	}
	wg.Wait()

	for i := 2; i < len(m.grid)-2; i++ {
		for j := 2; j < len(m.grid[i])-2; j++ {
			c := coord{i: i, j: j}
			if m.at(c) == 0 {
				m.set(c, 4)
			}
		}
	}
	m.printAll()
	close(m.reqs) // signal coordHandler to end

	// cleanup
	m.coords = []coord{}
	m.reqs = make(chan req)
	m.coordsOut = make(chan coord)
}

// deadEndHandler finds all dead ends then sends them in a random order
// on m.coordsOut.  It serializes access to m.coords to avoid locking.
func (m *maze) deadEndHandler() {
	// even rows/cols and avoid sentinels
	for i := 2; i < len(m.grid)-2; i += 2 {
		for j := 2; j < len(m.grid[i])-2; j += 2 {
			c := coord{i: i, j: j}
			if m.at(c) != 0 {
				continue
			}
			if m.isDeadEnd(c) {
				m.coords = append(m.coords, c)
			}
		}
	}
	for len(m.coords) != 0 {
		c, n := m.randCoord()
		m.coordsOut <- c
		m.rmCoord(n)
	}
	close(m.coordsOut) // signal goroutines to end
}

// fill checks that c is a dead end, then finds which direction is open
// and marks both c and the cell in that direction as filled, filling 2
// cells because of the even/odd corridor/wall setup.  It then returns
// the next cell in that direction.  During solve() fill() is the only
// place to access m.grid and m.writer.  This access is serialized via
// coordHandler().
func (m *maze) fill(c coord) (coord, error) {
	if !m.isDeadEnd(c) {
		return coord{}, errors.New(`not dead end`)
	}
	for _, d := range dirs {
		nc := c.move(d)
		if m.at(nc) == 0 {
			m.set(c, 2)
			m.set(nc, 2)
			<-m.ticker.C
			m.writer.Flush()
			return nc.move(d), nil
		}
	}
	panic(`fill: already filled`)
}

// fillPath fills from c until it reaches a cell that is not a dead end
// and cannot fill anymore.
func (m *maze) fillPath(c coord) {
	respChan := make(chan resp)
	req := req{c: c, resp: respChan}
	for {
		m.reqs <- req
		resp := <-respChan
		if resp.err != nil {
			break
		}
		req.c = resp.c
	}
}

// isDeadEnd checks that 3 of the 4 neighbors of c are walls or filled.
func (m *maze) isDeadEnd(c coord) bool {
	n := 0
	for _, d := range dirs {
		if m.at(c.move(d)) != 0 {
			n++
		}
	}
	return n == 3
}

func main() {
	rand.Seed(time.Now().UnixNano())

	rows, _ := strconv.Atoi(os.Args[1])
	cols, _ := strconv.Atoi(os.Args[2])
	fps, _ := strconv.Atoi(os.Args[3])
	ngrs, _ := strconv.Atoi(os.Args[4])

	m := newMaze(os.Stdout, rows, cols, fps, ngrs)
	m.create()
	m.solve()
}
