#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * http://ciphersaber.gurus.org/
 *
 * n defaults to 1 if not specified (cs1)
 * set n to 20 for default cs2
 * recommended n > 1000000
 *
 * to decode:  ./cs "key" [n] < encrypted | dd bs=1 skip=10 of=plaintext 2>/dev/null
 * to encode:  { dd bs=1 count=10 if=/dev/random 2>/dev/null; cat plaintext; } | ./cs "key" [n] > encrypted
 *
 * assumes POSIX environment (CHAR_BIT == 8)
 */

#define SWAP(x, y, t) do { (t) = (x); (x) = (y); (y) = (t); } while (0)
#define MAX(a, b) ((a) > (b) ? (a) : (b))

int rwall(ssize_t (*rw)(), int fd, void *buf, size_t n)
{
	ssize_t r;
	for (r = 1; n && r > 0; buf = (char *)buf + r, n -= r)
		do r = rw(fd, buf, n);
		while (r < 0 && errno == EINTR);
	return r < 0 ? r : n;
}

int main(int argc, char **argv)
{
	unsigned char c, t, i = 0, j = 0, k[256], s[256];
	int r, w, n = 1;

	if      (argc == 3) n = atoi(argv[2]);
	else if (argc != 2) return 1;

	for (c = 0; (k[c] = argv[1][c]); c++)
		if (c >= sizeof(k) - 10)
			return 1;

	if (rwall(read , 0, k + c, 10)) return 1;
	if (rwall(write, 1, k + c, 10)) return 1;
	c += 10;

	do
		s[i] = i;
	while (++i);

	do {
		j += s[i] + k[i % c];
		SWAP(s[i], s[j], t);
	} while (++i || --n);

	for (j = w = 0; !w && !(r = rwall(read, 0, &c, 1)); w = rwall(write, 1, &c, 1)) {
		j += s[++i];
		SWAP(s[i], s[j], t);
		c ^= s[0xff & (s[i] + s[j])];
	}

	if (r < 0 || w < 0)
		return 1;

	return 0;
}
