#include <stdio.h>
#include <unistd.h>

/*
 * rc4 encryption
 *
 * supply  hex key as only argument
 * supply  plaintext  or ciphertext on stdin
 * receive ciphertext or plaintext  on stdout respectively
 */

#define SWAP(x, y, t) do { (t) = (x); (x) = (y); (y) = (t); } while (0)

int main(int argc, char **argv)
{
	unsigned char t, i = 0, j = 0, k[256], s[256], *h = k;
	int c;

	if (argc != 2)
		return 1;

	for (argv++; **argv && sscanf(*argv, "%2hhx", h); *argv += 2, h++)
		;

	if (**argv)
		return 1;

	do
		s[i] = i;
	while (++i);

	do {
		j += s[i] + k[i % (h - k)];
		SWAP(s[i], s[j], t);
	} while (++i);

	for (j = 0; read(0, &c, 1); write(1, &c, 1)) {
		j += s[++i];
		SWAP(s[i], s[j], t);
		c ^= s[0xff & (s[i] + s[j])];
	}

	return 0;
}
