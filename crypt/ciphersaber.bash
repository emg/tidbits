# 
# n defaults to 1 if not specified (cs1)
# set n to 20 for default cs2
# recommended n > 1000000
# 
# to decode:  cipher_saber "key" [n] < encrypted | dd bs=1 skip=10 of=plaintext
# to encode:  { dd bs=1 count=10 if=/dev/random; cat plaintext; } | cipher_saber "key" [n] > encrypted
# 

cipher_saber() {
	local c i j k n s t x LC_CTYPE=C

	while IFS= read -r -d '' -n 1 c; do
		((${#k[@]} == ${#1})) && break
		k+=( "$(printf %d "'$c")" )
	done <<< "$1"

	for ((i = 0; i < 10; i++)); do
		IFS= read -r -d '' -n 1 c
		k+=( "$(printf %d "'$c")" )
		printf %b "\x$(printf %x "${k[@]: -1}")"
	done

	for ((i = 0; i < 256; i++)); do
		((s[i] = i))
	done

	for ((j = 0, n = ($# > 1) ? $2 : 1; n; n--)); do
		for ((i = 0; i < 256; i++)); do
			((j += s[i] + k[i % ${#k[@]}], j &= 0xff,
			  t = s[i], s[i] = s[j], s[j] = t))
		done
	done

	((i = j = 0))
	while IFS= read -r -d '' -n 1 c; do
		printf -v c %d "'$c"
		((i++      , i &= 0xff,
		  j += s[i], j &= 0xff,
		  t = s[i], s[i] = s[j], s[j] = t,
		  k = s[(s[i] + s[j]) & 0xff],
		  c ^= k))
		printf %b "\x$(printf %x "$c")"
	done
}
