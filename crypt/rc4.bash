# rc4
#
# supply key as only argument as hex digits
# supply plaintext on stdin
# read ciphertext from stdout
#
# Example from wikipedia, key is Key, output as hex to compare
# https://en.wikipedia.org/wiki/Rc4#Test_vectors
#
# $ printf Plaintext | rc4 "$(bin_to_hex Key)" | bin_to_hex
# bbf3e840af0ad3
# 

bin_to_hex() {
	local str c LC_CTYPE=C

	if (($#)); then
		for str; do
			[[ $str ]] || printf 00
			printf %s "$str" |
			while IFS= read -r -d '' -n 1 c; do
				printf %02x "'$c"
			done
		done
	else
		while IFS= read -r -d '' -n 1 c; do
			printf %02x "'$c"
		done
	fi
}

hex_to_bin() {
	local str x LC_CTYPE=C

	if (($#)); then
		for str; do
			printf %s "$str" |
			while IFS= read -r -d '' -n 2 x; do
				[[ $x == [[:xdigit:]][[:xdigit:]] ]] || { printf "Bad hex byte <%s>(0x%02x)\n" "$x" "'$x" 1>&2; return 1; }
				printf %b "\x$x"
			done
		done
	else
		while IFS= read -r -d '' -n 2 x; do
			[[ $x == [[:xdigit:]][[:xdigit:]] ]] || { printf "Bad hex byte <%s>(0x%02x)\n" "$x" "'$x" 1>&2; return 1; }
			printf %b "\x$x"
		done
	fi
}

rc4() {
	local c i j k s t x LC_CTYPE=C

	while IFS= read -r -d '' -n 2 x; do
		((${#k[@]} == ${#1})) && break
		[[ $x == [[:xdigit:]][[:xdigit:]] ]] || { printf "Bad hex byte <%s>(0x%02x)\n" "$x" "'$x" 1>&2; return 1; }
		k+=( $((0x$x)) )
	done <<< "$1"

	for ((i = 0; i < 256; i++)); do
		((s[i] = i))
	done

	for ((j = i = 0; i < 256; i++)); do
		((j += s[i] + k[i % ${#k[@]}], j &= 0xff,
		  t = s[i], s[i] = s[j], s[j] = t))
	done

	((i = j = 0))
	while IFS= read -r -d '' -n 1 c; do
		printf -v c "%d" "'$c"
		((i++      , i &= 0xff,
		  j += s[i], j &= 0xff,
		  t = s[i], s[i] = s[j], s[j] = t,
		  k = s[(s[i] + s[j]) & 0xff],
		  c ^= k))
		printf %b "\x$(printf %x "$c")"
	done
}
