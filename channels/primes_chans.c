#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define die(e) do{ fprintf(stderr, "error %d line %d\n", (e), __LINE__); exit(1); }while(0)
#define check(s) do{ int e = (s); if (e) die(e); }while(0)

// unbuffered channel
typedef struct {
    pthread_cond_t  c;
    pthread_mutex_t m;
    void           *p;
    unsigned char   f:1;
} Chan;

Chan *make_chan(void)
{
    Chan *c = malloc(sizeof(Chan));
    if (!c)
        die(errno);

    check(pthread_cond_init (&c->c, NULL));
    check(pthread_mutex_init(&c->m, NULL));
    c->f = 0;

    return c;
}

void write_chan(Chan *c, void *p)
{
    check(pthread_mutex_lock(&c->m));
    if (c->f) { // already have a blocking reader
        c->f = 0;
        c->p = p;
        check(pthread_cond_signal(&c->c));
    } else { // need to block and wait for a reader
        c->f = 1;
        c->p = p;
    }
    check(pthread_cond_wait(&c->c, &c->m));
    check(pthread_mutex_unlock(&c->m));
}

void read_chan(Chan *c, void **p)
{
    check(pthread_mutex_lock(&c->m));
    if (c->f) { // already have a blocking writer
        c->f = 0;
        *p   = c->p;
    } else { // need to block and wait for a writer
        c->f = 1;
        check(pthread_cond_wait(&c->c, &c->m));
        *p   = c->p;
    }
    check(pthread_cond_signal(&c->c));
    check(pthread_mutex_unlock(&c->m));
}

void *generate(void *arg)
{
    Chan *c = arg;
    for (uintptr_t i = 2; i; i++)
        write_chan(c, (void *)i);
    return NULL;
}

typedef struct {
    Chan     *in;
    Chan     *out;
    uintptr_t prime;
} Arg;

void *filter(void *arg)
{
    uintptr_t i;
    Arg      *a = arg;
    for (;;) {
        read_chan(a->in, (void **)&i);
        if (i % a->prime)
            write_chan(a->out, (void *)i);
    }
    return NULL;
}

typedef struct list List;
struct list {
    List     *n;
    pthread_t t;
    Arg       a;
};

void insert(List **h)
{
    List *l = malloc(sizeof(List));
    if (!l)
        die(errno);
    l->n = *h;
    *h   = l;
}

int main(void)
{
    uintptr_t p;
    pthread_t t;
    List     *l = NULL;
    Chan     *c = make_chan();

    check(pthread_create(&t, NULL, generate, c));

    for (int i = 1; i <= 500; i++) {
        read_chan(c, (void **)&p);
        printf("%d: %"PRIu64"\n", i, p);
        fflush(stdout);
        insert(&l);
        l->a = (Arg){ c, make_chan(), p };
        c    = l->a.out;
        check(pthread_create(&l->t, NULL, filter, (void *)&l->a));
    }

    return 0;
}
