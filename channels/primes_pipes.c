#include <pthread.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

void die(char *str)
{
    perror(str);
    exit(1);
}

void *generate(void *fd)
{
    for (int i = 2; i > 0; i++)
        if (write((int)fd, &i, sizeof(i)) != sizeof(i))
            die("bad/short write in generate");
    die("overflow"); // good luck
    return NULL;
}

typedef struct {
    int in;
    int out;
    int prime;
} Arg;

void *filter(void *arg)
{
    Arg *a = arg;
    int  i;

    for (;;) {
        if (read(a->in, &i, sizeof(i)) != sizeof(i))
            die("bad/short read in filter");
        if (i % a->prime)
            if (write(a->out, &i, sizeof(i)) != sizeof(i))
                die("bad/short write in filter");
    }
    return NULL;
}

typedef struct list List;
struct list {
    List     *next;
    pthread_t thread;
    Arg       arg;
};

void insert(List **head)
{
    List *l = malloc(sizeof(List));
    if (!l)
        die("bad malloc in insert");
    l->next = *head;
    *head   = l;
}

int main(void)
{
    List *list = NULL;
    int   fd, p[2];

    if (pipe(p))
        die("bad pipe in main");
    insert(&list);
    if (pthread_create(&list->thread, NULL, generate, (void*)p[1]))
        die("failed pthread_create in main");

    for (int j = 1; j <= 500; j++) {
        int i;

        fd = p[0];
        if (read(fd, &i, sizeof(i)) != sizeof(i))
            die("bad/short read in main loop");
        printf("%d: %d\n", j, i);
        fflush(stdout);
        if (pipe(p))
            die("bad pipe in main loop");
        insert(&list);
        list->arg = (Arg){ fd, p[1], i };
        if (pthread_create(&list->thread, NULL, filter, &list->arg))
            die("failed pthread_create in main loop");
    }
    return 0;
}

