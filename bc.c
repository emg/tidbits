#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Token Token;

#define NEWSTACK (Stack){ .data = NULL, .top = 0, .cap = 0 }
typedef struct {
    Token *data;
    size_t top;
    size_t cap;
} Stack;

struct Token {
    union {
        double f;
        int    i;
        Stack  s;
    };
    int type;
};

/* single character tokens represent themselves */
enum {
    /* '(' ')' '+' '-' '*' '/' '=' */
    NUM = UCHAR_MAX + 1,
    VAR,
    UMIN, /* unary minus */
    EQ, LE, GE, NE, /* ==, <=, >=, != */
    IF, FOR, DFN, /* if, for, define */

    EXPR, /* Token.s is populated with an expression */
};

struct {
    char prec;
    char lassoc;
    unsigned char nargs;
} opinfo[] = {
    ['=' ] = { 1, 0, 2 },

    ['<' ] = { 2, 1, 2 },
    ['>' ] = { 2, 1, 2 },
    [EQ  ] = { 2, 1, 2 },
    [LE  ] = { 2, 1, 2 },
    [GE  ] = { 2, 1, 2 },
    [NE  ] = { 2, 1, 2 },

    ['+' ] = { 3, 1, 2 },
    ['-' ] = { 3, 1, 2 },

    ['*' ] = { 4, 1, 2 },
    ['/' ] = { 4, 1, 2 },
    ['%' ] = { 4, 1, 2 },

    ['!' ] = { 5, 0, 1 },
    [UMIN] = { 5, 0, 1 },
};

typedef struct {
    char *word;
    int type;
} Keyword;
Keyword keywords[] = {
    { "if"    , IF  },
    { "for"   , FOR },
    { "define", DFN },

    { NULL, 0 }
};

Stack vars[26];
Stack tokbuf;

_Noreturn void err(int status, char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    exit(status);
}

void push(Stack *s, Token t)
{
    if (s->top == s->cap) {
        if (!(s->data = realloc(s->data, sizeof(*s->data) * (s->cap * 2 + 1))))
            err(errno, "realloc: %s\n", strerror(errno));
        s->cap = s->cap * 2 + 1;
    }
    s->data[s->top++] = t;
}

Token pop(Stack *s)
{
    if (!s->top)
        err(3, "pop: empty stack\n");
    return s->data[--s->top];
}

Token peek(Stack *s)
{
    if (!s->top)
        err(3, "peek: empty stack\n");
    return s->data[s->top - 1];
}

int empty(Stack *s)
{
    return !s->top;
}

void freestack(Stack *s)
{
    for (size_t i = 0; i < s->top; i++)
        if (s->data[i].type == '{')
            freestack(&s->data[i].s);
    free(s->data);
}

double getnum(FILE *f)
{
    double d;
    if (!fscanf(f, "%lf", &d))
        err(2, "bad number\n");
    return d;
}

int peekc(FILE *f)
{
    int c = getc(f);
    ungetc(c, f);
    return c;
}

void unlex(Token t)
{
    push(&tokbuf, tok);
}

Token lex(FILE *f)
{
    int c;

    if (!empty(&tokbuf))
        return pop(&tokbuf);

    while ((c = getc(f)) != EOF && isblank(c))
        ;
    if (c == EOF || c == '\n' || c == ';')
        return (Token){ .type = c };
    if (isdigit(c)) {
        ungetc(c, f);
        return (Token){ .f = getnum(f), .type = NUM };
    }
    if (strchr("=<>!", c) && peekc(f) == '=') {
        getc(f);
        switch (c) {
        case '=': return (Token){ .type = EQ };
        case '<': return (Token){ .type = LE };
        case '>': return (Token){ .type = GE };
        case '!': return (Token){ .type = NE };
        }
    }
    if (strchr("+-*/%()<>=!{},", c))
        return (Token){ .type = c };

    if (isalpha(c) && isalpha(peekc(f))) {
        char word[7] = { 0 }, *p = word; /* longest keyword is 6 characters */
        do {
            if (p >= word + sizeof(word) - 1)
                err(2, "bad/long keyword\n");
            *p++ = c;
        } while (isalpha(c = getc(f)));
        ungetc(c, f);
        *p = '\0';

        for (Keyword *k = keywords; k->word; k++)
            if (!strcmp(word, k->word))
                return (Token){ .type = k->type };
        err(2, "bad token: %s\n", word);
    }
    if (islower(c))
        return (Token){ .type = VAR, .i = c - 'a' };
    err(2, "bad token: %c (%d)\n", c, c);
}

double valf(Token t)
{
    switch (t.type) {
    default : err(2, "that's... wrong\n");
    case NUM: return t.f;
    case VAR: return empty(&vars[t.i]) ? 0 : peek(&vars[t.i]).f;
    }
}

void doop(Stack *vals, Token op)
{
    Token a, b;
    double af, bf;

    if (op.type == FN) {
        Stack *args = &op.s.data[0].s;

        if (vals->top < args->size)
            err(2, "missing function arguments\n");

        for (int i = args->size - 1; i >= 0; i--)
            push(&vars[args->data[i].i], pop(vals));
        af = eval(&op.s.data[1].s);
        for (int i = args->size - 1; i >= 0; i--)
            pop(&vars[args->data[i].i]);

        push(vals, (Token){ .type = NUM, .f = eval(&op.s.data[1].s) });
        return;
    }

    if (vals->top < opinfo[op.type].nargs)
        err(2, "missing values\n");

    b = pop(vals);
    bf = valf(b);

    if (opinfo[op.type].nargs == 2) {
        a = pop(vals);
        af = valf(a);
    }

    switch (op.type) {
        case '!' : push(vals, (Token){ .type = NUM, .f = !bf      }); break;
        case UMIN: push(vals, (Token){ .type = NUM, .f = -bf      }); break;

        case '%' : push(vals, (Token){ .type = NUM, .f = fmod(af, bf)}); break;
        case '*' : push(vals, (Token){ .type = NUM, .f = af *  bf }); break;
        case '/' : push(vals, (Token){ .type = NUM, .f = af /  bf }); break;

        case '+' : push(vals, (Token){ .type = NUM, .f = af +  bf }); break;
        case '-' : push(vals, (Token){ .type = NUM, .f = af -  bf }); break;

        case '>' : push(vals, (Token){ .type = NUM, .f = af >  bf }); break;
        case '<' : push(vals, (Token){ .type = NUM, .f = af <  bf }); break;
        case EQ  : push(vals, (Token){ .type = NUM, .f = af == bf }); break;
        case NE  : push(vals, (Token){ .type = NUM, .f = af != bf }); break;
        case GE  : push(vals, (Token){ .type = NUM, .f = af >= bf }); break;
        case LE  : push(vals, (Token){ .type = NUM, .f = af <= bf }); break;

        case '=' : if (a.type != VAR)
                       err(2, "must assign to a variable\n");
                   if (!empty(&vars[a.i]))
                       pop(&vars[a.i]);
                   push(&vars[a.i], (Token){ .f = bf });
                   push(vals, (Token){ .type = NUM, .f = bf       }); break;
    }
}

/* return last token type read */
int expr(FILE *f, Stack *out)
{
    Token tok, last = { .type = EOF };
    Stack ops = NEWSTACK;

    while ((tok = lex(f)).type != EOF && tok.type != '\n' && tok.type != ';' && tok.type != '}') {
        switch (tok.type) {
        case VAR: /* fall through */
        case NUM: push( out, tok); break;
        case '(': push(&ops, tok); break;
        case ')':
            if (last.type == '(')
                err(2, "empty parens\n");
            while (!empty(&ops) && peek(&ops).type != '(')
                push(out, pop(&ops));

            /* missing open paren, may be illegal or used for if (...) */
            if (empty(&ops))
                goto break2;
            pop(&ops);
            break;

        default :
            if (!opinfo[tok.type].prec)
                err(2, "invalid token in expression\n");
            if (tok.type == '-' && (last.type == EOF || opinfo[last.type].prec))
                tok.type = UMIN;
            while (!empty(&ops) &&
                    (( opinfo[tok.type].lassoc && opinfo[tok.type].prec <= opinfo[peek(&ops).type].prec) ||
                     (!opinfo[tok.type].lassoc && opinfo[tok.type].prec <  opinfo[peek(&ops).type].prec)))
                push(out, pop(&ops));
            push(&ops, tok);
            break;
        }
        last = tok;
    }
break2:

    if (empty(out) && empty(&ops)) /* empty expression */
        return tok.type;

    while (!empty(&ops))
        if (peek(&ops).type == '(')
            err(2, "missing )\n");
        else
            push(out, pop(&ops));

    freestack(&ops);
    return tok.type;
}

int pushexpr(FILE *f, Stack *out)
{
    Stack buf = NEWSTACK;
    int i = expr(f, &buf);
    push(out, (Token){ .type = EXPR, .s = buf });
    return i;
}

Token parse(FILE *f, Stack *out); /* FIXME: declarations at top of file */

void pushstatement(FILE *f, Stack *out)
{
    Stack buf = NEWSTACK;
    Token t = parse(f, &buf);
    push(out, (Token){ .type = t.type, .s = buf });
}

/* returned Token.type is the type parsed
 * returned Token.i is the last token type read
 * that is just absolutely horrible...
 * I really should differentiate between tokens and statements... */
Token parse(FILE *f, Stack *out)
{
    Token tok, t;

    while ((tok = lex(f)).type == '\n')
        ;

    switch (tok.type) {
    case EOF: /* fall through */
    case '}': return (Token){ .type = tok.type, .i = tok.type };

    case '{':
        while ((t = parse(f, &tok.s)).i != '}' && t.i != EOF)
            ;
        if (t.i == EOF)
            err(2, "no closing }\n");
        push(out, tok);
        return (Token){ .type = '{', .i = t.i };

    case IF :
        if (lex(f).type         != '(') err(2, "no test for if statement\n");
        if (pushexpr(f, &tok.s) != ')') err(2, "no closing ) for if test\n");
        pushstatement(f, &tok.s);
        push(out, tok);
        return (Token){ .type = IF, .i = tok.type };

    case FOR:
        if (lex(f).type         != '(') err(2, "missing for expressions\n");
        if (pushexpr(f, &tok.s) != ';') err(2, "malformed for expressions\n");
        if (pushexpr(f, &tok.s) != ';') err(2, "malformed for expressions\n");
        if (pushexpr(f, &tok.s) != ')') err(2, "malformed for expressions\n");
        pushstatement(f, &tok.s);
        push(out, tok);
        return (Token){ .type = FOR, .i = tok.type };

    case DFN:
        if ((t = lex(f)).type != VAR) err(2, "missing function name\n");
        if (lex(f).type       != '(') err(2, "missing argument list\n");

    default : /* single expression */
        unlex(tok);
        return (Token){ .type = EXPR, .i = pushexpr(f, out) };
    }
}

double eval(Stack *expr, int print)
{
    Stack vals = NEWSTACK;
    double f = 0;

    /* only print if the top level expression wasn't an assignment */
    print = print && peek(expr).type != '=';

    for (Token *t = expr->data; t < expr->data + expr->top; t++) {
        switch (t->type) {
        case IF  :
            if (eval(&t->s.data[0].s, 0))
                eval(&t->s.data[1].s, 1);
            break;

        case FOR :
            for (eval(&t->s.data[0].s, 0); eval(&t->s.data[1].s, 0); eval(&t->s.data[2].s, 0))
                eval(&t->s.data[3].s, 1);
            break;

        case '{' : /* fall through */
        case EXPR: eval(&t->s, 1); break;

        case VAR : /* fall through */
        case NUM : push(&vals, *t); break;

        default  : doop(&vals, *t); break;
        }
    }

    if (!empty(&vals)) { /* just evaled a single expression */
        if (vals.top != 1)
            err(2, "invalid expression\n"); /* validate during parsing to avoid this? */
        if (peek(&vals).type != VAR && peek(&vals).type != NUM)
            err(2, "result of expression is not a variable or number\n"); /* impossible right? */

        f = valf(pop(&vals));
        freestack(&vals);

        if (print)
            printf("%f\n", f);
    }
    return f;
}

int main(void)
{
    Stack prog = NEWSTACK;
    while (parse(stdin, &prog).i != EOF) {
        if (!empty(&prog))
            eval(&prog, 1);
        prog = NEWSTACK;
    }
    freestack(&prog);
    return 0;
}
