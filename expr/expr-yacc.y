%{
#include <inttypes.h>
#include <locale.h>
#include <regex.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define die(e, ...) do{ fprintf(stderr, __VA_ARGS__); exit(e); }while(0)

#define YYSTYPE Val
typedef struct {
    char    *s;
    intmax_t n;
} Val;

int  yylex  (void);
void yyerror(char*);

int    intlen;
char **args;

char *valstr(Val val, char *buf)
{
    char *p = val.s;
    if (!p) sprintf(p = buf, "%"PRIdMAX, val.n);
    return p;
}

int valcmp(Val a, Val b)
{
    if (!a.s && !b.s)
        return (a.n > b.n) - (a.n < b.n);

    char b1[intlen], *p = valstr(a, b1);
    char b2[intlen], *q = valstr(b, b2);

    return strcoll(p, q);
}

Val match(Val vstr, Val vregx)
{
    char b1[intlen], *str  = valstr(vstr , b1);
    char b2[intlen], *regx = valstr(vregx, b2);

    regex_t    re;
    regmatch_t matches[2];
    char       anchreg[strlen(regx) + 2];

    sprintf(anchreg, "^%s", regx);

    if (regcomp(&re, anchreg, 0))
        die(3, "regcomp failed");

    if (regexec(&re, str, 2, matches, 0))
        return (Val){ (re.re_nsub ? "" : NULL), 0 };

    if (re.re_nsub) {
        char    *ret, *p;
        regoff_t len = matches[1].rm_eo - matches[1].rm_so + 1;

        if (!(ret = malloc(len))) // FIXME: free
            die(3, "malloc failed");

        strncpy(ret, str + matches[1].rm_so, len);
        ret[len - 1] = '\0'; // strlcpy would be nice

        intmax_t d = strtoimax(ret, &p, 10);
        if (*ret && !*p)
            return (Val){ NULL, d };
        return (Val){ ret, 0 };
    }
    return (Val){ NULL, matches[0].rm_eo - matches[0].rm_so };
}

void num(Val v)
{
    if (v.s)
        die(2, "expected integer, got `%s'\n", v.s);
}
%}
%token VAL GE LE NE

%left '|'
%left '&'
%left '=' '>' GE '<' LE NE
%left '+' '-'
%left '*' '/' '%'
%left ':'
%%
prog: expr { if (*--args) yyerror("syntax error");
             if ($1.s) printf("%s\n"        , $1.s);
             else      printf("%"PRIdMAX"\n", $1.n);
             return !(($1.s && *$1.s) || $1.n); }
    ;

expr: VAL
    | expr '|' expr { if      ( $1.s && *$1.s) $$ = (Val){ $1.s,    0 };
                      else if (!$1.s &&  $1.n) $$ = (Val){ NULL, $1.n };
                      else if ( $3.s && *$3.s) $$ = (Val){ $3.s,    0 };
                      else                     $$ = (Val){ NULL, $3.n }; }

    | expr '&' expr { if ((($1.s && *$1.s) || $1.n) &&
                          (($3.s && *$3.s) || $3.n)) $$ = $1;
                      else $$ = (Val){ NULL, 0 }; }

    | expr '=' expr { $$ = (Val){ NULL, valcmp($1, $3) == 0 }; }
    | expr '>' expr { $$ = (Val){ NULL, valcmp($1, $3) >  0 }; }
    | expr GE  expr { $$ = (Val){ NULL, valcmp($1, $3) >= 0 }; }
    | expr '<' expr { $$ = (Val){ NULL, valcmp($1, $3) <  0 }; }
    | expr LE  expr { $$ = (Val){ NULL, valcmp($1, $3) <= 0 }; }
    | expr NE  expr { $$ = (Val){ NULL, valcmp($1, $3) != 0 }; }

    | expr '+' expr { num($1); num($3); $$ = (Val){ NULL, $1.n + $3.n }; }
    | expr '-' expr { num($1); num($3); $$ = (Val){ NULL, $1.n - $3.n }; }
    | expr '*' expr { num($1); num($3); $$ = (Val){ NULL, $1.n * $3.n }; }
    | expr '/' expr { num($1); num($3); $$ = (Val){ NULL, $1.n / $3.n }; }
    | expr '%' expr { num($1); num($3); $$ = (Val){ NULL, $1.n % $3.n }; }

    | expr ':' expr { $$ = match($1, $3); }

    | '(' expr ')'  { $$ = $2; }
    ;
%%
int main(int argc, char *argv[])
{
    setlocale(LC_ALL, "");

    if (!(intlen = snprintf(NULL, 0, "%"PRIdMAX, INTMAX_MIN) + 1))
        die(3, "failed to get max digits\n");

    args = argv + 1;
    if (!*args)
        yyerror("syntax error");
    if (!strcmp("--", *args))
        ++args;

    return yyparse();
}

int yylex(void)
{
    char *q, *p = *args++;
    if (!p)
        return 0;

    intmax_t d = strtoimax(p, &q, 10);
    if (*p && !*q) {
        yylval = (Val){ NULL, d };
        return VAL;
    }

    char *ops = "|&=><+-*/%():";
    if (*p && !p[1] && strchr(ops, *p))
        return *p;

    if (!strcmp(p, ">=")) return GE;
    if (!strcmp(p, "<=")) return LE;
    if (!strcmp(p, "!=")) return NE;

    yylval = (Val){ p, 0 };
    return VAL;
}

void yyerror(char *s)
{
    die(2, "%s\n", s);
}
