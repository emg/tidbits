#include <inttypes.h>
#include <limits.h>
#include <regex.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define die(e, ...) do{ fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); exit(e); }while(0)

enum {
    VAL = CHAR_MAX + 1, GE, LE, NE
};

typedef struct {
    char    *s;
    intmax_t n;
} Val;

char **args;
size_t intlen;
Val    yylval;

char *valstr(Val val, char *buf)
{
    char *p = val.s;
    if (!p) sprintf(p = buf, "%"PRIdMAX, val.n);
    return p;
}

Val match(Val vstr, Val vregx)
{
    char b1[intlen], *str  = valstr(vstr , b1);
    char b2[intlen], *regx = valstr(vregx, b2);

    regex_t    re;
    regmatch_t matches[2];
    char       anchreg[strlen(regx) + 2];

    sprintf(anchreg, "^%s", regx);

    if (regcomp(&re, anchreg, 0))
        die(3, "regcomp failed");

    if (regexec(&re, str, 2, matches, 0))
        return (Val){ (re.re_nsub ? "" : NULL), 0 };

    if (re.re_nsub) {
        char    *ret, *p;
        regoff_t len = matches[1].rm_eo - matches[1].rm_so + 1;

        if (!(ret = malloc(len))) // FIXME: free
            die(3, "malloc failed");

        strncpy(ret, str + matches[1].rm_so, len);
        ret[len - 1] = '\0'; // strlcpy would be nice

        intmax_t d = strtoimax(ret, &p, 10);
        if (*ret && !*p)
            return (Val){ NULL, d };
        return (Val){ ret, 0 };
    }
    return (Val){ NULL, matches[0].rm_eo - matches[0].rm_so };
}

int valcmp(Val a, Val b)
{
    if (!a.s && !b.s)
        return (a.n > b.n) - (a.n < b.n);

    char b1[intlen], *p = valstr(a, b1);
    char b2[intlen], *q = valstr(b, b2);

    return strcmp(p, q);
}

void num(Val v)
{
    if (v.s)
        die(2, "syntax error: expected integer got `%s'", v.s);
}

// otop points to one past last op
// vtop points to one past last val
// guaranteed otop != ops
// pop two vals, pop op, apply op, push val
void doop(int *ops, int **otop, Val *vals, Val **vtop)
{
    if ((*otop)[-1] == '(')
        die(2, "syntax error: extra (");
    if (*vtop - vals < 2)
        die(2, "syntax error: missing expression or extra operator");

    Val ret, a = (*vtop)[-2], b = (*vtop)[-1];
    int op = (*otop)[-1];

    switch (op) {
        case '|': if      ( a.s && *a.s) ret = (Val){ a.s ,   0 };
                  else if (!a.s &&  a.n) ret = (Val){ NULL, a.n };
                  else if ( b.s && *b.s) ret = (Val){ b.s ,   0 };
                  else                   ret = (Val){ NULL, b.n };
                  break;

        case '&': if (((a.s && *a.s) || a.n) &&
                      ((b.s && *b.s) || b.n)) ret = a;
                  else                        ret = (Val){ NULL, 0 };
                  break;

        case '=': ret = (Val){ NULL, valcmp(a, b) == 0 }; break;
        case '>': ret = (Val){ NULL, valcmp(a, b) >  0 }; break;
        case GE : ret = (Val){ NULL, valcmp(a, b) >= 0 }; break;
        case '<': ret = (Val){ NULL, valcmp(a, b) <  0 }; break;
        case LE : ret = (Val){ NULL, valcmp(a, b) <= 0 }; break;
        case NE : ret = (Val){ NULL, valcmp(a, b) != 0 }; break;

        case '+': num(a); num(b); ret = (Val){ NULL, a.n + b.n }; break;
        case '-': num(a); num(b); ret = (Val){ NULL, a.n - b.n }; break;
        case '*': num(a); num(b); ret = (Val){ NULL, a.n * b.n }; break;
        case '/': num(a); num(b); ret = (Val){ NULL, a.n / b.n }; break;
        case '%': num(a); num(b); ret = (Val){ NULL, a.n % b.n }; break;

        case ':': ret = match(a, b); break;
    }

    (*vtop)[-2] = ret;
    (*otop)--;
    (*vtop)--;
}

int yyparse(int argc)
{
    Val vals[argc], *vtop = vals;
    int ops [argc], *otop = ops;

    char prec[] = {
        ['|'] = 1,
        ['&'] = 2,
        ['='] = 3, ['>'] = 3, [GE] = 3, ['<'] = 3, [LE] = 3, [NE] = 3,
        ['+'] = 4, ['-'] = 4,
        ['*'] = 5, ['/'] = 5, ['%'] = 5,
        [':'] = 6,
    };

    int type, last = 0;
    while ((type = yylex())) {
        switch (type) {
            case VAL: *vtop++ = yylval; break;
            case '(': *otop++ = '('   ; break;
            case ')':
                if (last == '(')
                    die(2, "syntax error: empty ( )");
                while (otop > ops && otop[-1] != '(')
                    doop(ops, &otop, vals, &vtop);
                if (otop == ops)
                    die(2, "syntax error: extra )");
                otop--;
                break;
            default :
                if (prec[last])
                    die(2, "syntax error: extra operator");
                while (otop > ops && prec[otop[-1]] >= prec[type])
                    doop(ops, &otop, vals, &vtop);
                *otop++ = type;
                break;
        }
        last = type;
    }
    while (otop > ops)
        doop(ops, &otop, vals, &vtop);

    if (vtop == vals)
        die(2, "syntax error: missing expression");
    if (vtop - vals > 1)
        die(2, "syntax error: extra expression");

    vtop--;
    if (vtop->s) printf("%s\n"        , vtop->s);
    else         printf("%"PRIdMAX"\n", vtop->n);

    return (vtop->s && *vtop->s) || vtop->n;
}

int yylex(void)
{
    char *q, *p = *args++;
    if (!p)
        return 0;

    intmax_t d = strtoimax(p, &q, 10);
    if (*p && !*q) {
        yylval = (Val){ NULL, d };
        return VAL;
    }

    char *ops = "|&=><+-*/%():";
    if (*p && !p[1] && strchr(ops, *p))
        return *p;

    if (!strcmp(p, ">=")) return GE;
    if (!strcmp(p, "<=")) return LE;
    if (!strcmp(p, "!=")) return NE;

    yylval = (Val){ p, 0 };
    return VAL;
}

int main(int argc, char **argv)
{
    if (!(intlen = snprintf(NULL, 0, "%"PRIdMAX, INTMAX_MIN) + 1))
        die(3, "failed to get max digits\n");

    args = argv + 1;
    if (*args && !strcmp("--", *args))
        ++args;

    return !yyparse(argc);
}
