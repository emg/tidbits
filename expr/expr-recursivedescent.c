/*
 * or: and
 *     expr | and
 *
 * and: rel
 *      and & rel
 *
 * rel: add
 *      rel  = add
 *      rel >  add
 *      rel >= add
 *      rel <  add
 *      rel <= add
 *      rel != add
 *
 * add: mul
 *      add + mul
 *      add - mul
 *
 * mul: match
 *      mul * match
 *      mul / match
 *      mul % match
 *
 * match: val
 *        match : val
 *
 * val: VAL
 *      ( expr )
 */

/* FIXME: still very much broken */

#include <inttypes.h>
#include <limits.h>
#include <regex.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define die(e, ...) do{ fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); exit(e); }while(0)

enum {
    VAL = CHAR_MAX + 1, GE, LE, NE, EOA,
};

typedef struct {
    char    *s;
    intmax_t n;
} Val;
int type;
Val yylval;
size_t intlen;
char **args;

Val regmatch(Val, Val);
int valcmp(Val, Val);
char *valstr(Val, char*);
void num(Val);
Val expr(void);
Val or(void);
Val and(void);
Val rel(void);
Val add(void);
Val mul(void);
Val match(void);
Val val(void);
void advance(void);

char *valstr(Val val, char *buf)
{
    char *p = val.s;
    if (!p) sprintf(p = buf, "%"PRIdMAX, val.n);
    return p;
}

Val regmatch(Val vstr, Val vregx)
{
    char b1[intlen], *str  = valstr(vstr , b1);
    char b2[intlen], *regx = valstr(vregx, b2);

    regex_t    re;
    regmatch_t matches[2];
    char       anchreg[strlen(regx) + 2];

    sprintf(anchreg, "^%s", regx);

    if (regcomp(&re, anchreg, 0))
        die(3, "regcomp failed");

    if (regexec(&re, str, 2, matches, 0))
        return (Val){ (re.re_nsub ? "" : NULL), 0 };

    if (re.re_nsub) {
        char    *ret, *p;
        regoff_t len = matches[1].rm_eo - matches[1].rm_so + 1;

        if (!(ret = malloc(len))) // FIXME: free
            die(3, "malloc failed");

        strncpy(ret, str + matches[1].rm_so, len);
        ret[len - 1] = '\0'; // strlcpy would be nice

        intmax_t d = strtoimax(ret, &p, 10);
        if (*ret && !*p)
            return (Val){ NULL, d };
        return (Val){ ret, 0 };
    }
    return (Val){ NULL, matches[0].rm_eo - matches[0].rm_so };
}

int valcmp(Val a, Val b)
{
    if (!a.s && !b.s)
        return (a.n > b.n) - (a.n < b.n);

    char b1[intlen], *p = valstr(a, b1);
    char b2[intlen], *q = valstr(b, b2);

    return strcmp(p, q);
}

void num(Val v)
{
    if (v.s)
        die(2, "syntax error: expected integer got `%s'", v.s);
}

Val expr(void)
{
    return or();
}

Val or(void)
{
    Val a = and();
    while (type == '|') {
        advance();
        Val b = and();
        if      ( a.s && *a.s) a = (Val){ a.s ,   0 };
        else if (!a.s &&  a.n) a = (Val){ NULL, a.n };
        else if ( b.s && *b.s) a = (Val){ b.s ,   0 };
        else                   a = (Val){ NULL, b.n };
    }
    return a;
}

Val and(void)
{
    Val a = rel();
    while (type == '&') {
        advance();
        Val b = rel();
        if (!(((a.s && *a.s) || a.n) && ((b.s && *b.s) || b.n)))
            a = (Val){ NULL, 0 };
    }
    return a;
}

Val rel(void)
{
    Val a = add();
    while (strchr("=><", type) || type == GE || type == LE || type == NE) {
        int op = type;
        advance();
        Val b = add();
        switch (op) {
            case '=': a = (Val){ NULL, valcmp(a, b) == 0 }; break;
            case '>': a = (Val){ NULL, valcmp(a, b) >  0 }; break;
            case GE : a = (Val){ NULL, valcmp(a, b) >= 0 }; break;
            case '<': a = (Val){ NULL, valcmp(a, b) <  0 }; break;
            case LE : a = (Val){ NULL, valcmp(a, b) <= 0 }; break;
            case NE : a = (Val){ NULL, valcmp(a, b) != 0 }; break;
        }
    }
    return a;
}

Val add(void)
{
    Val a = mul();
    while (strchr("+-", type)) {
        int op = type;
        num(a);
        advance();
        Val b = mul();
        num(b);
        switch (op) {
            case '+': a = (Val){ NULL, a.n + b.n }; break;
            case '-': a = (Val){ NULL, a.n - b.n }; break;
        }
    }
    return a;
}


Val mul(void)
{
    Val a = match();
    while (strchr("*/%", type)) {
        int op = type;
        num(a);
        advance();
        Val b = match();
        num(b);
        switch (op) {
            case '*': a = (Val){ NULL, a.n * b.n }; break;
            case '/': a = (Val){ NULL, a.n / b.n }; break;
            case '%': a = (Val){ NULL, a.n % b.n }; break;
        }
    }
    return a;
}

Val match(void)
{
    Val a = val();
    while (type == ':') {
        advance();
        a = regmatch(a, val());
    }
    return a;
}

Val val(void)
{
    Val v;
    if (type == '(') {
        advance();
        v = expr();
        if (type != ')')
            die(2, "syntax error: missing )");
        advance();
        return v;
    }
    v = yylval;
    advance();
    return v;
}

void advance(void)
{
    char *q, *p = *args++;
    if (!p) {
        type = EOA;
        return;
    }

    intmax_t d = strtoimax(p, &q, 10);
    if (*p && !*q) {
        yylval = (Val){ NULL, d };
        type = VAL;
        return;
    }

    char *ops = "|&=><+-*/%():";
    if (*p && !p[1] && strchr(ops, *p)) {
        type = *p;
        return;
    }

    if (!strcmp(p, ">=")) { type = GE; return; }
    if (!strcmp(p, "<=")) { type = LE; return; }
    if (!strcmp(p, "!=")) { type = NE; return; }

    yylval = (Val){ p, 0 };
    type = VAL;
}

int main(int argc, char **argv)
{
    if (!(intlen = snprintf(NULL, 0, "%"PRIdMAX, INTMAX_MIN) + 1))
        die(3, "failed to get max digits\n");

    args = argv + 1;
    if (*args && !strcmp("--", *args))
        ++args;

    advance();
    Val v = expr();
    if (v.s) printf("%s\n"        , v.s);
    else     printf("%"PRIdMAX"\n", v.n);

    return !((v.s && *v.s) || v.n);
}
