package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

var (
	wordsFile string
	maxDepth  int

	words *node               // words from words file
	found *node               // correct words found during search
	runes []rune              // all runes found in words file
	tfs   []func([]rune, int) // transformations
)

func init() {
	flag.StringVar(&wordsFile, "f", "/usr/share/dict/words", "dictionary file to use")
	flag.IntVar(&maxDepth, "d", 2, "max depth to search")

	// in init() due to circular initialization tfs -> replace -> spellWord -> tfs etc.
	tfs = []func([]rune, int){replace, remove, transpose, insert}
}

type node struct {
	isEnd    bool           // is this letter the end of a valid word?
	branches map[rune]*node // branches to all letters that can follow this one
}

// addWord adds a new word to a tree (*node).
func (tree *node) addWord(word []rune) {
	for _, r := range word {
		n := tree.branches[r]
		if n == nil {
			n = &node{false, make(map[rune]*node)}
			tree.branches[r] = n
		}
		tree = n
	}
	tree.isEnd = true
}

// isWord checks if a word exists in a tree (*node).
func (tree *node) isWord(word []rune) bool {
	for _, r := range word {
		tree = tree.branches[r]
		if tree == nil {
			return false
		}
	}
	return tree.isEnd
}

// readWords fills a tree (*node) with words from a file, one per line.
func (tree *node) readWords(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	runeset := make(map[rune]bool)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		word := []rune(scanner.Text())
		tree.addWord(word)
		for _, r := range word {
			if !runeset[r] {
				runeset[r] = true
				runes = append(runes, r)
			}
		}
	}
	if err = scanner.Err(); err != nil {
		return err
	}

	return nil
}

// spellWord first checks if a word exists in the words tree and not in the
// found tree, printing if this is the case. It then recurses through the
// possible transformations.
func spellWord(word []rune, depth int) {
	if depth == 0 {
		if words.isWord(word) && !found.isWord(word) {
			fmt.Println(string(word))
			found.addWord(word)
		}
		return
	}
	for _, f := range tfs {
		f(word, depth-1)
	}
}

// replace tries replacing each rune in a word with every rune found in the
// words file
func replace(word []rune, depth int) {
	for i := range word {
		t := word[i]
		for _, r := range runes {
			if t == r {
				continue
			}
			word[i] = r
			spellWord(word, depth)
		}
		word[i] = t
	}
}

// try swapping each adjacement pair of runes in word
func transpose(word []rune, depth int) {
	for i := 0; i < len(word)-1; i++ {
		if word[i] == word[i+1] {
			continue
		}
		word[i], word[i+1] = word[i+1], word[i]
		spellWord(word, depth)
		word[i], word[i+1] = word[i+1], word[i]
	}
}

// try removing each rune in word
func remove(word []rune, depth int) {
	t := word[0]
	copy(word, word[1:])
	word = word[:len(word)-1]

	spellWord(word, depth)
	for i := 0; i < len(word); i++ {
		word[i], t = t, word[i]
		spellWord(word, depth)
	}
	word = append(word, t)
}

// try inserting every rune found in file in between each rune in word
func insert(word []rune, depth int) {
	word = append(word, 0)
	copy(word[1:], word)

	for i := range word {
		for _, r := range runes {
			word[i] = r
			spellWord(word, depth)
		}
		if i+1 < len(word) {
			word[i] = word[i+1]
		}
	}
	word = word[:len(word)-1]
}

func main() {
	flag.Parse()

	words = &node{branches: make(map[rune]*node)}

	if err := words.readWords(wordsFile); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	for _, w := range flag.Args() {
		fmt.Printf("= %s =\n", w)
		word := []rune(w)
		found = &node{branches: make(map[rune]*node)}

		for i := 0; i <= maxDepth && len(found.branches) == 0; i++ {
			spellWord(word, i)
		}
		fmt.Println()
	}
}
