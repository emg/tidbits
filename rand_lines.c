// rand_lines
// Usage: rand_lines <eolbyte> <nlines> <file>
//
// given "file" with lines ending in "eolbyte", print "nlines" random lines in
// random order to stdout

#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/stat.h>

#define RAND_PATH "/dev/urandom"
#define BAIL() do { status = -1; goto cleanup; } while (0)

typedef struct {
    char  *beg;
    size_t len;
} Line;

char *prog;

// Retry reads and writes on EINTR
ssize_t rw(ssize_t (*act)(), int fd, void *buf, size_t n)
{
    ssize_t r;
    do r = act(fd, buf, n);
    while (r < 0 && errno == EINTR);
    return r;
}

// Continually read or write until entire n bytes are read or written
int rw_all(ssize_t (*act)(), int fd, void *buf, size_t n)
{
    char *b = buf;
    for (ssize_t r; n; b += r, n -= r)
        if ((r = rw(act, fd, b, n)) < 0)
            return -1;
    return 0;
}

// Random number generation from
// http://www0.cs.ucl.ac.uk/staff/d.jones/GoodPracticeRNG.pdf
uint32_t s[4]; // Seed
double drand32(void)
{
    uint64_t t;
    uint32_t r;
    s[0] = 314527869 * s[0] + 1234567;
    s[1] ^= s[1] << 5; s[1] ^= s[1] >> 7; s[1] ^= s[1] << 22;
    t = 4294584393ULL * s[2] + s[3]; s[3] = t >> 32; s[2] = t;
    r = s[0] + s[1] + s[2];
    return (double)r / (1ULL << 32);
}

int srand32(void)
{
    int status = 0, e = 0, fd;
    if (0 > (fd = open(RAND_PATH, O_RDONLY))) BAIL();
    if (0 > rw_all(read, fd, s, sizeof(s)))   BAIL();

cleanup:
    e = errno;
    if (fd > 0 && close(fd) < 0) perror(prog);
    errno = e;
    return status;
}

// Starting at *file, read n lines ending with eolbyte or up to *len bytes and
// store Lines in array at lines. If last line has no eolbyte, ignore it.
// Update *file and *len accordingly.
// Return number of lines read.
int read_lines(char eolbyte, size_t n, Line *lines, char **file, off_t *len)
{
    size_t i = n;
    while (i-- && *len > 0) {
        char *end = memchr(*file, eolbyte, *len);
        if (!end) { // reached end of file, no eolbyte
            *file += *len;
            *len   = 0;
            break;
        }
        ++end;
        lines[i] = (Line){ *file, end - *file };
        *file = end;
        *len -= lines[i].len;
    }
    return n - ++i;
}

// Write n Lines from lines to stdout
int write_lines(Line *lines, size_t n)
{
    while (n--)
        if (rw_all(write, 1, lines[n].beg, lines[n].len) < 0)
            return -1;
    return 0;
}

void shuffle_lines(Line *lines, size_t n)
{
    for (size_t i = n - 1; i; --i) {
        size_t r = drand32() * (i + 1);
        Line   t = lines[i];
        lines[i] = lines[r];
        lines[r] = t;
    }
}

// Starting at file, read n lines ending with eolbyte.
// For each subsequent line read, accept line with
// probability n / nl and replace random existing line.
// Shuffle chosen lines then print.
int rand_lines(char eolbyte, int n, char *file, off_t len)
{
    Line  *lines;
    Line  new;
    char *beg = file;
    int   status = 0;

    if (!(lines = malloc(n * sizeof(Line))))
        BAIL();
    if (read_lines(eolbyte, n, lines, &beg, &len) != n) {
        fprintf(stderr, "%s: more lines requested than in file\n", prog);
        errno = 0;
        BAIL();
    }

    for (size_t nl = n + 1; read_lines(eolbyte, 1, &new, &beg, &len); ++nl)
        if (drand32() * nl < (double)n)
            lines[(size_t)(drand32() * n)] = new;

    shuffle_lines(lines, n);

    if (write_lines(lines, n) < 0)
        BAIL();

cleanup:
    if (lines) free(lines);
    return status;
}

int main(int argc, char **argv)
{
    char  eolbyte, *file = MAP_FAILED;
    int   n, status = 0, fd = -1;
    off_t fsize;

    prog = *argv;

    if (4 != argc) {
        fprintf(stderr, "Usage: %s <eolbyte> <nlines> <file>\n", prog);
        BAIL();
    }
    if (0 > (n = atoi(argv[2]))) {
        fprintf(stderr, "%s: nlines must be nonnegative\n", prog);
        BAIL();
    }
    eolbyte = *argv[1];

    if (0 > srand32())                         BAIL();
    if (0 > (fd    = open(argv[3], O_RDONLY))) BAIL();
    if (0 > (fsize = lseek(fd, 0, SEEK_END)))  BAIL();

    if (n == 0) // Successfully returned no lines
        goto cleanup;
    if (fsize == 0) {
        fprintf(stderr, "%s: more lines requested than in file\n", prog);
        BAIL();
    }

    if (MAP_FAILED == (file = mmap(NULL, fsize, PROT_READ, MAP_SHARED, fd, 0))) BAIL();
    if (         0 >  rand_lines(eolbyte, n, file, fsize))                      BAIL();

cleanup:
    if (         0 != status && errno)               perror(prog);
    if (MAP_FAILED != file   && munmap(file, fsize)) perror(prog);
    if (         0 <= fd     && close(fd))           perror(prog);
    return status ? EXIT_FAILURE : EXIT_SUCCESS;
}
