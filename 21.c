#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

enum {
    Ace   = 1,
    Jack  = 11,
    Queen = 12,
    King  = 13,

    minrank = Ace,
    maxrank = King,
};

enum {
    Clubs,
    Diamonds,
    Hearts,
    Spades,

    minsuit = Clubs,
    maxsuit = Spades,
};

typedef struct Card Card;
struct Card {
    Card *n; /* next */
    int   r; /* rank */
    int   s; /* suit */
};

typedef struct Hand Hand;
struct Hand {
    Hand *n;    /* next */
    Card *cl;   /* card list */
    int   bet;
    int   sn;   /* split number */
};

typedef struct Player Player;
struct Player {
    Player *n;  /* next */
    Hand   *hl; /* hand list */
    int     cr; /* credits */
    int     pn; /* player number */
};

typedef struct {
    Card *nc;   /* next card */
    Card *cut;  /* cut card causing shuffle */
    Card *cap;  /* capacity */
    Card *dcp;  /* top of discard pile (to shuffle midhand) */
    Card  ca[]; /* card array */
} Shoe;

typedef struct {
    Hand    dh;  /* dealer hand */
    Player *pl;  /* player list */
    Shoe   *sh;
} Table;

struct {
    double bjpays;    /* blackjack pays. e.g. 3./2. */
    double cutcard;   /* placement of cut card as fraction of shoe */
    int    ndecks;    /* number of decks in the shoe */
    int    nsplits;   /* number of times a hand can split, -1 for no limit */
    int    charlie;   /* player with this many cards without busting wins, 0 to disable rule */
    char   ddsplit;   /* can double down after split */
    char   surrender; /* can surrender */
    char   s17;       /* dealer stands on soft 17 */

    char   showval;   /* show value of hand */
} cfg = {
    .bjpays    =  3./2.,
    .cutcard   = .8,
    .ndecks    =  8,
    .nsplits   = -1,
    .charlie   =  0,
    .ddsplit   =  1,
    .surrender =  0,
    .s17       =  1,

    .showval   =  0,
};

char *rankstr[] = {
    [Ace  ] =  "A",
    [    2] =  "2",
    [    3] =  "3",
    [    4] =  "4",
    [    5] =  "5",
    [    6] =  "6",
    [    7] =  "7",
    [    8] =  "8",
    [    9] =  "9",
    [   10] = "10",
    [Jack ] =  "J",
    [Queen] =  "Q",
    [King ] =  "K",
};

char *suitstr[] = {
    [Clubs   ] = "C",
    [Diamonds] = "D",
    [Hearts  ] = "H",
    [Spades  ] = "S",
};

int  strtoi(char *s, char **e, int useall);
void printcard(Card const *cp);
void printclist(Card const *cl);
void printdealer(Hand const *hp, int isflipped);
void printhand(Hand const *hp, char *trail);
void inscard(Hand *hp, Card *cp);
int  addhand(Player *pp, int bet);
void rmhand(Player *pp);
void addplayer(Table *tp, int pn);
void rmplayer(Table *tp, int pn);
int  value(Hand const *hp);
int  valuer(Card const *cl, int sum);
int  isbj(Hand *hp);
Shoe *mkshoe(int ndecks);
void shuffle(Shoe *sh, Card *end);
Card *deal(Shoe *sh);
int  takebets(Table *tp);
void playhand(Player *pp, Hand *hp, Shoe *sh);
int  play(Table *tp);

int
strtoi(char *s, char **e, int useall)
{
    long i = strtol(s, e, 10);
    if (i <= 0 || i >= INT_MAX || (useall && **e)) {
        puts("invalid number");
        return -1;
    }
    return i;
}

/* TODO: configure fancy printing */
void
printcard(Card const *cp)
{
    printf("%s%s ", rankstr[cp->r], suitstr[cp->s]);
}

void
printclist(Card const *cl)
{
    if (!cl)
        return;
    printclist(cl->n);
    printcard(cl);
}

void
printdealer(Hand const *hp, int isflipped)
{
    if (!isflipped) {
        printcard(hp->cl->n);
        puts("XX");
        return;
    }
    printclist(hp->cl);
    if (cfg.showval)
        printf("= %d", value(hp));
    putchar('\n');
}

void
printhand(Hand const *hp, char *trail)
{
    printf("b%d: ", hp->bet);
    printclist(hp->cl);
    if (cfg.showval)
        printf("= %d", value(hp));
    fputs(trail, stdout);
}

void
inscard(Hand *hp, Card *cp)
{
    cp->n  = hp->cl;
    hp->cl = cp;
}

int
addhand(Player *pp, int bet)
{
    Hand *hp = malloc(sizeof(*hp));
    if (!hp) {
        puts("failure to add hand");
        return -1;
    }
    *hp = (Hand){ .n = pp->hl, .bet = bet };
    pp->hl = hp;
    return 0;
}

void
rmhand(Player *pp)
{
    Hand *hp;
    if (!pp)
        return;
    hp = pp->hl;
    pp->hl = hp->n;
    free(hp);
}

void
addplayer(Table *tp, int pn)
{
    Player *pp;
    for (pp = tp->pl; pp && pp->pn != pn; pp = pp->n)
        ;
    if (pp) {
        puts("player number already in use");
        return;
    }
    if (!(pp = malloc(sizeof(*pp)))) {
        puts("failure to add player");
        return;
    }
    *pp  = (Player){ .n = tp->pl, .pn = pn };
    tp->pl = pp;
}

void
rmplayer(Table *tp, int pn)
{
    Player **ind, *pp;
    for (ind = &tp->pl; *ind && (*ind)->pn != pn; ind = &(*ind)->n)
        ;
    if (!(*ind)) {
        puts("invalid player number");
        return;
    }
    pp = *ind;
    *ind = (*ind)->n;
    free(pp);
}

int
value(Hand const *hp)
{
    return valuer(hp->cl, 0);
}

int
valuer(Card const *cl, int sum)
{
    int v1, v2;

    if (!cl)
        return sum;

    sum += cl->r < 10 ? cl->r : 10;
    v1 = valuer(cl->n, sum);

    if (cl->r != Ace)
        return v1;

    v2 = valuer(cl->n, sum + 10);

    if (v1 > v2) {
        int t = v1;
        v1 = v2;
        v2 = t;
    }
    if (v2 > 21)
        return v1;
    return v2;
}

int
isbj(Hand *hp)
{
    return value(hp) == 21 && !hp->cl->n->n && !hp->sn;
}

Shoe *
mkshoe(int ndecks)
{
    int ncards = ndecks * 52;
    Shoe *sh = malloc(sizeof(*sh) + ncards * sizeof(*sh->ca));
    if (!sh)
        return NULL;

    sh->nc = sh->ca;
    sh->cap = sh->ca + ncards;

    for (int i = 0; i < ndecks; i++)
        for (int s = minsuit; s <= maxsuit; ++s)
            for (int r = minrank; r <= maxrank; ++r)
                *sh->nc++ = (Card){ .r = r, .s = s };

    sh->nc = sh->ca;
    sh->cut = sh->cap;
    return sh;
}

void
shuffle(Shoe *sh, Card *end)
{
    for (int i = end - sh->ca - 1; i; i--) {
        int r = rand() % (i + 1);
        Card t    = sh->ca[i];
        sh->ca[i] = sh->ca[r];
        sh->ca[r] = t;
    }
    sh->nc = sh->ca;
}

Card *
deal(Shoe *sh)
{
    if (sh->nc >= sh->cap)
        shuffle(sh, sh->dcp);
    return sh->nc++;
}

int
takebets(Table *tp)
{
    static char *s = NULL;
    static size_t n = 0;

    char *e, *p;
    int i;
    Player *pp;
    Hand *hp;

    puts("Taking bets");
    puts("-----");
    puts("aX   : add player number X");
    puts("rX   : remove player X");
    puts("pXcN : player X adds N credits");
    puts("pXbN : player X bets N credits on new hand");
    puts("pXr  : player X removes last bet");
    puts("s    : status");
    puts("d    : deal");
    puts("q    : quit");
    puts("-----");

    /* TODO: handle EOF */
    /* TODO: handle multiple commands on one line */
    while (getline(&s, &n, stdin) >= 0) {
        s[strlen(s)-1] = 0;
        if (s[1] && !strchr("arp", *s)) {
            puts("invalid command");
            continue;
        }
        switch (*s) {
            case 'a':
            case 'r':
                if ((i = strtoi(s + 1, &e, 1)) < 0)
                    break;
                if (*s == 'a')
                    addplayer(tp, i);
                else
                    rmplayer(tp, i);
                break;
            case 'p':
                if ((i = strtoi(s + 1, &e, 0)) < 0)
                    break;
                for (pp = tp->pl; pp && pp->pn != i; pp = pp->n)
                    ;
                if (!pp) {
                    puts("invalid player number");
                    break;
                }
                p = e;
                switch (*p) {
                    case 'c':
                    case 'b':
                        if ((i = strtoi(p + 1, &e, 1)) < 0)
                            break;
                        if (*p == 'c') {
                            pp->cr += i;
                            break;
                        }
                        if (i > pp->cr) {
                            puts("bet greater than available credits");
                            break;
                        }
                        if (addhand(pp, i))
                            break;
                        pp->cr -= i;
                        break;
                    case 'r':
                        if (p[1]) {
                            puts("invalid command");
                            break;
                        }
                        pp->cr += pp->hl->bet;
                        rmhand(pp);
                        break;
                    default:
                        puts("invalid command");
                        break;
                }
                break;
            case 's':
                for (pp = tp->pl; pp; pp = pp->n) {
                    printf("Player %d: credits = %d, ", pp->pn, pp->cr);
                    for (hp = pp->hl; hp; hp = hp->n)
                        printf("bet %d, ", hp->bet);
                    putchar('\n');
                }
                break;
            case 'd':
                for (pp = tp->pl; pp; pp = pp->n)
                    if (pp->hl)
                        goto out;
                puts("no bets");
                break;
            case 'q':
                free(s);
                return 1;
            default:
                puts("invalid command");
                break;
        }
    }
out:
    return 0;
}

void
playhand(Player *pp, Hand *hp, Shoe *sh)
{
    int c;
    Hand *t;

    printhand(hp, " [hsdp?]: ");

    if (value(hp) == 21 && !hp->sn) {
        puts("blackjack");
        return;
    }

    /* TODO: add/configure surrender */
    /* TODO: don't allow multiple commands on one line */
    /* TODO: handle 'q' */
    while ((c = getchar()) != EOF) {
        switch (c) {
            case '?':
                puts("(h)it, (s)tand, (d)ouble down, s(p)lit");
                break;

            /* TODO: configure max cards per hand */
            /* TODO: configure hits after splits */
            case 'h':
                inscard(hp, deal(sh));
                if (value(hp) > 21)
                    goto out;
                break;
            case 's':
                goto out;

            /* TODO: configure double down after split */
            case 'd':
                if (hp->cl->n->n) {
                    puts("cannot double down after hit");
                    break;
                }
                if (hp->bet > pp->cr) {
                    puts("bet greater than available credits");
                    break;
                }
                pp->cr -= hp->bet;
                hp->bet *= 2;
                inscard(hp, deal(sh));
                goto out;

            /* TODO: configure number of splits */
            /* TODO: configure allowed cards for splits */
            case 'p':
                if (hp->cl->n->n || hp->cl->r != hp->cl->n->r) {
                    puts("cannot split this hand");
                    break;
                }
                if (hp->bet > pp->cr) {
                    puts("bet greater than available credits");
                    break;
                }
                if (addhand(pp, hp->bet))
                    break;
                t      = pp->hl;
                pp->hl = pp->hl->n;
                t->n   = hp->n;
                hp->n  = t;

                t->sn = ++hp->sn;

                inscard(t, hp->cl->n);
                hp->cl->n = NULL;

                inscard(hp, deal(sh));
                inscard(t, deal(sh));
                printhand(t, "\n");
                break;
            default:
                puts("invalid command");
                break;
        }
        if ((c = getchar()) != '\n' && c != EOF)
            putchar(c);
        printhand(hp, " [hsdp?]: ");
    }
out:
    if ((c = getchar()) != '\n' && c != EOF)
        putchar(c);
    printhand(hp, value(hp) > 21 ? " : bust\n" : "\n");
}

int
dealerhits(Hand *hp)
{
    int v = value(hp);

    if (v < 17)
        return 1;
    if (v > 17)
        return 0;

    /* TODO: fix soft 17 handling */
    if (hp->cl->n->n)
        return 0;
    if (hp->cl->r == Ace || hp->cl->n->r == Ace)
        return !cfg.s17;
    return 0;
}

int
play(Table *tp)
{
    int dbj, dv;

    if (tp->sh->nc >= tp->sh->cut)
        shuffle(tp->sh, tp->sh->cap);

    if (takebets(tp))
        return 1;

    for (int i = 0; i < 2; i++) {
        for (Player *pp = tp->pl; pp; pp = pp->n)
            for (Hand *hp = pp->hl; hp; hp = hp->n)
                inscard(hp, deal(tp->sh));
        inscard(&tp->dh, deal(tp->sh));
    }

    puts("\n-----\nDealer:");
    printdealer(&tp->dh, 0);
    for (Player *pp = tp->pl; pp; pp = pp->n) {
        printf("\nPlayer %d:\n", pp->pn);
        for (Hand *hp = pp->hl; hp; hp = hp->n)
            printhand(hp, "\n");
    }
    puts("-----");

    /* TODO: add insurance */

    for (Player *pp = tp->pl; pp; pp = pp->n) {
        printf("\nPlayer %d:\n", pp->pn);
        for (Hand *hp = pp->hl; hp; hp = hp->n)
            playhand(pp, hp, tp->sh);
    }

    /* TODO: configure dealer checks for blackjack */
    puts("\n-----\nDealer:");
    printdealer(&tp->dh, 1);

    /* TODO: configure max cards per hand */
    while (dealerhits(&tp->dh)) {
        inscard(&tp->dh, deal(tp->sh));
        printdealer(&tp->dh, 1);
    }
    puts("-----");

    dbj = isbj(&tp->dh);
    dv  = value(&tp->dh);

    for (Player *pp = tp->pl; pp; pp = pp->n) {
        printf("Player %d:\n", pp->pn);
        for (Hand *hp = pp->hl; hp; hp = hp->n) {
            int pbj = isbj(hp), pv = value(hp);

            printhand(hp, " : ");

            if (dbj && pbj) {
                puts("push");
                pp->cr += hp->bet;
            } else if (pbj) {
                puts("blackjack");
                pp->cr += hp->bet + hp->bet * cfg.bjpays;
            } else if (dbj) {
                puts("lose");
            } else if (pv > 21) {
                puts("bust");
            } else if (dv > 21 || pv > dv) {
                puts("win");
                pp->cr += hp->bet * 2;
            } else if (pv == dv) {
                puts("push");
                pp->cr += hp->bet;
            } else {
                puts("lose");
            }
        }
    }
    puts("-----\n");

    return 0;
}

int
main(void)
{
    Table t = { .sh = NULL };

    srand(time(NULL));

    if (!(t.sh = mkshoe(cfg.ndecks)))
        return 1;
    shuffle(t.sh, t.sh->cap);

    t.sh->cut = t.sh->ca + (ptrdiff_t)((t.sh->cap - t.sh->ca) * cfg.cutcard);

    for (;;) {
        int q = play(&t);

        t.dh.cl = NULL;
        for (Player *pp = t.pl; pp; pp = pp->n)
            while (pp->hl)
                rmhand(pp);

        t.sh->dcp = t.sh->nc;

        if (q)
            break;
    }

    free(t.sh);

    for (Player *pp = t.pl, *rm = pp; pp; rm = pp) {
        pp = pp->n;
        free(rm);
    }

    return 0;
}
