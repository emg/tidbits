package main

import (
	"bufio"
	"crypto/tls"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
)

type buf struct {
	name  string
	num   int
	out   chan string
	lines []string
}

var curbuf *buf
var bufmap = map[string]*buf{}
var bufsl = []*buf{}

func newbuf(name string, out chan string) *buf {
	b := &buf{name: name, num: len(bufsl), out: out, lines: []string{}}
	bufmap[name] = b
	bufsl = append(bufsl, b)
	if curbuf == nil {
		curbuf = b
	}
	return b
}

func split(s, sep string) (string, string) {
	ss := strings.SplitN(s, sep, 2)
	s1 := ""
	if len(ss) == 2 {
		s1 = ss[1]
	}
	return ss[0], s1
}

func lexsrv(s string) (nick, usr string, args []string) {
	if s[0] == ':' {
		nick, s = split(s[1:], " ")
		nick, usr = split(nick, "!")
	}
	s, tail := split(s, " :")
	args = strings.Split(s, " ")
	if tail != "" {
		args = append(args, tail)
	}
	return
}

func srvout(con io.Writer, in chan string) {
	for ln := range in {
		fmt.Fprint(con, ln+"\r\n")
	}
}

func srvin(con io.Reader, out chan string) {
	scn := bufio.NewScanner(con)
	for scn.Scan() {
		parsesrv(scn.Text(), out)
	}
}

func parsesrv(ln string, out chan string) {
	nick, usr, args := lexsrv(ln)

	to := ""
    msg := ""
	if len(args) > 1 {
		to = args[1]
	}

	switch args[0] {
	case "PING":
		out <- "PONG " + ln[5:]
		return
	case "JOIN":
		msg = fmt.Sprintf("%s(%s) has joined %s", nick, usr, args[1])
	case "PART":
		if nick == "emg__" {
			return
		}
		msg = fmt.Sprintf("%s(%s) has left %s", nick, usr, args[1])
	case "MODE":
		msg = fmt.Sprintf("%s changed mode %s", nick, strings.Join(args[1:], " "))
	case "PRIVMSG":
		msg = fmt.Sprintf("<%s> %s", nick, args[2])
	default:
		msg = ln
	}

	if to == "emg__" || usr == "" {
		to = nick
	}
	b := bufmap[to]
	if b == nil {
		b = newbuf(to, out)
		fmt.Printf("new buf %s\n", to)
	}
	b.lines = append(b.lines, msg)
	if b == curbuf {
		fmt.Println(msg)
	}
}

func usr() {
	scn := bufio.NewScanner(os.Stdin)
	for scn.Scan() {
		ln := scn.Text()
        if ln[0] == '/' {
            if ln[1] == 'n' {
                n := curbuf.num + 1
                if n >= len(bufsl) {
                    n = 0
                }
                curbuf = bufsl[n]
                fmt.Println("---------" + curbuf.name + "-----------")
                for _, l := range curbuf.lines {
                    fmt.Println(l)
                }
                continue
            }
            curbuf.out <- ln[1:]
            continue
        }
        curbuf.out <- "PRIVMSG " + curbuf.name + " :" + ln
	}
}

func main() {
	con, err := tls.Dial("tcp", "chat.freenode.net:6697", nil)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	defer con.Close()

	wg := sync.WaitGroup{}
	wg.Add(3)

	sout := make(chan string)
	go func() {
		srvout(con, sout)
		wg.Done()
	}()
	go func() {
		srvin(con, sout)
		wg.Done()
	}()
	go func() {
		sout <- "NICK emg__"
		sout <- "USER emg__ * * emg__"
		usr()
		wg.Done()
	}()

	wg.Wait()
}
