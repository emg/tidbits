/*
sudoku solver

example input:
6 9 0 0 0 0 0 0 0
7 0 0 0 0 0 0 1 0
0 0 0 8 0 0 0 4 0
0 0 0 0 0 0 0 0 0
0 0 0 0 9 6 0 0 0
0 3 5 0 0 0 0 0 0
0 0 3 5 0 0 0 0 2
0 0 0 0 0 2 7 0 9
0 0 0 0 0 0 0 0 6

or

6 9 0 0 0 0 0 0 0 7 0 0 0 0 0 0 1 0 0 0 0 8 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 9 6 0 0 0 0 3 5 0 0 0 0 0 0 0 0 3 5 0 0 0 0 2 0 0 0 0 0 2 7 0 9 0 0 0 0 0 0 0 0 6
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    SUB  = 3,         /* subdivision */
    SIZE = SUB * SUB, /* size of all dimensions */
};

int nsols;

void print(unsigned char puzzle[SIZE][SIZE])
{
    for (size_t i = 0; i < SIZE; i++)
        for (size_t j = 0; j < SIZE; j++)
            printf("%hhu%c", puzzle[i][j], j + 1 == SIZE ? '\n' : ' ');
    printf("\n");
}

void input(unsigned char puzzle[SIZE][SIZE])
{
    /* read puzzle as 81 whitespace separated numbers */
    for (size_t i = 0; i < SIZE; i++)
        for (size_t j = 0; j < SIZE; j++)
            if (scanf("%hhu", &puzzle[i][j]) != 1 || puzzle[i][j] > SIZE)
                exit(3);

    /* check that no number appears more than once in a single row or column */
    for (size_t i = 0; i < SIZE; i++) {
        unsigned char usedcol[SIZE + 1] = { 0 }, usedrow[SIZE + 1] = { 0 };
        for (size_t j = 0; j < SIZE; j++)
            if ((puzzle[i][j] && usedrow[puzzle[i][j]]++) ||
                (puzzle[j][i] && usedcol[puzzle[j][i]]++))
                exit(2);
    }

    /* check that no number appears more than once in a single subdivision */
    for (size_t imin = 0; imin < SIZE; imin += SUB) {
        for (size_t jmin = 0; jmin < SIZE; jmin += SUB) {
            unsigned char used[SIZE + 1] = { 0 };
            for (size_t i = imin; i < imin + SUB; i++)
                for (size_t j = jmin; j < jmin + SUB; j++)
                    if (puzzle[i][j] && used[puzzle[i][j]]++)
                        exit(2);
        }
    }
    print(puzzle);
}

void solve(unsigned char puzzle[SIZE][SIZE], size_t row, size_t col)
{
    unsigned char used[SIZE + 1] = { 0 };

    /* past the last column, go to next row */
    if (col == SIZE) {
        row++;
        col = 0;
    }

    /* past the last row, we found a solution */
    if (row == SIZE) {
        nsols++;
        print(puzzle);
        return;
    }

    /* cell was set on input, move to next one */
    if (puzzle[row][col]) {
        solve(puzzle, row, col + 1);
        return;
    }

    /* find which numbers have been used in this row and column */
    for (size_t i = 0; i < SIZE; i++) {
        used[puzzle[i][col]] = 1;
        used[puzzle[row][i]] = 1;
    }

    /* find which numbers have been used in this subdivision */
    for (size_t imin = row / SUB * SUB, i = 0; i < SUB; i++)
        for (size_t jmin = col / SUB * SUB, j = 0; j < SUB; j++)
            used[puzzle[imin + i][jmin + j]] = 1;

    /* try each of the unused numbers and move to next cell */
    for (size_t try = 1; try <= SIZE; try++) {
        if (used[try])
            continue;
        puzzle[row][col] = try;
        solve(puzzle, row, col + 1);
    }

    puzzle[row][col] = 0;
}

int main(void)
{
    unsigned char puzzle[SIZE][SIZE];
    input(puzzle);
    solve(puzzle, 0, 0);
    return !nsols;
}
