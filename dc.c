#define _POSIX_C_SOURCE 200809L
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN(a) (sizeof(a) / sizeof(*a))

/* single characters represent themselves */
enum {
    NUM = UCHAR_MAX + 1,
    STR,
    LE, GE, NE, /* !>, !<, != */
};

typedef struct {
    union {
        double d;
        char  *s;
    };
    int type;
} Token;

typedef struct {
    Token *data;
    size_t top;
    size_t cap;
} Stack;

Stack stack;
Stack regs[256];
int halt;

_Noreturn void err(int status, char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    exit(status);
}

void push(Stack *s, Token t)
{
    if (s->top == s->cap) {
        if (!(s->data = realloc(s->data, sizeof(*s->data) * (s->cap * 2 + 1))))
            err(errno, "realloc: %s\n", strerror(errno));
        s->cap = s->cap * 2 + 1;
    }
    s->data[s->top++] = t;
}

Token pop(Stack *s)
{
    if (!s->top)
        err(3, "pop: empty stack\n");
    return s->data[--s->top];
}

Token peek(Stack *s)
{
    if (!s->top)
        err(3, "peek: empty stack\n");
    return s->data[s->top - 1];
}

int empty(Stack *s)
{
    return !s->top;
}

void freestack(Stack *s)
{
    Token t;
    while (s->top)
        if ((t = pop(s)).type == STR)
            free(t.s);
    free(s->data);
}

Token lex(char **s)
{
    while (isspace(**s))
        (*s)++;

    if (!**s || **s == '#')
        return (Token){ .type = 0 };

    if (isdigit(**s) || **s == '_') {
        double sign = **s == '_' ? -1 : 1;
        if (**s == '_')
            (*s)++;
        return (Token){ .type = NUM, .d = sign * strtod(*s, s) };
    }
    if (**s == '[') {
        char *p = *s;
        for (int nest = 0; **s; (*s)++)
            if (**s == '[')
                nest++;
            else if (**s == ']' && !--nest)
                break;

        if (!**s)
            err(2, "multiline strings not supported\n");
        **s = '\0';
        p = strdup(p + 1);
        **s = ']';
        (*s)++;
        return (Token){ .type = STR, .s = p };
    }
    if (**s == '!') {
        (*s) += 2;
        switch ((*s)[-1]) {
        default : err(2, "bad token: %s\n", *s - 2);
        case '>': return (Token){ .type = LE };
        case '<': return (Token){ .type = GE };
        case '=': return (Token){ .type = NE };
        }
    }
    if (strchr("+-*/%^pPsSlLxfcdrvq><=", **s))
        return (Token){ .type = *(*s)++ };

    err(2, "bad token: %s\n", *s);
}

void arithmetic(Token tok)
{
    Token a, b;

    if ((b = pop(&stack)).type != NUM) err(2, "not a number\n");
    if ((a = pop(&stack)).type != NUM) err(2, "not a number\n");

    switch (tok.type) {
    case '+': push(&stack, (Token){ .type = NUM, .d =      a.d + b.d  }); break;
    case '-': push(&stack, (Token){ .type = NUM, .d =      a.d - b.d  }); break;
    case '*': push(&stack, (Token){ .type = NUM, .d =      a.d * b.d  }); break;
    case '/': push(&stack, (Token){ .type = NUM, .d =      a.d / b.d  }); break;
    case '%': push(&stack, (Token){ .type = NUM, .d = fmod(a.d , b.d) }); break;
    case '^': push(&stack, (Token){ .type = NUM, .d = pow (a.d , b.d) }); break;
    }
}

/* FIXME: declarations at top of file */
int eval(char *s);

void conditional(Token tok, int reg)
{
    Token a, b;
    char exec[] = { 'l', reg, 'x', '\0' };

    if ((a = pop(&stack)).type != NUM) err(2, "not a number\n");
    if ((b = pop(&stack)).type != NUM) err(2, "not a number\n");

    switch (tok.type) {
    case '>': if (a.d >  b.d) eval(exec); break;
    case '<': if (a.d <  b.d) eval(exec); break;
    case '=': if (a.d == b.d) eval(exec); break;
    case GE : if (a.d >= b.d) eval(exec); break;
    case LE : if (a.d <= b.d) eval(exec); break;
    case NE : if (a.d != b.d) eval(exec); break;
    }
}

void usereg(Token tok, int reg)
{
    switch (tok.type) {
    case 's':
        if (!empty(&regs[reg]))
            if ((tok = pop(&regs[reg])).type == STR)
                free(tok.s);
        /* fall through */
    case 'S': push(&regs[reg], pop(&stack    )); break;
    case 'L': push(&stack    , pop(&regs[reg])); break;
    case 'l':
        if ((tok = peek(&regs[reg])).type == STR)
            tok.s = strdup(tok.s);
        push(&stack, tok);
        break;
    }
}

void printtok(Token tok)
{
    switch (tok.type) {
    default : err(2, "that's impossible!!!\n");
    case NUM: printf("%f\n", tok.d); break;
    case STR: printf("%s\n", tok.s); break;
    }
}

int eval(char *s)
{
    Token tok, t;

    while ((tok = lex(&s)).type) {
        switch (tok.type) {
        default :
            err(2, "bad token: '%c' (%d)\n", tok.type);

        case STR:
        case NUM:
            push(&stack, tok);
            break;

        case '+':
        case '-':
        case '*':
        case '/':
        case '%':
        case '^':
            arithmetic(tok);
            break;

        case '>':
        case '<':
        case '=':
        case GE :
        case LE :
        case NE :
            conditional(tok, *s++); /* oops, didn't use lex(), that's cheating */
            break;

        case 'q':
            return 2;

        case 'v':
            if (peek(&stack).type != NUM)
                err(2, "not a number\n");
            push(&stack, (Token){ .type = NUM, .d = sqrt(pop(&stack).d) });
            break;

        case 'r':
            tok = pop(&stack);
            t   = pop(&stack);
            push(&stack, tok);
            push(&stack, t  );
            break;

        case 'd':
            if ((tok = peek(&stack)).type == STR)
                tok = (Token){ .type = STR, .s = strdup(tok.s) };
            push(&stack, tok);
            break;

        case 'c':
            while (stack.top)
                if ((tok = pop(&stack)).type == STR)
                    free(tok.s);
            stack.top = 0;
            break;

        case 'x':
            if (peek(&stack).type == STR) {
                int i = eval((tok = pop(&stack)).s);
                free(tok.s);
                if (i)
                    return i - 1;
            }
            break;

        case 'f':
            for (Token *t = stack.data + stack.top - 1; t >= stack.data; t--)
                printtok(*t);
            break;

        case 'p':
            printtok(peek(&stack));
            break;

        case 'P':
            printtok(tok = pop(&stack));
            if (tok.type == STR)
                free(tok.s);
            break;

        case 's':
        case 'S':
        case 'l':
        case 'L':
            usereg(tok, *s++); /* oops, didn't use lex(), that's cheating */
            break;
        }
    }
    return 0;
}

int main(void)
{
    char *line = NULL;
    size_t n = 0;;
    while (getline(&line, &n, stdin) != -1)
        if (eval(line))
            break;
    free(line);

    for (Stack *s = regs; s < regs + LEN(regs); s++)
        freestack(s);
    freestack(&stack);
    return 0;
}
