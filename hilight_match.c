/*
 * ./hilight_match "$(tput setaf 1)" "$(tput sgr0)" foobar
 *
 * hilight longest match of foobar at beginning of lines
 * hilight by printing first argument, end by printing second
 */
#include <stdio.h>

int main(int argc, char **argv)
{
	char *beg_color, *end_color, *query, *q;
	int  c, on = 1;

	if (argc != 4) return 1;

	beg_color = argv[1];
	end_color = argv[2];
	query = q = argv[3];

	while ((c = getc(stdin)) != EOF) {
		switch (on) {
			case 1: if (c != *q) { on = 0; break;                               }
			        else         { on = 2; fputs(beg_color, stdout);            }
			case 2: if (c != *q) { on = 0; fputs(end_color, stdout); q = query; }
			        else         { q++;                                         }
		}
		if (c == '\n') on = 1;
		putc(c, stdout);
	}

	return 0;
}
