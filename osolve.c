#include <string.h>
#include <stdio.h>

/*
 * The solver uses Pochmann's T perm blindfold method. It first solves the
 * edges, then the corners. The setup moves are encoded in the int arrays, and
 * the T permutation as the number 25040607254490693
 *
 * Reading the cube
 * ----------------
 * findpiece() uses a bitmap/hash for pieces, idea taken from Jaap's entry in
 * Tom Rokicki's short cube code contest. In order to represent the array as a
 * string (specifically one that looks obfuscated), add 28 to the values. faces
 * string is also obfuscated, by subtracting 33 from the ascii values.
 *
 * faces: "UDFBRL" (minus 33 = "4#%!1+")
 * index:  012345
 *
 * example:
 * UF = 1<<0 | 1<<2 = 5
 * 5 + 28 = 33 = '!'
 * position of '!' in hash "!-%=\".&>0@4D" is 0, UF is piece 0
 *
 * Orientation is determined by position of the minimum face index
 *
 * example:
 * UFR = 0,2,4 : position of minimum face is 0
 * FRU = 2,4,0 : position of minimum face is 2
 *
 * Correctly oriented pieces are first so that a solved cube satisfies the
 * property cube[i] == i
 *
 * example:
 * BDL = 1<<3 | 1<<1 | 1<<5 = 42
 * 42 + 28 = 70 = 'F'
 * index of 'F' in hash "15EA2BF6" is 6
 * BDL is piece number 6
 *
 * BDL = 3,1,5
 * position of minimum face (D) is 1
 * orientation of BDL is 1
 *
 * BDL = piece number + orientation number * number of pieces = 6 + 1*8 = 14
 *
 * Decoding move sequences
 * -----------------------
 * Setup move sequences consist of a series of 5 bit entries. The first 3 bits
 * correspond to the face, the next 2 to the number of turns. The faces and
 * turns begin at 1 to differentiate from unused portions of the number that
 * remain 0. Move sequences are indexed by piece number.
 *
 * faces: "-UDFBRL"
 * index:   123456
 * turns: "- 2'"
 * index:   123
 *
 * example:
 * setup move for BDL, piece 14, is 23893
 * 23893 = 0x5d55 = 0b101110101010101 =
 * 101,11  010,10  101,01 = 5,3  2,2  5,1 = R' D2 R
 */

int findpiece(char *str)
{
    int len = strlen(str), numpieces = len == 2 ? 12 : 8, min=6, twist, n, i, p;

    char *faces = "4#%!1+", *hash = len == 2 ? "!-%=\".&>0@4D" : "15EA2BF6";

    for(i = n = twist = 0; i < len; i++) {
        if ((p = strchr(faces, str[i]-33) - faces) < min)
            min = p, twist = i;
        n |= 1 << p;
    }
    return strchr(hash, 28+n) - hash + numpieces*twist;
}

/* print a sequence of moves, or if invert print the sequence backwards with
 * inverted turn counts */
void printseq(long long move, int invert)
{
    if (!move)
        return;

    if (!invert)
        printseq(move >> 5, invert);

    /* strings are obfuscated versions of " UDFBRL" and "  2'" */
    printf("%c%c ", 21+"~@/1-=7"[move>>2 & 7], "?(:/"[invert ? 4-(move&3) : move&3]-8);

    if (invert)
        printseq(move >> 5, invert);
}

/*
 * solve a subsection of the cube (edges or corners)
 * starting at cube of length n (12 or 8)
 * looking up by piece index in setupmoves
 * keeping track of parity in odd
 */
int solve(int *cube, int n, int *setupmoves, int odd)
{
    int i = n, piece = cube[1], t = piece % n;
    /* check if cube is solved */
    while (i-- && cube[i] == i)
        ;
    /* if it is, return parity */
    if (!++i)
        return odd;
    /* if the piece in the from position (UR for edges, URB for corners, both
     * are piece number 1 in their set) already belongs there, find the first
     * piece out of place and switch with it to break into a new cycle */
    if (t == 1) {
        for (piece = 0; piece < n; piece++)
            if (piece != 1 && cube[piece] != piece)
                break;
        t = cube[piece], cube[piece] = cube[1], cube[1] = t;

    /* else put it in place and grab the piece from that position, accounting
     * for orientation changes */
    } else {
        cube[1] = (cube[t] + piece/n*n) % (n * (n == 12 ? 2 : 3));
        cube[t] = t;
    }

    /* print the setup moves, the T permutation, and inverse of setup moves */
    printseq(setupmoves[piece], 0);
    printseq(25040607254490693, 0);
    printseq(setupmoves[piece], 1);

    /* recurse (because that's more fun than looping) and invert odd to keep
     * track of parity */
    return solve(cube, n, setupmoves, !odd);
}

int main(int argc, char **argv)
{
    argv++; argc--;
    int i, cube[20];

    /* populate the cube */
    for (i = 0; i < argc; i++)
        cube[i] = findpiece(argv[i]);

    /* solve the edges */
    if (solve(cube, 12,
         (int[]){22710,0,22774,0,378,346,314,26,6822,27,6886,25,704375,0,771893,
         26717626,702327,12236663,773941,10139511,7653,7589,5671,5735}, 0))
        /* swap corners due to odd number of T perms */
        i = cube[13], cube[13] = cube[12], cube[12] = i;

    /* solve corners */
    solve(cube + 12, 8,
     (int[]){0,0,27098,14702,366,14,302,334,782939471,0,566894,26537326,384309,
     23861,23893,374063,781890863,0,894425,13,15,303,335, 367}, 0);

    return 0;
}
