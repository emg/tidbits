#!/bin/sh

# got a bike, wanted to look at the gearing ratios
# 
# 22 34
# 11 13 15 17 19 21 24 28 32 36

awk '
NR == 1 {
    for (i = 1; i <= NF; i++)
        rings[i] = $i
}
NR == 2 {
    for (i = 1; i <= NF; i++)
        cogs[NF - i + 1] = $i
}
END {
    for (r in rings)
        for (c in cogs)
            printf("%2d %2d %f\n", r, c, rings[r] / cogs[c])
}' "$@" | sort -k 3n
