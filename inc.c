/*
 * ./inc file count
 * read number from file, increment, write back to file
 * do so count times
 *
 * test locking with
 *
 * rm foo; pids=(); for ((i = 0; i < 100; i++)); do ./inc foo 1000 & pids+=($!); done; for pid in "${pids[@]}"; do wait "$pid"; echo $?; done; cat foo
 *
 * prints exit status of all processes, make sure it's 0
 *
 */
#define _XOPEN_SOURCE 700 // for gcc, also gives us _POSIX_C_SOURCE == 200809L
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    if (argc != 3)
        return 1;

    int n = atoi(argv[2]);
    if (!n)
        return 2;

    while (n--) {
        int   i = 0;
        FILE *f = fopen(argv[1], "r+");
        if (!f && errno == ENOENT) {
            f = fopen(argv[1], "w");
            i = -1;
        }
        if (!f)
            return 3;

        if (lockf(fileno(f), F_LOCK, 0))
            return 4;

        if (i >= 0 && fscanf(f, "%d", &i) != 1)
            return 5;
        rewind(f);
        if (fprintf(f, "%d\n", ++i) <= 0)
            return 6;

        if (fclose(f))
            return 7;
    }

    return 0;
}
