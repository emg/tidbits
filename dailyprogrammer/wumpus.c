/* https://www.reddit.com/r/dailyprogrammer/comments/21kqjq/4282014_challenge_154_hard_wumpus_cave_game/ */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* only used for unvisited rooms, once visited it becomes the printable
 * character representing the content of the room */
enum {
    GOLD, PITS, WEAP, WUMP, EMPTY,
};

int armed;
int points;

/* initialize cave, place player's starting position in *a, *b */
void init(int n, char cave[n][n], int *a, int *b)
{
    int i = rand() % (n - 2) + 1, j = rand() % (n - 2) + 1;
    int probs[] = {
        [GOLD] = 15,
        [PITS] =  5,
        [WUMP] = 15,
        [WEAP] = 15,
    };

    /* fill entire cave with walls, set the entrance, make that our initial
     * start point */
    memset(cave, EMPTY, sizeof(cave[0][0]) * n * n);
    cave[i][j] = '^';
    *a = i;
    *b = j;

    /* create rooms according to probabilities */
    for (i = 0; (size_t)i < sizeof(probs)/sizeof(*probs); i++) {
        for (j = probs[i] * (n - 2) * (n - 2) / 100; j; j--) {
            char *p = &cave[rand() % (n - 2) + 1][rand() % (n - 2) + 1];
            if (*p == '#')
                *p = i;
            else
                j++;
        }
    }

    /* rest of rooms are empty */
    for (i = 1; i < n - 1; i++)
        for (j = 1; j < n - 1; j++)
            if (cave[i][j] == '#')
                cave[i][j] = EMPTY;
}

/* display map, description of current room, hints of adjacent rooms, status */
void display(int n, char cave[n][n], int a, int b)
{
    int i, j, pits = 0, wump = 0;

    putchar('\n');
    for (i = a - 1; i <= a + 1; i++) {
        for (j = b - 1; j <= b + 1; j++) {
            if (i == a && j == b)        putchar('@');
            else if (cave[i][j] > EMPTY) putchar(cave[i][j]);
            else                         putchar('?');

            if ((i == a || j == b) && cave[i][j] == WUMP) wump = 1;
            if ((i == a || j == b) && cave[i][j] == PITS) pits = 1;
        }
        putchar('\n');
    }

    switch (cave[a][b]) {
    case '$': puts("you found gold");           break;
    case 'W': puts("you found a weapon");       break;
    case '.': puts("you are in an empty room"); break;
    case '^': puts("you are at the entrance");  break;
    }

    if (wump) puts("you smell a foul stench");
    if (pits) puts("you hear howling wind");

    printf("[%d points] You are %sarmed\n", points, armed ? "" : "un");
}

/* return 1 to continue, 0 on death
 * if player moved update position in *a, *b */
int move(int n, char cave[n][n], int *a, int *b, int dir)
{
    int i = *a + (dir == 'S') - (dir == 'N');
    int j = *b + (dir == 'E') - (dir == 'W');

    switch (cave[i][j]) {
    case '#'  : puts("you walked into a wall"); return 1;
    case PITS : puts("you fell into a pit");    return 0;
    case GOLD : cave[i][j] = '$';               break;
    case WEAP : cave[i][j] = 'W';               break;
    case EMPTY: cave[i][j] = '.'; points++;     break;
    case WUMP :
        if (armed) {
            puts("you killed a wumpus");
            cave[i][j] = '.';
            points += 10;
        } else {
            puts("you were eaten by a wumpus");
            return 0;
        }
        break;
    }
    *a = i;
    *b = j;
    return 1;
}

/* return 0 to quit, 1 to continue, if player moves update position in *a, *b */
int action(int n, char cave[n][n], int *a, int *b)
{
    int c, i, j;

    printf("\naction> ");

    /* no portable way to make terminal raw so you have to hit enter */
    if ((c = getchar()) == EOF)
        c = 'X'; /* quit on EOF */
    else
        while ((i = getchar()) != EOF && i != '\n')
            ;/* discard rest of line */

    switch ((c = toupper(c))) {
    default:
        puts("?NSEWLRX");
        break;
    case 'N': case 'S': case 'E': case 'W':
        return move(n, cave, a, b, c);
    case 'X':
        points = -1;
        return 0;
    case 'R':
        if (cave[*a][*b] == '^') {
            puts("you exit the cave");
            return 0;
        } else {
            puts("this is not the exit");
            break;
        }
    case 'L':
        switch (cave[*a][*b]) {
        default:
            puts("there is nothing here to loot");
            break;
        case 'W':
            puts("you picked up weapon");
            armed = 1;
            points += 5;
            cave[*a][*b] = '.';
            /* all other weapon caves become gold caves */
            for (i = 1; i < n - 1; i++) {
                for (j = 1; j < n - 1; j++) {
                    switch (cave[i][j]) {
                    case 'W' : cave[i][j] = '$' ; break;
                    case WEAP: cave[i][j] = GOLD; break;
                    }
                }
            }
            break;
        case '$':
            puts("you picked up gold");
            points += 5;
            cave[*a][*b] = '.';
            break;
        }
    }
    return 1;
}

int main(int argc, char **argv)
{
    if (argc != 2)
        return 1;

    int i, j, n = atoi(argv[1]) + 2;
    char cave[n][n];

    init(n, cave, &i, &j);
    do display(n, cave, i, j);
    while (action(n, cave, &i, &j));

    if (points >= 0)
        printf("\nGame Over\nyou scored %d points\n", points);
    return 0;
}
