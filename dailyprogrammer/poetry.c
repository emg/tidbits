/* https://www.reddit.com/r/dailyprogrammer/comments/3bzipa/20150703_challenge_221_hard_poetry_in_a_haystack/
 * read the explanation of the first solution, thought it sounded cool, figured I'd try it
 * the iliad as input was important, the word list through frequencies off
 * USAGE: ./poetry file1 file2 min
 * file1 is used to create the bigram table
 * file2 is the poetry/gibberish file
 * min is the minimum number of times a bigraph must appear
 *
 * ./poetry iliad.mb.txt poetry.txt 50
 * is the solution from the first comment
 */
#define _POSIX_C_SOURCE 200809L

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    FILE *f;
    char *p, *s = NULL;
    size_t n = 0;
    int c, m, l = 0, bg[26][26] = { 0 };

    if (argc != 4)
        return 1;

    m = atoi(argv[3]);

    if (!(f = fopen(argv[1], "r")))
        return 2;

    while ((c = tolower(fgetc(f))) != EOF) {
        if (islower(c)) {
            if (l)
                bg[l - 'a'][c - 'a']++;
            l = c;
        } else {
            l = 0;
        }
    }

    fclose(f);

    if (!(f = fopen(argv[2], "r")))
        return 3;

    while (getline(&s, &n, f) != -1) {
        for (p = s; p[1]; p++)
            if (islower(*p) && islower(p[1]) && bg[*p - 'a'][p[1] - 'a'] < m)
                break;
        if (!p[1])
            fputs(s, stdout);
    }

    free(s);
    fclose(f);
    return 0;
}
