# https://www.reddit.com/r/dailyprogrammer/comments/31aja8/20150403_challenge_208_hard_the_universal_machine/
# assumes valid input. run as: awk -f machine.awk inputfile
NR == 3 { state = $0 }
NR == 4 { endstate = $0 }
NR == 5 {
    head = center = min = max = 0
    for (i = 0; i < length; i++)
        tape[i] = substr($0, i + 1, 1)
}
NR > 5 {
    outstate[$1" "$2] = $4
    outsym  [$1" "$2] = $5
    headdir [$1" "$2] = $6
}
END {
    while (state != endstate) {
        if (tape[head] == "")
            tape[head] = "_"
        statesym   = state" "tape[head]
        state      = outstate[statesym]
        tape[head] = outsym  [statesym]
        head      += (headdir[statesym] == ">") - (headdir[statesym] == "<")
        if (head < min) min = head
        if (head > max) max = head
    }
    for (beg = min; tape[beg] == "_" && beg != head && beg != center; beg++);
    for (end = max; tape[end] == "_" && end != head && end != center; end--);
    for (i = beg; i <= end; i++)
        printf("%s", tape[i])
    printf("\n")
    for (i = beg; i <= end; i++)
        if      (i == center) printf("|")
        else if (i == head  ) printf("^")
        else                  printf(" ")
    printf("\n")
}
