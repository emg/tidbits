/* https://www.reddit.com/r/dailyprogrammer/comments/2yquvm/20150311_challenge_205_intermediate_rpn/ */
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* operator tokens represent themselves */
enum {
    NUM = CHAR_MAX + 1,
    ERR,
};
int val;

int lex(void) {
    int c;

    while (isblank(c = getchar())) /* ignore whitespace */
        ;
    if (c == EOF)
        return EOF;
    if (isdigit(c)) { /* got a number */
        val = 0;
        do val = val * 10 + c - '0';
        while (isdigit(c = getchar()));
        ungetc(c, stdin);
        return NUM;
    }
    if (!strchr("+-/x*()\n", c)) {/* bad token */
        fprintf(stderr, "bad character: '%c'(%d)\n", c, c);
        return ERR;
    }
    return c;
}

/* https://en.wikipedia.org/wiki/Shunting-yard_algorithm */
int parse(void)
{
    char prec[] = {
        ['+'] = 1, ['-'] = 1,
        ['*'] = 2, ['x'] = 2, ['/'] = 2,
    };
    int stack[100], *top = stack, tok; /* FIXME: fixed size stack... */

    while ((tok = lex()) != EOF && tok != '\n' && tok != ERR) {
        switch (tok) {
        case NUM: printf("%d ", val); break;
        case '(': *top++ = tok; break;
        case ')': if (top[-1] == '(') {
                      fprintf(stderr, "empty ()\n");
                      return ERR;
                  }
                  while (--top >= stack && *top != '(')
                      printf("%c ", *top);
                  if (top < stack) {/* no open paren */
                      fprintf(stderr, "extra )\n");
                      return ERR;
                  }
                  break;
        default : while (--top >= stack && prec[tok] <= prec[*top])
                      printf("%c ", *top);
                  top++;
                  *top++ = tok;
                  break;
        }
    }
    if (tok == ERR)
        return ERR;
    while (--top >= stack) {
        if (*top == '(') { /* no close paren */
            fprintf(stderr, "missing )\n");
            return ERR;
        } else {
            printf("%c ", *top);
        }
    }
    return tok != EOF;
}

int main(void)
{
    while (parse())
        printf("\n");
    return 0;
}
