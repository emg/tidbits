#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    if (argc != 3)
        return 1;

    int i, j, m = atoi(argv[1]), n = atoi(argv[2]);
    char arrows[] = "<^>v";

    /* width height */
    printf("%d %d\n", m, n);
    for (i = 0; i < n; i++) {
        for (j = 0; j < m; j++) /* NOTE: haven't seeded */
            putchar(arrows[rand() % 4]);
        putchar('\n');
    }
    return 0;
}
