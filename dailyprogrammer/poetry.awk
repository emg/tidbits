#!/usr/bin/nawk -f
# https://www.reddit.com/r/dailyprogrammer/comments/3bzipa/20150703_challenge_221_hard_poetry_in_a_haystack/
# awk -F '[^[:alpha:]]' 'NR==FNR{w[tolower($0)];next};{for(i=1;i<=NF;i++)if($i&&!($i in w))next};1' /usr/share/dict/words-insane poetry.txt
# USAGE: ./poetry dictionary gibberish
# /usr/share/dict/words (86747 words added) is missing a few words
# /usr/share/dict/words-insane (500945 words added) has them all
# iliad.mb.txt (7467 words added) has them all

BEGIN {
    FS = "[^[:alpha:]]"
}

NR == FNR {
    for (i = 1; i <= NF; i++)
        w[tolower($i)]
    next
}

{
    for (i = 1; i <= NF; i++)
        if ($i && !($i in w))
            next
    print
}
