# NOTE: fflush is a gnuism, hence the .gawk extension and .gawk shebang in the
#       .gawk rule in the Makefile
function hand() {
    print hands[int(rand() * nhands) + 1]
    fflush()
}
BEGIN {
    nhands = split("Rock Paper Scissors Lizard Spock", hands)
    hand()
}
{
    hand()
}
