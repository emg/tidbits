/* Always play Spock */

#include <stdio.h>

int main(void)
{
    char buf[] = "Scissors\n"; /* longest hand */
    for (;;) {
        printf("Spock\n");
        fflush(stdout);
        fgets(buf, sizeof(buf), stdin);
    }
    return 0; /* not reached */
}
