#define _POSIX_C_SOURCE 200809L
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <search.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/wait.h>

#include "rpsls.h"

typedef struct {
    char *name;
    FILE *fa[2]; /* [0] = from bot, [1] = to bot */
    pid_t pid;
    int   score;
} Bot;

Bot b1, b2;

void err(char *fmt, ...)
{
    va_list ap;
    int     e = errno;

    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    if (fmt[strlen(fmt) - 1] == ':')
        fprintf(stderr, " %s\n", strerror(e));

    exit(1);
}

void cleanup(void)
{
    /* don't worry about closing file descriptors and streams, just kill the
     * bot processes */
    if (signal(SIGCHLD, SIG_IGN) == SIG_ERR)
        fprintf(stderr, "signal: %s\n", strerror(errno));

    if (b1.pid > 0) {
        kill(b1.pid, SIGTERM);
        waitpid(b1.pid, NULL, 0);
    }
    if (b2.pid > 0) {
        kill(b2.pid, SIGTERM);
        waitpid(b2.pid, NULL, 0);
    }
}

void startbot(Bot *bot)
{
    int tobot[2], toeng[2];

    if (pipe(tobot)) err("pipe:");
    if (pipe(toeng)) err("pipe:");

    if ((bot->pid = fork()) < 0) {
        err("fork:");
    } else if (!bot->pid) { /* child, bot */
        if (dup2(tobot[0], 0) < 0) err("dup2:");
        if (dup2(toeng[1], 1) < 0) err("dup2:");
        if (execl(bot->name, bot->name, NULL)) {
            fprintf(stderr, "execl: %s\n", strerror(errno));
            _exit(1); /* can't use exit after failed exec */
        }
    } else { /* parent, engine */
        if (!(bot->fa[0] = fdopen(toeng[0], "r"))) err("fdopen:");
        if (!(bot->fa[1] = fdopen(tobot[1], "w"))) err("fdopen:");
        if (setvbuf(bot->fa[0], NULL, _IOLBF, BUFSIZ)) err("setvbuf:");
        if (setvbuf(bot->fa[1], NULL, _IOLBF, BUFSIZ)) err("setvbuf:");
        bot->score = 0;
    }
}

int handcmp(int hand1, int hand2)
{
    return WIN(hand1, hand2) - WIN(hand2, hand1);
}

int scmp(const void *p1, const void *p2)
{
    return strcmp(*(char **)p1, *(char **)p2);
}

int readhand(Bot *bot)
{
    char buf[] = "scissors\n", *p = buf; /* longest hand */
    size_t len;
    char **hand;

    if (!fgets(buf, sizeof(buf), bot->fa[0]))
        err("fgets:");

    len = strlen(buf) - 1;
    if (buf[len] != '\n')
        err("line too long: %s: '%s...'\n", bot->name, buf);
    else
        buf[len] = '\0';

    len = sizeof(names) / sizeof(*names);
    if (!(hand = lfind(&p, names, &len, sizeof(*names), scmp)))
        err("invalid hand: %s: '%s'\n", bot->name, buf);

    return hand - names;
}

int main(int argc, char **argv)
{
    int sigs[] = {
        SIGTERM,
        SIGINT,
        SIGCHLD,
    };

    if (argc != 4)
        return 1;

    for (size_t i = 0; i < sizeof(sigs)/sizeof(*sigs); i++)
        if (signal(sigs[i], exit) == SIG_ERR)
            err("signal:");

    if (atexit(cleanup))
        err("atexit:");

    b1.name = argv[1];
    b2.name = argv[2];

    startbot(&b1);
    startbot(&b2);

    for (int nhands = atoi(argv[3]); nhands; nhands--) {
        int hand1 = readhand(&b1);
        int hand2 = readhand(&b2);

        switch (handcmp(hand1, hand2)) {
            case  1: b1.score++; break;
            case -1: b2.score++; break;
        }

        if (fprintf(b2.fa[1], "%s\n", names[hand1]) < 0) err("fputs:");
        if (fprintf(b1.fa[1], "%s\n", names[hand2]) < 0) err("fputs:");
    }
    printf("%s: %d\n%s: %d\n", b1.name, b1.score, b2.name, b2.score);

    return 0;
}
