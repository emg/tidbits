/* https://www.reddit.com/r/dailyprogrammer/comments/32vlg8/20150417_challenge_210_hard_loopy_robots/
 * directions
 *     +y
 *      0
 * -x 3 B 1 +x
 *      2
 *     -y
 */
#include <stdio.h>

int main(int argc, char **argv)
{
    int nloops[] = { 1, 4, 2, 4 };
    int x = 0, y = 0, dir = 0;

    if (argc != 2) {
        fprintf(stderr, "supply moves as single argument\n");
        return 1;
    }
    for (char *p = argv[1]; *p; p++) {
        switch (*p) {
        default : fprintf(stderr, "invalid move: '%c'\n", *p);
                  return 2;
        case 'S': x += (dir == 1) - (dir == 3);
                  y += (dir == 0) - (dir == 2);
                  break;
        case 'L': dir = (dir + 3) % 4; break;
        case 'R': dir = (dir + 1) % 4; break;
        }
    }
    if (!dir && (x || y))
        printf("No loop detected!\n");
    else
        printf("Loop detected! %d cycle(s) to complete loop\n", nloops[dir]);

    return 0;
}
