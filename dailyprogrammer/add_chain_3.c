/* https://www.reddit.com/r/dailyprogrammer/comments/2y5ziw/20150306_challenge_204_hard_addition_chains/ */
/* versions:
 * 0: star chains counting up
 * 1: star chains counting up pruning if chain[depth] * (1 << len - depth) < goal
 * 2: star chains counting down pruning as above
 * 3: addition chains as above with iterative deepening
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

void print(int *chain, int len)
{
    for (int i = 0; i < len; i++)
        printf("%d,", chain[i]);
    printf("\n");
}

int solve(int *chain, int len, int depth, int goal)
{
    if (depth + 1 == len && chain[depth] == goal) {
        print(chain, len);
        return 1;
    }

    if (chain[depth] * (1 << len - depth) < goal ||
        chain[depth] > goal ||
        depth + 1 == len)
        return 0;

    for (int i = depth; i >= 0 && chain[i] * 2 > chain[depth]; i--) {
        int sum;
        for (int j = i; j >= 0 && (sum = chain[i] + chain[j]) > chain[depth]; j--) {
            chain[depth + 1] = sum;
            if (solve(chain, len, depth + 1, goal))
                return 1;
        }
    }
    return 0;
}

int main(int argc, char **argv)
{
    if (argc != 2)
        return 1;

    for (int goal = atoi(argv[1]), len = 1;; len++) {
        printf("trying length %d\n", len);

        int chain[len + 1];
        chain[0] = 1;
        chain[1] = 2;
        if (solve(chain, len + 1, 1, goal))
            return 0;
    }
    return 1; /* not reached */
}
