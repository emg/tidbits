/*
 * mkfifo f1 f2 f3 f4
 * ./distribute f1 f2 f3 f4 < file
 *
 * distribute lines from file to named fifos when they are available to write
 */
#define _POSIX_C_SOURCE 200809L
#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/select.h>

int main(int argc, char ** argv)
{
	FILE *fifos[argc - 1];
	fd_set writefds;
	char buf[128];
	int i, nfds, fds[argc - 1];

	for (i = nfds = 0; i < argc - 1; i++) {
		fifos[i] = fopen(argv[i + 1], "w");
		if (!fifos[i])
			err(EXIT_FAILURE, "failed to open %s", argv[i + 1]);
		fds[i] = fileno(fifos[i]);
		if (fds[i] > nfds)
			nfds = fds[i];
	}
	nfds++;

	for (;;) {
		FD_ZERO(&writefds);

		for (i = 0; i < argc - 1; i++)
			FD_SET(fds[i], &writefds);

		if (select(nfds, NULL, &writefds, NULL, NULL) < 0)
			err(EXIT_FAILURE, "select() failed");

		for (i = 0; i < argc - 1; i++) {
			if (FD_ISSET(fds[i], &writefds)) {
				if (!fgets(buf, sizeof(buf), stdin))
					goto cleanup;
				fputs(buf, fifos[i]);
				fflush(fifos[i]);
			}
		}
	}

cleanup:
	for (i = 0; i < argc - 1; i++)
		fclose(fifos[i]);

	return 0;
}
