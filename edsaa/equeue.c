#include <stdlib.h>
#include "edsaa.h"

int
eqinit(eQueue *q, size_t cap)
{
	if (!(q->buf = malloc(cap * sizeof(*q->buf))))
		return -1;

	q->beg = q->end = q->buf;
	q->cap = cap;

	return 0;
}

void
eqdestroy(eQueue *q)
{
	free(q->buf);
}

int
eqisempty(eQueue *q)
{
	return !q->len;
}

int
eqisfull(eQueue *q)
{
	return q->len == q->cap;
}

void
eqpush(eQueue *q, eDatum dat)
{
	*q->end++ = dat;

	if (q->end == q->buf + q->cap)
		q->end = q->buf;

	++q->len;
}

void
eqpop(eQueue *q, eDatum *dat)
{
	*dat = *q->beg++;

	if (q->beg == q->buf + q->cap)
		q->beg = q->buf;

	--q->len;
}
