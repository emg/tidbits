#include <stdint.h>
#include <pthread.h>

typedef union {
	void    *p;
	intptr_t i;
    double   f;
} eDatum;

typedef struct {
    eDatum *buf;
    eDatum *beg;
    eDatum *end;
	size_t  cap;
	size_t  len;
} eQueue;

typedef struct {
	eQueue          q;
    pthread_cond_t  rcnd;
    pthread_cond_t  wcnd;
    pthread_mutex_t mtx;
} eTSQueue;

int  eqinit   (eQueue *q, size_t cap); /* return 0 on success, -1 on error */
void eqdestroy(eQueue *q);
int  eqisempty(eQueue *q);
int  eqisfull (eQueue *q);
void eqpush   (eQueue *q, eDatum dat);
void eqpop    (eQueue *q, eDatum *dat);

int etsqinit   (eTSQueue *tsq, size_t size);
int etsqdestroy(eTSQueue *tsq);
int etsqpush   (eTSQueue *tsq, eDatum dat);
int etsqpop    (eTSQueue *tsq, eDatum *dat);
