#include "edsaa.h"

int
etsqinit(eTSQueue *tsq, size_t size)
{
	int err = eqinit(&tsq->q, size);

	if (err)
		return err;

	if ((err = pthread_mutex_init(&tsq->mtx, NULL)))
		return err;

	if ((err = pthread_cond_init(&tsq->rcnd, NULL)))
		return err;

	if ((err = pthread_cond_init(&tsq->wcnd, NULL)))
		return err;

	return 0;
}

int
etsqdestroy(eTSQueue *tsq)
{
	int err = pthread_cond_destroy(&tsq->wcnd);

	if (err)
		return err;

	if ((err = pthread_cond_destroy(&tsq->rcnd)))
		return err;

	if ((err = pthread_mutex_destroy(&tsq->mtx)))
		return err;

	eqdestroy(&tsq->q);

	return 0;
}

int
etsqpush(eTSQueue *tsq, eDatum dat)
{
	int err = pthread_mutex_lock(&tsq->mtx);

	if (err)
		return err;

	while (eqisfull(&tsq->q))
		if ((err = pthread_cond_wait(&tsq->wcnd, &tsq->mtx)))
			return err;

	eqpush(&tsq->q, dat);

	if ((err = pthread_cond_signal(&tsq->rcnd)))
		return err;

	if ((err = pthread_mutex_unlock(&tsq->mtx)))
		return err;

	return 0;
}

int
etsqpop(eTSQueue *tsq, eDatum *dat)
{
	int err = pthread_mutex_lock(&tsq->mtx);

	if (err)
		return err;

	while (eqisempty(&tsq->q))
		if ((err = pthread_cond_wait(&tsq->rcnd, &tsq->mtx)))
			return err;

	eqpop(&tsq->q, dat);

	if ((err = pthread_cond_signal(&tsq->wcnd)))
		return err;

	if ((err = pthread_mutex_unlock(&tsq->mtx)))
		return err;

	return 0;
}
