#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int (*Inst)(void);
#define STOP (Inst)0

Inst  *prog, *pc, *ni; /* program, program counter, next instruction spot */
char  *bfm , *p;       /* bf memory, bf pointer */
size_t prog_size, bfm_size;

int grow(void **ptr, size_t *nmemb, size_t size, void **half, int clear)
{
    size_t n   = *nmemb * 2;
    void  *tmp = realloc(*ptr, n * size);
    if (!tmp) { perror("realloc failed"); return 1; }
    *nmemb = n;
    *ptr   = tmp;
    *half  = (char *)*ptr + n * size / 2;
    if (clear) memset(*half, 0, n * size / 2);
    return 0;
}
int code(Inst f)
{
    if (ni == prog + prog_size)
        if (grow((void **)&prog, &prog_size, sizeof(*prog), (void **)&ni, 0))
            return 1;
    *ni++ = f;
    return 0;
}
int incp (void)
{
    if (++p != bfm + bfm_size) return 0;
    return grow((void **)&bfm, &bfm_size, sizeof(*bfm), (void **)&p, 1);
}
int decp (void) { if (--p >= bfm) return 0; fprintf(stderr, "p < 0\n"); return 1; }
int incat(void) { ++*p;                                                 return 0; }
int decat(void) { --*p;                                                 return 0; }
int putch(void) { putchar(*p); fflush(stdout);                          return 0; }
int getch(void) { int c = getchar(); if (c != EOF) *p = c;              return 0; }
int lbrac(void) { pc++; if (!*p) pc = (Inst*)(prog + *(ptrdiff_t *)pc); return 0; }
int rbrac(void) { pc++; if ( *p) pc = (Inst*)(prog + *(ptrdiff_t *)pc); return 0; }

int main(int argc, char **argv)
{
    int  c, e, b = 0;
    Inst insts[] = { ['>'] = incp , ['<'] = decp , ['+'] = incat, ['-'] = decat,
                     ['.'] = putch, [','] = getch, ['['] = lbrac, [']'] = rbrac,
                     [UCHAR_MAX] = NULL };
    FILE *script = argc > 1 ? fopen(argv[1], "r") : stdin;

    if (!script) { perror("fopen failed"); return 1; }

    p  = bfm  = calloc(bfm_size  = 1, sizeof(*bfm ));
    ni = prog = calloc(prog_size = 1, sizeof(*prog));

    while ((c = fgetc(script)) != EOF) {
        if (!insts[c]) continue;
        if (!(e = code(insts[c]))) {
            switch (c) {
                case '[':     ++b;        e = code(STOP);  break;
                case ']': if (--b < 0 || (e = code(STOP))) break;
                              for (pc = ni - 2; pc > prog && *pc != STOP; pc--)
                                  ; //TODO: stack (is it worth it?)
                          *(ptrdiff_t *)pc       = (ni - 1) - prog;
                          *(ptrdiff_t *)(ni - 1) = pc       - prog;
                          break;
            }
        }
        if (b < 0 || e) break;
    }
    e = code(STOP);
    if (b)
        fprintf(stderr, "extra %c\n", b < 0 ? ']' : '[');
    else if (!e)
        for (pc = prog; *pc != STOP; pc++)
            if ((*pc)())
                break;
    if (fclose(script)) perror("fclose failed");
    free(bfm);
    free(prog);
    return 0;
}
