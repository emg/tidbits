#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef union {
	enum { stop = 0, incp, decp, incat, decat, putch, getch, lbrac, rbrac } inst;
	ptrdiff_t addr;
} Inst;
#define STOP ((Inst){ .inst = stop })

Inst  *prog, *pc, *ni; /* program, program counter, next instruction spot */
char  *bfm , *p;       /* bf memory, bf pointer */
size_t prog_size, bfm_size;

int grow(void **ptr, size_t *nmemb, size_t size, void **half, int set)
{
	void *tmp = realloc(*ptr, (*nmemb *= 2) * size);
	if (!tmp) { perror("realloc failed"); return 1; }
	*ptr = tmp;
	*half = (char *)*ptr + *nmemb * size / 2;
	if (set) memset(*half, 0, *nmemb * size / 2);
	return 0;
}

int code(Inst f)
{
	if (ni == prog + prog_size)
		if (grow((void **)&prog, &prog_size, sizeof(*prog), (void **)&ni, 0))
			return 1;
	*ni++ = f;
	return 0;
}

int main(int argc, char **argv)
{
	int insts[] = { ['>'] = incp , ['<'] = decp , ['+'] = incat, ['-'] = decat,
	                ['.'] = putch, [','] = getch, ['['] = lbrac, [']'] = rbrac,
					[UCHAR_MAX] = 0 };
	int  c, b = 0, e = 0;
	FILE *script = argc > 1 ? fopen(argv[1], "r") : stdin;

	if (!script) { perror("fopen failed"); return 1; }

	p  = bfm  = calloc(bfm_size  = 1, sizeof(*bfm ));
	ni = prog = calloc(prog_size = 1, sizeof(*prog));

	while ((c = fgetc(script)) != EOF) {
		if (!insts[c]) continue;
		if (!(e = code((Inst){ .inst = insts[c] }))) {
			switch (c) {
				case '[':     ++b;        e = code(STOP);  break;
				case ']': if (--b < 0 || (e = code(STOP))) break;
					for (pc = ni - 2; pc > prog && pc->inst != stop; pc--)
						; //TODO: stack (is it worth it?)
					pc->addr       = (ni - 1) - prog;
					(ni - 1)->addr = pc       - prog;
					break;
			}
		}
		if (b < 0 || e) break;
	}
	if (b) fprintf(stderr, "extra %c\n", b < 0 ? ']' : '[');
	else if (!e) {
		e = code(STOP);
		for (pc = prog; !e && pc->inst != stop; pc++) {
			switch (pc->inst) {
				default   : e++; fprintf(stderr, "bad instruction\n"); break;
				case incp :
					if (++p == bfm + bfm_size)
						e = grow((void **)&bfm, &bfm_size, sizeof(*bfm), (void **)&p, 1);
					break;
				case decp : if ((e = --p < bfm)) fprintf(stderr, "p < 0\n"); break;
				case incat: ++*p;                                break;
				case decat: --*p;                                break;
				case putch: putchar(*p); fflush(stdout);         break;
				case getch: if ((c = getchar()) != EOF) *p = c;  break;
				case lbrac: pc++; if (!*p) pc = prog + pc->addr; break;
				case rbrac: pc++; if ( *p) pc = prog + pc->addr; break;
			}
		}
	}
	if (fclose(script)) perror("fclose failed");
	free(bfm);
	free(prog);
	return 0;
}
