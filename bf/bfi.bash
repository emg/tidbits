#!/usr/bin/env bash

# brainfuck interpreter
# usage: ./bfi script
# in order to get line based input from stdin, cat | ./bfi script
# easier than dealing with nul bytes on input in bash

incp() { ((++p)); }
decp() { ((p--)) && return 0; echo "p < 0" 1>&2; return 1; }
incat(){ ((++bfm[p])); return 0; }
decat(){ ((--bfm[p])); return 0; }
putch(){ local LC_CTYPE=C; printf %b "\x$(printf %x "${bfm[p]}")"; }
getch(){ local LC_CTYPE=C c; IFS= read -r -n 1 -d '' c && printf -v bfm[p] %d "'$c"; return 0; }
lbrac(){ ((++pc)); ((!bfm[p] && (pc = prog[pc]))); return 0; }
rbrac(){ ((++pc)); (( bfm[p] && (pc = prog[pc]))); return 0; }

code() {
	prog[ni++]=$1;
	case "$1" in
		lbrac) stack+=($ni); code $stop;;
		rbrac)
			((${#stack[@]})) || return 1;
			((prog[${stack[@]: -1}] = ni))
			code "${stack[@]: -1}"
			unset stack[${#stack[@]}-1]
			;;
	esac
}

read_script() {
	[[ -r "$1" ]] || { echo "cannot read $1" 1>&2; return 1; }

	local c b stack;
	declare -A insts=([>]=incp  [<]=decp   [+]=incat  [-]=decat
	                  [.]=putch [,]=getch [\[]=lbrac [\]]=rbrac)

	while IFS= read -r -n 1 -d '' c; do
		[[ ${insts[$c]} ]] || continue
		code "${insts[$c]}" || { echo "extra ]" 2>&1; return 1; }
	done < "$1"

	((!${#stack[@]})) || { echo "extra [" 2>&1; return 1; }
	code $stop
}

run_script() {
	for ((pc = 0; prog[pc] != stop; pc++)); do
		"${prog[$pc]}" || break
	done
}

if (($# != 1)); then
	echo "Usage: ./bfi scriptfile" 1>&2
	exit 1
fi

stop=-1 prog=() ni=0 bfm=() p=0
read_script "$1" && run_script
