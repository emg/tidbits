/* backstory:
 * Wrote a program to solve the Rubik's cube. A big part is table lookup (both
 * when generating distance/pruning tables and when solving). Table sizes were
 * known at compile time making lookup fast.
 *
 * Writing a new program for cubes of arbitrary size (and some puzzles that
 * aren't cubes) to generate tables and solve puzzles generically. As part of
 * it I need to be able to generate and use tables of arbitrary dimensions.
 * Lookups in these tables and incrementing coordinates for the lookups are
 * both slow. How can I speed them up?
 *
 * each entry in the table is 2 bits, hence the shifting and masking
 *
 * examples of both approaches below with FIXME tags on the slow parts
 * the slow example is roughly 4x slower using gcc -O3
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <sys/times.h>

#define die(s) do{ perror(s); exit(1); }while(0)

void time(void (*func)(void))
{
    clock_t r0, r1;
    struct tms t0, t1;
    long ticks;

    if ((ticks = sysconf(_SC_CLK_TCK)) <= 0)
        die("sysconf");
    if ((r0 = times(&t0)) < 0)
        die("times");

    func();

    if ((r1 = times(&t1)) < 0)
        die("times");
    printf("real %f\nuser %f\nsys  %f\n\n",
           (r1           - r0          ) / (double)ticks,
           (t1.tms_utime - t0.tms_utime) / (double)ticks,
           (t1.tms_stime - t0.tms_stime) / (double)ticks);
}

/* old/fast example
 *
 * do GET_ENT() for all entries
 * keep track of how many entries are 0,1,2,3 in counts so we do something and
 * don't get "statement with no effect" warnings and optimized out
 *
 * This is an example of how the old program was written. An array of known
 * dimensions, normal array lookups.
 */
uint8_t a[(2187U*2048*495 + 3) / 4];
#define GET_ENT3(a, i, j, k) (((a)[((i)*2048*495 + (j)*495 + (k)) / 4] >> (((i)*2048*495 + (j)*495 + (k)) % 4 * 2)) & 3)

void fast_example(void)
{
    printf("fast: size:%zu ", sizeof(a));

    size_t counts[] = { 0, 0, 0, 0 };
    size_t nent = 0;

    /* so we have something to look at */
    memset(a, 0xE4, sizeof(a));

    for (size_t i = 0; i < 2187; i++) {
        for (size_t j = 0; j < 2048; j++) {
            for (size_t k = 0; k < 495; k++) {
                counts[GET_ENT3(a, i, j, k)]++;
                nent++;
            }
        }
    }

    printf("entries:%zu ", nent);
    for (size_t i = 0; i < 4; i++)
        printf("%zu:%zu ", i, counts[i]);
    printf("\n");
}

/* simulate a multidimensional array with arbitrary number of dimensions and
 * arbitrary dimensions
 * each entry in the table is 2 bits
 * get and set entries using an array of coordinates
 *
 * Someone suggested trying strides as a flexible array member instead of
 * allocating in attempt to reduce redirections. Also tried data as flexible
 * array member instead. In both cases no measuarable difference. (although now
 * I have to allocate the struct instead of using one with local storage in
 * slow_example()...)
 */
typedef struct {
    uint8_t *data;
    size_t  *dims;
    size_t   ndim;
    size_t   strides[];
} Multid;

Multid *init(size_t ndim, size_t *dims)
{
    size_t size = 1;
    Multid *m = malloc(sizeof(*m) + ndim * sizeof(*m->strides));

    if (!m)
        die("malloc");
    if (!(m->dims = malloc(ndim * sizeof(*m->dims))))
        die("malloc");

    m->ndim = ndim;
    for (size_t i = 0; i < ndim; i++) {
        m->dims[i] = dims[i];
        size *= dims[i];
    }

    /* size of m->data as each entry is 2 bits, round up */
    size = (size + 3) / 4;
    printf("size:%zu ", size);

    if (!(m->data = malloc(size * sizeof(*m->data))))
        die("malloc");

    /* so we have something to look at */
    memset(m->data, 0xE4, size * sizeof(*m->data));

    /* find coeffecients for each dimension so we don't have to do this
     * multiplication every time we perform a lookup */
    for (size_t i = m->ndim, mul = 1; i--; mul *= m->dims[i])
        m->strides[i] = mul;

    return m;
}


/* serves the same purpose as the GET_ENT() macro did when array dimensions
 * were known
 * FIXME: this is slow
 */
uint8_t get_ent(Multid *m, size_t *coords)
{
    size_t off = 0, shift;

    for (size_t i = m->ndim; i--; off += m->strides[i] * coords[i])
        ;
    shift = off % 4 * 2;
    off /= 4;

    return (m->data[off] >> shift) & 3;
}

/* count in a variable base defined by m->dims
 * used to loop through tables of arbitrary dimensions
 *
 * for (i = 0; i < maxi; i++)
 *     for (j = 0; j < maxj; j++)
 *         for (k = 0; k < maxk; k++)
 *             func(i, j, k);
 *
 * essentially becomes
 *
 * ind[] = {    0,    0,    0 };
 * max[] = { maxi, maxj, maxk };
 * do func(ind); while (inc_coords(ind, max));
 *
 * FIXME: this is slow
 */
int inc_coords(Multid *m, size_t *coords)
{
    int carry = 1;
    for (size_t n = m->ndim; n-- && carry; carry = !coords[n])
        coords[n] = (coords[n] + 1) % m->dims[n];
    return !carry;
}

/* do get_ent() for all entries
 * keep track of how many entries are 0,1,2,3 in counts so we do something and
 * don't get optimized out in fast_example()
 *
 * This is an example of how the new program is written, create a table with
 * given dimensions, use get_ent() and inc_coords() independent of those
 * dimensons.
 *
 * In reality this will not be strictly incremental access, get_ent() will be
 * called roughly 20 times per loop with different coordinates
 */
void slow_example(void)
{
    printf("slow: ");

    Multid *m = init(3, (size_t[]){ 2187, 2048, 495 });
    size_t coords[] = { 0, 0, 0 };
    size_t counts[] = { 0, 0, 0, 0 };
    size_t nent = 0;

    do {
        counts[get_ent(m, coords)]++;
        nent++;
    } while (inc_coords(m, coords));

    printf("entries:%zu ", nent);
    for (size_t i = 0; i < 4; i++)
        printf("%zu:%zu ", i, counts[i]);
    printf("\n");

    free(m->data);
    free(m->dims);
    free(m);
}

int main(void)
{
    setbuf(stdout, NULL);
    time(fast_example);
    time(slow_example);
    return 0;
}
