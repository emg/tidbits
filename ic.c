/*
 * ic: interactive calculator
 *
 * ic is an interactive RPN calculator with fixed stack size. As it is meant for
 * interactive use, it reads keys as they are pressed instead of waiting for a
 * newline and instantly updates the stack. It behaves like HP's RPN
 * caclulators. It's designed for use the numpad. More functions are available
 * by putting the left hand on homerow.
 *
 * If an argument is provided to ic it is used as the stack size. The stack size
 * defaults to 4.
 *
 * Digits, decimal point, arithmetic characters, enter, all work as expected.
 *
 * Other keys:
 *
 * [q][w][e][r]         quit  swap  EEX   rot
 *  [a][s][d][f]          add   sub   div   mul
 *   [z][x][c][v]           <--   y^x   CHS   sqrt
 *
 * [q] quit the calculator
 * [w] sWap x<->y
 * [e] EEX (not implemented yet)
 * [r] rotate stack down
 * [R] rotate stack up
 *
 * [a] add
 * [s] subtract
 * [d] divide
 * [f] multiply    (think factor maybe?)
 *
 * [z] backspace   (think ^Z is undo)
 * [x] y^x         (think eXponent)
 * [c] change sign
 * [v] square root (think v looks like square root sign, or v is sqrt in dc)
 *
 */

#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>
#include <termios.h>

struct termios *defaultterm, old;

double *stack;
size_t stacksize = 4;
char numbuf[32], *nump;
int innum, inexp, infrac, sign = 1;

static noreturn void
die(char *fmt, ...)
{
	int e = errno;

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (*fmt && fmt[strlen(fmt)-1] == ':')
		fprintf(stderr, " %s", strerror(e));

	fputc('\n', stderr);
	exit(1);
}

static void
cleanup(void)
{
	if (defaultterm)
		tcsetattr(0, TCSANOW, defaultterm);
	putchar('\n');
}

static void
lift(void)
{
	for (double *p = stack + stacksize - 1; p > stack; p--)
		*p = p[-1];
	*stack = 1; /* used as sign for input */
}

static void
drop(void)
{
	for (double *p = stack; p < stack + stacksize - 1; p++)
		*p = p[1];
}

static void
rotatedown(void)
{
	double tmp = *stack;
	drop();
	stack[stacksize-1] = tmp;
}

static void
rotateup(void)
{
	double tmp = stack[stacksize-1];
	lift();
	*stack = tmp;
}

/* put the input buffer into the x register */
static void
intox(void)
{
	if (inexp) {
		*stack = *stack * pow(10, sign * atof(numbuf));
	} else if (innum) {
		*stack = sign * atof(numbuf);
	} else {
		die("called intox when not innum");
	}
	innum = 0;
	inexp = 0;
	infrac = 0;
	nump = numbuf;
	*nump = 0;
}

static void
draw(void)
{
	printf("\033[%zuA\r\n", stacksize);

	for (double *p = stack + stacksize - 1; p > stack; p--)
		printf("%G\033[K\r\n", *p);

	if (inexp)
		printf("%GE", *stack);

	if (innum) {
		if (sign < 0)
			putchar('-');
		fputs(numbuf, stdout);
	} else {
		if (*stack == 0)
			*stack = 0;
		printf("%G", *stack);
	}

	fputs("\033[K", stdout);
}

static void
run(void)
{
	double tmp;
	int last, c = 0;

	for (;;) {
		draw();

		last = c;
		c = getchar();

		if (c == 'e' || c == 'E') {
			if (innum)
				intox();
			inexp = 1;
			innum = 1;
			sign = 1;
			continue;
		}

		if (c == '.') {
			if (infrac)
				continue;
			infrac = 1;
		}

		/* TODO: stop more than one decimal point */
		if (isdigit(c) || c == '.') {
			if (!innum) {
				innum = 1;
				sign = 1;
				if (last != '\n')
					lift();
			}
			if (nump < numbuf + sizeof(numbuf)/sizeof(*numbuf) - 1){ /* 1 byte to terminate */
				*nump++ = c;
				*nump = 0;
			}
			continue;
		}

		/* add, subtract, multiply, divide, y^x */
		if (strchr("+-*/asdfx", c)) {
			if (innum)
				intox();
			switch (c) {
				/* asdf: add subtract divide ... factor? */
				case '+': case 'a': stack[1] += *stack; break;
				case '-': case 's': stack[1] -= *stack; break;
				case '*': case 'f': stack[1] *= *stack; break;
				case '/': case 'd': stack[1] /= *stack; break;

				/* x: eXponent, y^X */
				case 'x':
					stack[1] = pow(stack[1], *stack);
					break;
			}
			drop();
			continue;
		}

		/* enter, rotate, swap, sqrt */
		if (strchr("\n\rRrwv", c)) {
			if (innum)
				intox();
			switch (c) {
				case '\n':
				case '\r':
					c = '\n';
					lift();
					*stack = stack[1];
					break;

				case 'r':
					rotatedown();
					break;

				case 'R':
					rotateup();
					break;

				case 'w':
					tmp = *stack;
					*stack = stack[1];
					stack[1] = tmp;
					break;

				case 'v':
					*stack = sqrt(*stack);
					break;
			}
			continue;
		}

		/* quit, backspace, change sign */
		switch (c) {
			case 'q':
			case 3:
			case 4:
				return;

			case '\b':
			case 127:
			case 'z':
				if (innum) {
					if (nump > numbuf)
						*--nump = 0;
					if (nump == numbuf) {
						*stack = 0;
						innum = 0;
					}
				} else {
					*stack = 0;
					nump = numbuf;
				}
				break;

			case 'c': /* Change sign */
				if (innum)
					sign *= -1;
				else
					*stack *= -1;
				break;
		}
	}
}

static void
init(void)
{
	if (atexit(cleanup))
		die("atexit:");

	if (tcgetattr(0, &old))
		die("tcgetattr:");

	defaultterm = &old;

	struct termios new;
	new.c_lflag &= ~ICANON;
	new.c_lflag &= ~IGNCR;
	new.c_lflag &= ~ECHO;

	new.c_cc[VMIN ] = 1;
	new.c_cc[VTIME] = 0;

	if (tcsetattr(0, TCSANOW, &new))
		die("tcsetattr:");

	nump = numbuf;
	for (double *p = stack; p < stack + stacksize; p++) {
		*p = 0;
		putchar('\n');
	}
}

int
main(int argc, char *argv[])
{
	if (argc == 2)
		stacksize = atoi(argv[1]);

	double stackbuf[stacksize];
	stack = stackbuf;

	init();
	run();

	return 0;
}
