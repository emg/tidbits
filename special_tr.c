// for memmem()
#define _GNU_SOURCE

#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/stat.h>

int main(int argc, char **argv)
{
    if (argc < 6) return 1; // not enough arguments

    char beg  = **++argv; // begin   group e.g. '('
    char end  = **++argv; // end     group e.g. ')'
    char find = **++argv; // find in group e.g. ' '
    char repl = **++argv; // replace with  e.g. ':'

    while (*++argv) {
        int    fd;
        off_t  size;
        char  *file, *fend, *p, *q;

        if (0 > (fd   = open (*argv, O_RDWR)  )) return 2; // failed to open file
        if (0 > (size = lseek(fd, 0, SEEK_END))) return 3; // failed to find end of file

        if (size == 0) {
            if (0 > close(fd)) return 4;
            continue;
        }

        if (MAP_FAILED == (file = mmap(NULL, size, PROT_WRITE, MAP_SHARED, fd, 0))) return 5; // failed to map file

        for (p = file, fend = p + size; (p = memmem(p, fend - p, &beg, 1)); p = q) {
            if (!(q = memmem(p, fend - p, &end, 1))) return 6; // failed to find matching paren
            while ((p = memmem(p, q - p, &find, 1))) *p = repl;
        }

        if (0 > munmap(file, size)) return 7; // failed to unmap
        if (0 > close (fd)        ) return 8; // failed to close file
    }
    return 0; // success
}
