#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/*
 * ./run echo foo bar
 * sets real uid to effective uid then runs command
 * if setuid bit is set, run commands as another user
 */

int main(int argc, char **argv)
{
	if (setuid(geteuid()))
		fprintf(stderr, "%s: setuid failed: %s\n", *argv, strerror(errno));

	execvp(argv[1], argv + 1);
	fprintf(stderr, "%s: exec failed: %s\n", *argv, strerror(argc = errno));
	return argc;
}
