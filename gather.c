/*
 * mkfifo f1 f2 f3 f4
 * ./gather f1 f2 f3 f4
 *
 * print lines from named fifos as they are available to read
 */
#define _POSIX_C_SOURCE 200809L
#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/select.h>

int main(int argc, char **argv)
{
	FILE *fifos[argc - 1];
	fd_set readfds;
	char buf[128];
	int i, f, nfds, fds[argc - 1];

	for (i = nfds = 0; i < argc - 1; i++) {
		fifos[i] = fopen(argv[i + 1], "r");
		if (!fifos[i])
			err(EXIT_FAILURE, "failed to open %s", argv[i + 1]);
		fds[i] = fileno(fifos[i]);
		if (fds[i] > nfds)
			nfds = fds[i];
	}
	nfds++;

	for (;;) {
		FD_ZERO(&readfds);

		for (i = f = 0; i < argc - 1; i++) {
			if (fifos[i]) {
				FD_SET(fds[i], &readfds);
				f = 1;
			}
		}
		if (!f)
			break;

		if (select(nfds, &readfds, NULL, NULL, NULL) < 0)
			err(EXIT_FAILURE, "select() failed");

		for (i = 0; i < argc - 1; i++) {
			if (FD_ISSET(fds[i], &readfds)) {
				if (!fgets(buf, sizeof(buf), fifos[i])) {
					fclose(fifos[i]);
					fifos[i] = NULL;
				} else {
					printf("%s %s", argv[i + 1], buf);
					fflush(stdout);
				}
			}
		}
	}

	return 0;
}
