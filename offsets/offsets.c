/*
 * offsets.c
 *
 * for each word (grouping of alphanumeric characters) print the word and the
 * byte offset into the file
 * combine with offsets.awk to get a listing of word [offset,...] pairs
 */

#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <wctype.h>

int main(void)
{
	size_t off;
	wint_t c;
	wchar_t buf[256], *p = buf;
	char mbs[MB_CUR_MAX];

	setlocale(LC_ALL,"");

	for (off = 0; (c = fgetwc(stdin)) != WEOF; off += wctomb(mbs, c)) {
		if (iswalnum(c)) {
			*p++ = c;
		} else if (p != buf) {
			*p = 0;
			printf("%ls %zu\n", buf, off - wcstombs(NULL, buf, 0));
			p = buf;
		}
	}
	return 0;
}
