#!/usr/bin/env bash
 
# 
# offsets.bash
# 
# for each word (grouping of alphanumeric characters) print the word and the
# byte offset into the file
# combine with offsets.awk to get a listing of word [offset,...] pairs
# 

print_offsets() {
	local word lc lw
	while IFS= read -r -d '' -n 1 c; do
		lc=$(LC_CTYPE=C; printf %d ${#c})
		if [[ $c == [[:alnum:]] ]]; then
			word+=$c
			((lw += lc))
		elif [[ $word ]]; then
			printf "%s %zu\n" "$word" $((off - lw))
			unset word lw
		fi
		((off += lc))
	done
}

declare -A offsets
while read word off; do
	offsets[$word]+=${offsets[$word]:+,}$off
done < <(print_offsets)

for word in "${!offsets[@]}"; do
	printf "%s [%s]\n" "$word" "${offsets[$word]}"
done
