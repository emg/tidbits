# offsets.awk
#
# given a list of word offset pairs, accumulate offsets and print
# word [offset,...] pairs
# use with offsets.c
# no good way to shebang awk, so just do awk -f manually

{
	a[$1] = a[$1] (a[$1] ? "," : "") $2;
}
END {
	for (w in a)
		printf("%s [%s]\n", w, a[w]);
}
