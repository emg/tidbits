/*
 * sortwords.c
 *
 * Programming Pearls: 2.1.C
 * taking into account current LC_CTYPE and LC_COLLATE:
 * read in word ([:space:] separated),
 * print word followed by space
 * sort characters
 * print sorted word followed by newline
 *
 * use with anagrams.awk to get anagram classes
 */

#include <limits.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wchar.h>
#include <wctype.h>

int wcscoll_cmp(const void *a, const void *b)
{
	return wcscoll((const wchar_t *)a, (const wchar_t *)b);
}

int main(void)
{
	wint_t  wc;
	wchar_t word[sysconf(_SC_LINE_MAX) * 2], *p = word;

	setlocale(LC_ALL, "");
	while ((wc = fgetwc(stdin)) != WEOF) {
		if (iswspace(wc)) {
            fputwc(L' ', stdout);
			qsort(word, (p - word) / 2, sizeof(wchar_t) * 2, wcscoll_cmp);
			for (wchar_t *q = word; q < p; q += 2)
				fputwc(*q, stdout);
			fputwc(L'\n', stdout);
			p = word;
		} else {
            fputwc(wc, stdout);
			*p++ = wc;
			*p++ = L'\0';
		}
	}
	return 0;
}
