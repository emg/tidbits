/******************************************************************************
 * Evan Gates                                                                 *
 * 2012/12/29                                                                 *
 * rpn_spreadsheet.c                                                          *
 *                                                                            *
 * PURPOSE: this program reads in a spreadsheet of positive integers, cell    *
 *          references, and postfix expressions, then outputs the resulting   *
 *          calculations as a spreadsheet of doubles.                         *
 * USAGE  : compile with gcc, supply spreadsheet on stdin                     *
 * EXAMPLE: gcc -o rpn rpn_spreadsheet.c                                      *
 *          ./rpn < test1.in                                                  *
 * NOTES  : tested with gcc 4.7.2 and glibc 2.17                              *
 *                                                                            *
 * Spreadsheet Format                                                         *
 * ------------------                                                         *
 * A spreadsheet consists of a two-dimensional array of cells, labeled A1, A2,*
 * A3, ... Each cell contains either a number (its value) or an expression.   *
 * Expressions contain integers, cell references, and the operators '+', '-', *
 * '*', '/'. There are a maximum of 26 rows. Expressions are given in Reverse *
 * Polish Notation.                                                           *
 *                                                                            *
 * The spreadsheet is represented in a plain text file as follows:            *
 *   - Line 1: two integers, seperated by white space, defining the number of *
 *     columns and rows respectively (n, m)                                   *
 *   - n*m lines each containing an expression which is the value of the      *
 *     corresponding cell (cells enumerated in the order A1, A2, ... , A<n>,  *
 *     B1, B2, ... , B<n>, ...)                                               *
 *                                                                            *
 * Example                                                                    *
 * -------                                                                    *
 * The following spreadsheet:                                                 *
 *                                                                            *
 *             1           2           3                                      *
 *        -----------------------------------                                 *
 *     A |    A2     |   4 5 *   | 1 2 3 + / |                                *
 *        -----------------------------------                                 *
 *     B |  A1 B2 /  |     3     | 4 7 - 3 * |                                *
 *        -----------------------------------                                 *
 *                                                                            *
 * Is represented as:                                                         *
 * 3 2                                                                        *
 * A2                                                                         *
 * 4 5 *                                                                      *
 * 1 2 3 + /                                                                  *
 * A1 B2 /                                                                    *
 * 3                                                                          *
 * 4 7 - 3 *                                                                  *
 *                                                                            *
 * And results in:                                                            *
 * 3 2                                                                        *
 * 20.000000                                                                  *
 * 20.000000                                                                  *
 * 0.200000                                                                   *
 * 6.666667                                                                   *
 * 3.000000                                                                   *
 * -9.000000                                                                  *
 ******************************************************************************/

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_ROWS 26

#define die(fmt, args...) ({                                                                                  \
	fprintf(stderr, "%s: " fmt "%s%s\n", prog_name, ##args, errno ? ": " : "", errno ? strerror(errno) : ""); \
	cleanup();                                                                                                \
	exit(EXIT_FAILURE);                                                                                       \
})

struct stack {
	double *nums;
	int    size;
	int    depth;
};

char   *prog_name = NULL;

char  **sheet[MAX_ROWS] = {}; // read lines   in
double *out  [MAX_ROWS] = {}; // spit doubles out

int     rows = 0, cols = 0;   // size of spreadsheet
int     cycle;                // boolean to find cyclic dependencies
int     cur_row, cur_col;

// free individual cells, then rows
void cleanup(void)
{
	int i, j;

	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++)
			free(sheet[i][j]);
		free(sheet[i]);
		free(out  [i]);
	}
}

// NOTE: used explicitly (outside of push and pop) to free stacks in case of fatal errors
void resize(struct stack *s, int size)
{
	double *tmp;

	if (s->nums == NULL && size == 0) {
		return;                          // called from a fatal error, but stack is already empty
	} else if ((tmp = realloc(s->nums, size * sizeof(double))) == NULL && size != 0) {
		int e = errno;
		free(s->nums);                   // "if realloc() fails the original block is left untouched; it is not freed or moved" from malloc(3)
		errno = e;
		die("failed to realloc stack");
	} else {
		s->nums = size ? tmp : NULL;     // "if size was equal to 0, either NULL or a pointer suitable to be passed to free() is returned" from malloc(3)
		s->size = size;
	}
}

// double size when full
void push(struct stack *s, double d)
{
	if (s->depth == s->size)
		resize(s, s->size ? s->size * 2 : 1);

	s->nums[s->depth++] = d;
}

// halve size when 3/4 empty (free when empty)
double pop(struct stack *s)
{
	double ret = s->nums[--s->depth];

	if (s->depth <= s->size / 4)
		resize(s, s->depth * 2);

	return (ret);
}

// recurse on references, push numbers on stack, pop and operate
// return 0 on success, 1 on cycle, 2 on out of bounds, 3 on malformed token
// NOTE: beware, there are multiple return points
// (for a function this small I prefer that to gotos for errors and cleanup)
enum err_codes {success, err_cycle, err_oobounds, err_badcell};

int eval_cell(int row, int col)
{
	struct stack s = {};
	int i, j, err;
	char *tok, *saveptr;

	if (sheet[row][col] == NULL) // already evaluated due to reference
		return (success);

	if (row == cur_row && col == cur_col && cycle++)
		return (err_cycle);

	for (tok = strtok_r(sheet[row][col], " \t\n", &saveptr); tok; tok = strtok_r(NULL, " \t\n", &saveptr)) {
		switch (*tok) {
			// reference. translate to row col, recurse, push result
			case 'a' ... 'z':
				*tok = toupper(*tok);
			case 'A' ... 'Z':
				i = *tok - 'A';
				j = atoi(tok + 1) - 1;
				if (i < 0 || i >= rows || j < 0 || j >= cols) {
					cur_row = row;
					cur_col = col;
					resize(&s, 0);
					return (err_oobounds);
				}
				if ((err = eval_cell(i, j)) != success) {
					resize(&s, 0);
					return (err);
				}
				push(&s, out[i][j]);
				break;
			// number. push
			case '0' ... '9':
				push(&s, strtod(tok, NULL));
				break;
			// operator. pop 2, operate, push result
			case '+': push(&s,     pop(&s) + pop(&s)); break;
			case '-': push(&s, 0.0-pop(&s) + pop(&s)); break;
			case '*': push(&s,     pop(&s) * pop(&s)); break;
			case '/': push(&s, 1.0/pop(&s) * pop(&s)); break;
			// for debugging purposes
			default :
				cur_row = row;
				cur_col = col;
				resize(&s, 0);
				return (err_badcell);
		}
	}

	out[row][col] = pop(&s);
	free(sheet[row][col]);
	sheet[row][col] = NULL;
	return (success);
}

int main(int argc, char **argv)
{
	int i, j;
	size_t n; // unused, needed for getline(3)

	prog_name = argv[0];

	if (argc != 1)
		die("Do not use arguments. Supply input on stdin.");

	if (fscanf(stdin, "%d %d\n", &cols, &rows) != 2) // specification says width then height, n*m, A<n>
		die("failed to read size of spreadsheet");

	if (rows > MAX_ROWS)
		die("too many rows (maximum %d rows)", MAX_ROWS);

	for (i = 0; i < rows; i++) {
		if ((sheet[i] = calloc(cols, sizeof(char *))) == NULL) die("failed to allocate spreadsheet");
		if ((out  [i] = calloc(cols, sizeof(double))) == NULL) die("failed to allocate spreadsheet");
	}

	// read the spreadsheet
	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			if (getline(&sheet[i][j], &n, stdin) < 0)
				die("failed to read cell %c%d", 'A' + i, j + 1);

	// evaluate the spreadsheet
	for (cur_row = 0; cur_row < rows; cur_row++) {
		for (cur_col = 0; cur_col < cols; cur_col++) {
			cycle = 0;
			switch (eval_cell(cur_row, cur_col)) {
				case err_cycle   : die("cycle detected starting in %c%d", 'A' + cur_row, cur_col + 1); break;
				case err_oobounds: die("out of bounds reference in %c%d", 'A' + cur_row, cur_col + 1); break;
				case err_badcell : die("unsupported tokens in cell %c%d", 'A' + cur_row, cur_col + 1); break;
			}
		}
	}

	// print the results
	printf("%d %d\n", cols, rows);

	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			printf("%f\n", out[i][j]);

	cleanup();

	return (0);
}
