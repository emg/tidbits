# reverse mapping of keys -> values in given array
function reverse_mapping(array,        tmp_key, tmp_element, done) {
    # user shouldn't have passed these, but let's be safe
    tmp_key = tmp_element = ""
    for (key in done)
        delete done[key]

    for (key in array) {
        # if we already reversed this pair to avoid a collision, continue
        if (key in done)
            continue

        # if this swap would cause a colision, store those values
        if (array[key] in array) {
            tmp_key     = array[    key]
            tmp_element = array[tmp_key]
        }

        # reverse the mapping of our current key -> element pair
        array[array[key]] = key

        # delete the now old mapping
        delete array[key]

        # if we had a collision, reverse those values, too
        if (tmp_key) {
            array[tmp_element] = tmp_key
            done [tmp_key    ]
            tmp_key = ""
        }
    }
}

# example
BEGIN {
    a["b"] = 1
    a[1] = "a"
    a[2] = "b"
    a[3] = "c"

    for (k in a)
        printf("a[%s] = %s\n", k, a[k])

    reverse_mapping(a)

    printf("\n")
    for (k in a)
        printf("a[%s] = %s\n", k, a[k])
}
