#!/usr/bin/env bash
encode() {
    local c i s LC_ALL=C
    local code=({A..Z} {a..z} {0..9} + /)

    print() {
                           printf %c "${code[(s[0] >> 2            )       ]}"
                           printf %c "${code[(s[0] << 4 | s[1] >> 4) & 0x3f]}"
        if ((i > 1)); then printf %c "${code[(s[1] << 2 | s[2] >> 6) & 0x3f]}"; else printf =; fi
        if ((i > 2)); then printf %c "${code[(s[2]                 ) & 0x3f]}"; else printf =; fi
        i=0
    }

    while IFS= read -r -d '' -n 1 c; do
        printf -v 's[i++]' %d "'$c"
        ((i == 3)) && print
    done
    for ((c = i; c < 3; c++)); do
        s[c]=0
    done
    ((i)) && print
}

decode() {
    local c i s LC_ALL=C
    declare -A code

    for c in {A..Z} {a..z} {0..9} + /; do
        ((code["$c"] = ++i))
    done

    print() {
        ((i > 1)) && printf -v c %x "$(((s[0] << 2 | s[1] >> 4) & 0xff))" && printf %b "\x$c"
        ((i > 2)) && printf -v c %x "$(((s[1] << 4 | s[2] >> 2) & 0xff))" && printf %b "\x$c"
        ((i > 3)) && printf -v c %x "$(((s[2] << 6 | s[3]     ) & 0xff))" && printf %b "\x$c"
        i=0
    }

    i=0
    while IFS= read -r -d '' -n 1 c; do
        [[ $c == $'\n' ]] && continue
        [[ $c == = ]] && break
        ((code["$c"])) || return 1
        ((s[i++] = code["$c"] - 1))
        ((i == 4)) && print
    done
    ((i == 1)) && return 1
    if [[ $i -eq 2 && $c == = ]]; then
        IFS= read -r -d '' -n 1 c
        [[ $c != = ]] && return 1
    fi
    ((i)) && print
}

if [[ "$#" -eq 1 && "$1" == -d ]]; then
    decode
elif (($#)); then
    exit 1
else
    encode
fi
