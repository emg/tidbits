# someone on #bash was asking how to do a stream xor in bash
xor() {
    local key LC_CTYPE=C c i

    for ((i = 0; i < ${#1}; i++)); do
        printf -v key[i] %d "'${1:i:1}"
    done;

    i=0
    while IFS= read -r -d '' -n 1 c; do
        printf -v c %d "'$c"
        ((c ^= key[i++], i == "${#key[@]}")) && i=0
        printf %b "\x$(printf %x "$c")"
    done
}

xor "ab" <<< "Test" | xor "ab"
