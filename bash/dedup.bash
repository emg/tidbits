dedup() {
    (($#)) || return 1

    local    arg
    local -A set

    for arg; do
        set["$arg"]=1
    done
    printf "%s\0" "${!set[@]}"
}

dedup_to_array() {
    (($# > 1)) || return 1

    local arr=$1 element i
    shift

    unset "$arr"
    while IFS= read -rd '' element; do
        printf -v "$arr[i++]" %s "$element"
    done < <(dedup "$@")
}

example() {
    # our test array
    myarr=(foo bar baz qux foo 'two words' bonzai baz 'two words' baz bar)

    dedup_to_array myarr "${myarr[@]}"

    # print the new array
    printf '<%s>' "${myarr[@]}"
    echo
}
