arrange_opts() {
    local flags args optstr=$1
    shift

    while (($#)); do
        case $1 in
            --) args+=("$@")
                break;
                ;;
            -*) flags+=("$1")
                if [[ $optstr == *"${1: -1}:"* ]]; then
                    flags+=("$2")
                    shift
                fi
                ;;
            * ) args+=("$1")
                ;;
        esac
        shift
    done
    OPTARR=("${flags[@]}" "${args[@]}")
}

example() {
    local OPTIND OPTARR optstring=ab:cd

    printf 'before arrange:'
    printf '<%s>' "$@"
    echo

    arrange_opts "$optstring" "$@"
    set -- "${OPTARR[@]}"

    printf 'after  arrange:'
    printf '<%s>' "$@"
    printf \\n\\n

    printf flag:arg\\n

    OPTIND=1
    while getopts "$optstring" opt; do
        case $opt in
            a|c|d) printf        %s\\n "$opt"    ;;
            b    ) printf      b:%s\\n "$OPTARG" ;;
            \?   ) printf badopt:%s\\n "$OPTARG" ;;
            :    ) printf  noarg:%s\\n "$OPTARG" ;;
            *    ) printf    wtf:%s\\n "$opt"    ;;
        esac
    done
    shift $((OPTIND-1))

    printf \\nargs:
    printf '<%s>' "$@"
    echo
}

example -ad foo -cb bar baz -- -a bonzai
