#define _BSD_SOURCE /* for inet_aton */

#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <ifaddrs.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <netinet/if_ether.h>
#include <netinet/in.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>

#define BUFSIZE 65536 /* FIXME: what's a reasonable size? */

/* capture outgoing packets and send them to other ports via UDP */
int main(int argc, char **argv)
{
    char *capdev, *outdev;
    int capfd, outfd;
    size_t nips = (argc - 3) / 2;
    struct sockaddr_in ips[nips]; /* FIXME: error check this... */

    /* read arguments: capdev outdev ip port [ip port]...
     * port should match hub_in's port for each ip
     */
    if (argc < 5)
        err(1, "USAGE: %s capdev outdev ip port [ip port]...\n", *argv);
    capdev = *++argv;
    outdev = *++argv;

    for (struct sockaddr_in *sa = ips; *++argv; sa++) {
        bzero(sa, sizeof(*sa));
        sa->sin_family = AF_INET;
        if (!inet_aton(*argv, (struct in_addr *)&sa->sin_addr.s_addr))
            err(1, "inet_aton: could not convert '%s' to valid ip address\n", *argv);
        sa->sin_port = htons(atoi(*++argv)); /* FIXME: argc error checking, strtol() error checking */
    }

    /* open raw socket on capture device */
    if ((capfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0)
        err(1, "socket: %s\n", strerror(errno));

    /* socket(7) says SO_BINDTODEVICE is not supported for packet sockets, use
     * bind instead, but this seems to work and bind fails with bind: Invalid
     * argument */
    if (setsockopt(capfd, SOL_SOCKET, SO_BINDTODEVICE, capdev, strlen(capdev) + 1) < 0)
        err(1, "setsockopt: %s\n", strerror(errno));

    /* open UDP socket for sending on output device */
    if ((outfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        err(1, "socket: %s\n", strerror(errno));

    if (setsockopt(outfd, SOL_SOCKET, SO_BINDTODEVICE, outdev, strlen(outdev) + 1) < 0)
        err(1, "setsockopt: %s\n", strerror(errno));

    /* capture and send outgoing packets */
    for (;;) {
        unsigned char buf[BUFSIZE];
        struct sockaddr_ll src;
        socklen_t srclen = sizeof(src);
        ssize_t msglen = recvfrom(capfd, buf, sizeof(buf), 0, (struct sockaddr *)&src, &srclen);

        if (msglen < 0)
            err(1, "recv: %s\n", strerror(errno));

        if (src.sll_pkttype != PACKET_OUTGOING)
            continue;

        /* send packet to all ips given */
        for (struct sockaddr_in *sa = ips; sa < ips + nips; sa++) {
            if (sendto(outfd, buf, msglen, 0, (struct sockaddr *)sa, sizeof(*sa)) < 0)
                err(1, "sendto: %s\n", strerror(errno));
        }
    }
    return 0;
}
