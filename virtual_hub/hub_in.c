#define _BSD_SOURCE /* for inet_aton */

#include <arpa/inet.h>
#include <errno.h>
#include <err.h>
#include <ifaddrs.h>
#include <netinet/if_ether.h>
#include <netinet/in.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <linux/if_packet.h>

#define BUFSIZE 65536 /* FIXME: what's a reasonable size? */

/* receive packets via UDP over management interface and send them on the wire */
int main(int argc, char **argv)
{
    char *indev, *outdev;
    int infd, outfd, outindex;
    struct sockaddr_in inip;
    struct ifaddrs *addrlist, *ap;

    /* read arguments: outdev indev inip inport
     * inport should match hub_out's outport for our ip */
    if (argc != 5)
        err(1, "USAGE: %s outdev indev inip inport\n", *argv);
    outdev = *++argv;
    indev  = *++argv;

    bzero(&inip, sizeof(inip));
    if (!inet_aton(*++argv, (struct in_addr *)&inip.sin_addr.s_addr))
        err(1, "inet_aton: could not convert '%s' to valid ip address\n", *argv);

    inip.sin_port = htons(atoi(*++argv)); /* FIXME: strtol() for error checking */
    inip.sin_family = AF_INET;

    /* find our dev index */
    if (getifaddrs(&addrlist) < 0)
        err(1, "getifaddrs: %s\n", strerror(errno));

    for (ap = addrlist; ap; ap = ap->ifa_next) {
        if (!strcmp(ap->ifa_name, outdev) && ap->ifa_addr->sa_family == AF_PACKET) {
            outindex = ((struct sockaddr_ll *)ap->ifa_addr)->sll_ifindex;
            break;
        }
    }
    freeifaddrs(addrlist);

    if (!ap)
        err(1, "no device named '%s' with type AF_PACKET\n", outdev);

    /* open UDP socket on indev bound to inip:inport */
    if ((infd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        err(1, "socket: %s\n", strerror(errno));

    if (setsockopt(infd, SOL_SOCKET, SO_BINDTODEVICE, indev, strlen(indev) + 1) < 0)
        err(1, "setsockopt: %s\n", strerror(errno));

    if (bind(infd, (struct sockaddr *)&inip, sizeof(inip)) < 0)
        err(1, "bind: %s\n", strerror(errno));

    /* open raw socket on outdev */
    if ((outfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0)
        err(1, "socket: %s\n", strerror(errno));

    /* receive and forward packets */
    for (;;) {
        unsigned char buf[BUFSIZE];
        struct sockaddr_ll dest;
        ssize_t msglen = recv(infd, buf, sizeof(buf), 0);

        if (msglen < 0)
            err(1, "recv: %s\n", strerror(errno));

        bzero(&dest, sizeof(dest));
        dest.sll_family   = AF_PACKET;
        dest.sll_ifindex  = outindex;
        dest.sll_protocol = ((struct ethhdr *)buf)->h_proto;
        dest.sll_halen    = ETH_ALEN;
        memcpy(dest.sll_addr, ((struct ethhdr *)buf)->h_dest, ETH_ALEN);

        if ((msglen = sendto(outfd, buf, msglen, 0, (struct sockaddr *)&dest, sizeof(dest))) < 0)
            err(1, "sendto: %s\n", strerror(errno));
    }
    return 0;
}
