/*
 * base64
 * with no arguments read from stdin and write base64 encoded output to stdout
 * with argument -d read base64 encoded input from stdin and write decoded output to stdout
 * ignores newlines in encoded input
 * any illegal character in encoded input causes error
 * no newlines in encoded output
 * does not require padding (=) but if padding is present it must be correct
 * after padding more encoded input may follow
 */

#include <stdio.h>
#include <string.h>
#include <limits.h>

static char code[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static int encode(FILE *in, FILE *out)
{
    int c, i = 0;
    unsigned char s[3] = { 0 };

    do {
        if ((c = fgetc(in)) == EOF)
            memset(s + i, 0, 3 - i);
        else
            s[i++] = c;

        if (i == 3 || (i && c == EOF)) {
            fputc(        code[(s[0] >> 2            )       ]      , out);
            fputc(        code[(s[0] << 4 | s[1] >> 4) & 0x3f]      , out);
            fputc(i > 1 ? code[(s[1] << 2 | s[2] >> 6) & 0x3f] : '=', out);
            fputc(i > 2 ? code[(s[2]                 ) & 0x3f] : '=', out);
            i = 0;
        }
    } while (c != EOF);
    return 0;
}

static int decode(FILE *in, FILE *out)
{
    int c, i = 0;
    unsigned char s[4] = { 0 };
	char edoc[UCHAR_MAX] = { 0 };

	for (char *p = code; *p; p++)
		edoc[*p] = p - code + 1;

    do {
        if ((c = fgetc(in)) == '\n')
            continue;

        if ((c == '=' && i < 2)                    ||
            (c == EOF && i && i < 2)               ||
            (c != EOF && c != '=' && !edoc[c])     ||
            (c == '=' && i == 2 && fgetc(in) != '='))
            return 1;

		s[i++] = edoc[c] - 1;

        if (i == 4 || c == '=' || c == EOF) {
            if (i > 1) fputc((s[0] << 2 | s[1] >> 4) & 0xff, out);
            if (i > 2) fputc((s[1] << 4 | s[2] >> 2) & 0xff, out);
            if (i > 3) fputc((s[2] << 6 | s[3]     ) & 0xff, out);
            i = 0;
        }
    } while (c != EOF);
    return 0;
}

int main(int argc, char **argv)
{
    if (argc == 2 && !strcmp(argv[1], "-d"))
        return decode(stdin, stdout);
    else if (argc != 1)
        return 1;
    else
        return encode(stdin, stdout);
}
