#define _GNU_SOURCE
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#define print_err(msg)               fprintf(stderr, "%s: " msg " %s: %s\n", *argv, *path, strerror(errno))
#define check_goto(label, expr, msg) do { if (expr) break; print_err(msg); goto label; } while (0)

// retry syscalls when interrupted by signal
// NOTE: close(3p) is not included as state of fildes is unspecified after interruption
int     open_     (const char *path, int oflag)         { int     r; do { r = open     (path, oflag);        } while (r < 0 && errno == EINTR); return r; }
ssize_t read_     (int fildes, void *buf, size_t nbyte) { ssize_t r; do { r = read     (fildes, buf, nbyte); } while (r < 0 && errno == EINTR); return r; }
ssize_t write_    (int fildes, void *buf, size_t nbyte) { ssize_t r; do { r = write    (fildes, buf, nbyte); } while (r < 0 && errno == EINTR); return r; }
int     ftruncate_(int fildes, off_t length)            { int     r; do { r = ftruncate(fildes, length);     } while (r < 0 && errno == EINTR); return r; }

#define open      open_
#define read      read_
#define write     write_
#define ftruncate ftruncate_

int rwall(ssize_t (*rw)(), int fd, char *buf, size_t count)
{
	for (ssize_t r; count; buf += r, count -= r)
		if ((r = rw(fd, buf, count)) < 0)
			return r;
	return 0;
}

int main(int argc, char **argv)
{
	char *find, *repl;
	size_t find_len, repl_len;

	if (argc < 4 || strlen(argv[1]) == 0) {
		fprintf(stderr, "Usage: %s find replace file...\n", *argv);
		return 1;
	}

	find = argv[1];
	repl = argv[2];
	find_len = strlen(find);
	repl_len = strlen(repl);

	for (char **path = argv + 3; *path; path++) {
		char *beg, *end, *p, *file = NULL;
		int   fd = -1;
		off_t size;
		check_goto(cleanup,    0 <= (fd   = open  (*path, O_RDWR))      , "Failed to open"      );
		check_goto(cleanup,    0 <= (size = lseek (fd, 0, SEEK_END))    , "Failed to lseek EOF" ); if (size == 0) goto cleanup; // empty file
		check_goto(cleanup,    0 ==         lseek (fd, 0, SEEK_SET)     , "Failed to lseek BOF" );
		check_goto(cleanup, NULL != (file = malloc(size))               , "Failed to malloc for");
		check_goto(cleanup,    0 ==         rwall (read, fd, file, size), "Failed to read"      );
		check_goto(cleanup,    0 ==         lseek (fd, 0, SEEK_SET)     , "Failed to lseek BOF" );

		for (beg = file, end = beg + size, size = 0; (p = memmem(beg, end - beg, find, find_len)); size += repl_len + (p - beg), beg = p + find_len) {
			check_goto(cleanup, 0 == rwall(write, fd, beg , p - beg ), "Failed to write to");
			check_goto(cleanup, 0 == rwall(write, fd, repl, repl_len), "Failed to write to");
		}
		check_goto(cleanup, 0 == rwall(write, fd, beg, end - beg), "Failed to write to"); size += end - beg;
		check_goto(cleanup, 0 == ftruncate(fd, size)             , "Failed to truncate");

cleanup:
		if (fd >= 0 && close(fd) < 0) print_err("Failed to close");
		if (file) free(file);
	}

	return 0;
}
