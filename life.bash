#!/bin/bash

# read a file or stdin with a starting grid
# space = dead    -> 0
# x     = alive   -> 1
# actually any character that's not x or \n will be treated as a dead cell

IFS= LC_ALL=C
mapfile -t lines < "${1-/dev/stdin}"
endgen=${2-100}

# TODO write position reading code, this is no longer good
for line in "${lines[@]}"; do
    (( width = width < ${#line} ? ${#line} : width ))
done

declare -A cells newcells checked

for (( i = 0; i < ${#lines[@]}; i++ )); do
    for (( j = 0; j < width; j++ )); do
        if [[ ${lines[i]:j:1} = x ]]; then
            cells[$i,$j]=1
        fi
    done
done

display () {
    str=
    for ij in "${!cells[@]}"; do
        i=${ij%,*} j=${ij#*,}
        ((mini = i < mini ? i : mini, maxi = i > maxi ? i : maxi,
          minj = j < minj ? j : minj, maxj = j > maxj ? j : maxj))
    done;

    eval is=({$mini..$maxi})
    eval js=({$minj..$maxj})
    
    for i in "${is[@]}"; do
        for j in "${js[@]}"; do
            if [[ ${cells[$i,$j]} ]]; then str+=x; else str+=' '; fi
        done
        str+=$'\n'
    done
    printf '%s' "$str"

    printf "gen:%d pop:%d width:%d height:%d gen/sec:%d\n" \
        "$gen" "${#cells[@]}" "$((maxj - minj))" "$((maxi - mini))" "$((gen / (SECONDS ? SECONDS : 1)))" >&2
}

trap display EXIT

check() {
    checked[$1]=1
    i=${1%,*} j=${1#*,}

    ((ip = i - 1, in = i + 1, jp = j - 1, jn = j + 1))

    ((sum = cells[$ip,$jp] + cells[$i,$jp] + cells[$in,$jp] +
            cells[$ip,$j]  + cells[$i,$j]  + cells[$in,$j]  +
            cells[$ip,$jn] + cells[$i,$jn] + cells[$in,$jn] ,

    sum == 3 || (sum == 4 && cells[$1]))) && newcells[$1]=1
}

while ((gen < endgen && ++gen)); do

    for ij in "${!cells[@]}"; do
        i=${ij%,*} j=${ij#*,}
        ((ip = i - 1, in = i + 1, jp = j - 1, jn = j + 1))

        for coord in {$ip,$i,$in},{$jp,$j,$jn}; do
            [[ ${checked[$coord]} ]] || check $coord
        done
    done

    cells=()

    for ij in "${!newcells[@]}"; do
        cells[$ij]=1
    done

    newcells=() checked=()
done
