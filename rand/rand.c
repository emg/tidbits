#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>

#define RAND_PATH "/dev/urandom"
#define BAIL() do { status = -1; goto cleanup; } while (0)

char *prog;

// Random number generation from
// http://www0.cs.ucl.ac.uk/staff/d.jones/GoodPracticeRNG.pdf
uint32_t s[4]; // Seed
uint32_t rand32(void)
{
    uint64_t t;
    //uint32_t r;
    s[0] = 314527869 * s[0] + 1234567;
    s[1] ^= s[1] << 5; s[1] ^= s[1] >> 7; s[1] ^= s[1] << 22;
    t = 4294584393ULL * s[2] + s[3]; s[3] = t >> 32; s[2] = t;
    return s[0] + s[1] + s[2];
}

int srand32(void)
{
    int status = 0, e = 0, fd;
    if (0 > (fd = open(RAND_PATH, O_RDONLY))) BAIL();
    if (sizeof(s) != read(fd, s, sizeof(s)))  BAIL();

cleanup:
    e = errno;
    if (fd > 0 && close(fd) < 0) perror(prog);
    errno = e;
    return status;
}

uint64_t s2[2];
uint32_t s3[4];
uint64_t rand64(void)
{
    uint64_t t;
    s2[0] = 1490024343005336237ULL * s2[0] + 123456789;
    s2[1] ^= s2[1] << 21; s2[1] ^= s2[1] >> 17; s2[1] ^= s2[1] << 30;
    t = 4294584393ULL * s3[0] + s3[1]; s3[1] = t >> 32; s3[0] = t;
    t = 4246477509ULL * s3[2] + s3[3]; s3[3] = t >> 32; s3[2] = t;

    return s2[0] + s2[1] + s3[0] + ((uint64_t)s3[2] << 32);
}

int srand64(void)
{
    int status = 0, e = 0, fd;
    if (0 > (fd = open(RAND_PATH, O_RDONLY))) BAIL();
    if (sizeof(s2) != read(fd, s2, sizeof(s2)))  BAIL();
    if (sizeof(s3) != read(fd, s3, sizeof(s3)))  BAIL();

cleanup:
    e = errno;
    if (fd > 0 && close(fd) < 0) perror(prog);
    errno = e;
    return status;
}

int main(int argc, char **argv)
{
    prog = *argv;

    if (srand32()) return 1;

    uint32_t r[1024], *p;
    do for (p = r; r + sizeof(r)/sizeof(*r) - p; *(p++) = rand32());
    while (write(1, r, sizeof(r)) == sizeof(r));

    /*
    if (srand64()) return 1;

    uint64_t r[4096], *p;
    do for (p = r; r + sizeof(r)/sizeof(*r) - p; *(p++) = rand64());
    while (write(1, r, sizeof(r)) == sizeof(r));
    */

    perror("");
    return 0;
}
