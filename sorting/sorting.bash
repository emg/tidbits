print_array()
{
	declare -n _arr=$1
	printf "["
	printf "%s," "${_arr[@]:0:${#_arr[@]}-1}"
	printf "%s]\n" "${_arr[@]: -1}"
}

swap_in_array()
{
	declare -n _arr=$1
	declare -i _a=$2 _b=$3
	declare _t

	_t=${_arr[$_a]}
	_arr[$_a]=${_arr[$_b]}
	_arr[$_b]=$_t
}

shuffle_array()
{
	declare -n _arr=$1

	declare -i _i

	for ((_i = ${#_arr[@]} - 1; _i > 0; _i--)); do
		swap_in_array "$1" $((_i)) $((RANDOM % (_i + 1)))
	done
}

str_cmp()
{
	declare -i a b
	[[ $1 > $2 ]] && a=1
	[[ $2 > $1 ]] && b=1
	printf "%d" $((a - b))
}

int_cmp()
{
	printf "%d" $((($1 > $2) - ($2 > $1)))
}

compar()
{
	if declare -f "$cmp_func" >/dev/null 2>&1; then
		"$cmp_func" "$@"
	else
		str_cmp "$@"
	fi
}

is_sorted()
{
	declare -n _arr=$1

	for ((i = 0; i < ${#_arr[@]} - 1; i++)); do
		(("$(compar "${_arr[$i]}" "${_arr[$((i + 1))]}")" <= 0)) || return 1
	done

	return 0
}

is_heap()
{
	declare -n _arr=$1

	for ((i = 1; i < ${#_arr[@]}; i++)); do
		(("$(compar "${_arr[i]}" "${_arr[$(((i - 1) / 2))]}")" <= 0)) || return 1
	done

	return 0
}

bogo_sort()
{
	while ! is_sorted "$1"; do
		shuffle_array "$1"
	done
}

bubble_sort()
{
	declare -n _arr=$1

	declare -i _left _right

	for ((_right = ${#_arr[@]} - 1; _right > 0; _right--)); do
		for ((_left = 0; _left < _right; _left++)); do
			if (("$(compar "${_arr[$_left]}" "${_arr[$_right]}")" > 0)); then
				swap_in_array "$1" $((_left)) $((_right))
			fi
		done
	done
}

select_sort()
{
	declare -n _arr=$1

	declare -i _left _right _min

	for ((_left = 0; _left < ${#_arr[@]}; _left++)); do
		for ((_min = _right = _left; _right < ${#_arr[@]}; _right++)); do
			if (("$(compar "${_arr[$_right]}" "${_arr[$_min]}")" < 0)); then
				((_min = _right))
			fi
		done
		swap_in_array "$1" $((_min)) $((_left))
	done
}

sift_down()
{
	declare -n _arr=$1
	declare -i _beg=$2 _end=$3

	declare -i _child _swap

	for ((; _beg * 2 + 1 <= _end; _beg = _child)); do
		((_child = _beg * 2 + 1))
		if ((_child + 1 <= _end && "$(compar "${_arr[$_child]}" "${_arr[$((_child + 1))]}")" < 0)); then
			((_child++))
		fi
		if (("$(compar "${_arr[$_child]}" "${_arr[$_beg]}")" < 0)); then
			return
		fi
		swap_in_array "$1" $((_child)) $((_beg))
	done
}

sift_up()
{
	declare -n _arr=$1
	declare -i _beg=$2 _end=$3

	declare -i _child _parent

	for ((_child = _end; _child > _beg; _child = _parent)); do
		((_parent = (_child - 1) / 2))
		if (("$(compar "${_arr[$_parent]}" "${_arr[$_child]}")" < 0)); then
			swap_in_array "$1" $((_parent)) $((_child))
		else
			break
		fi
	done
}

heapify()
{
	declare -n _arr=$1

	declare -i _count=${#_arr[@]} _beg

	for ((_beg = (_count - 2) / 2; _beg >= 0; _beg--)); do
		sift_down "$1" $_beg $((_count - 1))
	done
}

heap_sort()
{
	declare -n _arr=$1

	declare -i _count=${#_arr[@]} _end

	heapify "$1"

	for ((_end = _count - 1; _end > 0;)); do
		swap_in_array "$1" $((_end)) $((0))
		sift_down "$1" $((0)) $((--_end))
	done
}

partition()
{
	declare -n _arr=$1
	declare -i _left=$2 _right=$3

	declare -i _i

	for ((_i = _left; _i < _right; _i++)); do
		if (("$(compar "${_arr[$_i]}" "${_arr[$_right]}")" < 0)); then
			swap_in_array "$1" $((_i)) $((_left++))
		fi
	done
	swap_in_array "$1" $((_left)) $((_right))
	((_piv = _left))
}

quick_sort()
{
	declare -n _arr=$1
	declare -i _left _right

	if (($# == 1)); then
		((_left = 0, _right = ${#_arr[@]} - 1))
	else
		((_left = $1, _right = $2))
	fi

	if ((_right - _left < 2)); then
		return
	fi

	declare -i _piv # set in partition()

	partition "$1" $((_left)) $((_right))

	qsort "$1" $((_left)) $((_piv - 1))
	qsort "$1" $((_piv + 1)) $((_right))
}
