/*
 * naive huffman encoding
 * symbol length fixed at 8 bits
 * no maximum code length
 *
 * TODO
 * more error checking
 * change queue to minimize code length variance
 */

#include <errno.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>

#define countof(x) (sizeof(x)/sizeof(*(x)))

typedef struct Node Node;
struct Node {
	Node   *left, *right;
	size_t  weight;
	uint8_t symbol;
};

typedef struct {
	Node **data;
	size_t len;
} Heap;

typedef struct {
	uint8_t *bits;
	size_t   count; /* double duty symbol count in file then number of bits */
} Code;

typedef struct {
	FILE   *fp;
	size_t  nbits;
	uint8_t bits;
} Bitbuf;

static noreturn void
die(char *fmt, ...)
{
	va_list ap;
	int e = errno;

	fflush(stdout);

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[strlen(fmt)-1] == ':')
		fprintf(stderr, " %s", strerror(e));

	fputc('\n', stderr);
	exit(1);
}

static void
efputc(int c, FILE *fp)
{
	if (fputc(c, fp) == EOF)
		die("fputc:");
}

/*
 * heap/priority queue
 */
static void
swap(Node **a, Node **b)
{
	Node *t = *a;
	*a = *b;
	*b = t;
}

static void
siftup(Heap *hp)
{
	for (size_t parent, child = hp->len - 1; child; child = parent) {
		parent = (child - 1) / 2;
		if (hp->data[parent]->weight <= hp->data[child]->weight)
			return;
		swap(hp->data + parent, hp->data + child);
	}
}

static void
siftdown(Heap *hp, size_t start)
{
	for (size_t parent = start, child = parent*2 + 1;
	     child < hp->len;
	     parent = child, child = parent*2 + 1) {
		if (child + 1 < hp->len && hp->data[child+1]->weight < hp->data[child]->weight)
			child++;
		if (hp->data[child]->weight >= hp->data[parent]->weight)
			return;
		swap(hp->data + child, hp->data + parent);
	}
}

static void
heapify(Heap *hp)
{
	for (size_t top = (hp->len - 2) / 2; top < hp->len; top--)
		siftdown(hp, top);
}

static void
push(Heap *hp, Node *np)
{
	hp->data[hp->len++] = np;
}

static void
enqueue(Heap *hp, Node *np)
{
	push(hp, np);
	siftup(hp);
}

static Node *
dequeue(Heap *hp)
{
	Node *np = *hp->data;
	*hp->data = hp->data[--hp->len];
	siftdown(hp, 0);
	return np;
}

/*
 * bit twiddling
 */
static void
setbit(uint8_t *bits, size_t bit)
{
	bits[bit/8] |= 1 << bit%8;
}

static void
clearbit(uint8_t *bits, size_t bit)
{
	bits[bit/8] &= ~(1 << bit%8);
}

static int
testbit(uint8_t *bits, size_t bit)
{
	return bits[bit/8] & 1 << bit%8;
}

static size_t
nbytes(size_t nbits)
{
	return (nbits + 7) / 8;
}

static void
putbit(int bit, Bitbuf *buf)
{
	buf->bits <<= 1;
	buf->bits |= !!bit;

	if (++buf->nbits == 8) {
		efputc(buf->bits, buf->fp);
		buf->bits = buf->nbits = 0;
	}
}

static void
putbits(uint8_t *bytes, size_t nbits, Bitbuf *buf)
{
	for (size_t i = 0; i < nbits; i++)
		putbit(testbit(bytes, i), buf);
}

static int
getbit(Bitbuf *buf)
{
	if (!buf->nbits) {
		int c = fgetc(buf->fp);
		if (c == EOF)
			die("fgetc:");
		buf->bits = c;
		buf->nbits = 8;
	}
	return buf->bits & 1<<--buf->nbits;
}

static uint8_t
getbyte(Bitbuf *buf)
{
	uint8_t byte = 0;
	for (size_t i = 0; i < 8; i++)
		byte |= !!getbit(buf) << i;
	return byte;
}

/*
 * write/read header
 */
static void
puttree(Node *np, Bitbuf *buf)
{
	if (!np->left) {
		putbit(1, buf);
		putbits(&np->symbol, 8, buf);
		return;
	}
	putbit(0, buf);
	puttree(np->left, buf);
	puttree(np->right, buf);
}

static void
gettree(Node *np, Node **next, Node *end, Bitbuf *buf)
{
	if (getbit(buf)) {
		np->symbol = getbyte(buf);
		np->left = np->right = 0;
		return;
	}

	if (*next == end)
		die("Bad header: too many nodes");

	np->left  = (*next)++;
	np->right = (*next)++;
	gettree(np->left, next, end, buf);
	gettree(np->right, next, end, buf);
}

/*
 * code generation
 */
static void
findcodelens(Node *np, size_t depth, Code *codes, size_t *maxbits, size_t *totalbytes)
{
	if (!np->left) {
		codes[np->symbol].count = depth;
		if (depth > *maxbits)
			*maxbits = depth;
		*totalbytes += nbytes(depth);
		return;
	}
	findcodelens(np->left, depth + 1, codes, maxbits, totalbytes);
	findcodelens(np->right, depth + 1, codes, maxbits, totalbytes);
}

static void
gencodes(Node *np, size_t depth, Code *codes, uint8_t *bits, uint8_t **codebits)
{
	if (!np->left) {
		memcpy(*codebits, bits, nbytes(depth));
		codes[np->symbol].bits = *codebits;
		*codebits += nbytes(depth);
		return;
	}

	clearbit(bits, depth);
	gencodes(np->left, depth + 1, codes, bits, codebits);
	setbit(bits, depth);
	gencodes(np->right, depth + 1, codes, bits, codebits);
}

/*
 * encode/decode
 */
static void
encode(FILE *infile, FILE *outfile)
{
	Code codes[256] = { { 0 } };
	size_t filelen = 0;

	/* count frequencies of symbols */
	for (int c; (c = fgetc(infile)) != EOF; codes[c].count++, filelen++)
		;
	if (ferror(infile))
		die("fgetc:");
	if (fseek(infile, 0, SEEK_SET))
		die("Infile must be seekable: fseek:");

	/* write length early to bail on empty file */
	for (size_t i = 0; i < sizeof(filelen); i++)
		efputc(filelen >> 8*i & 0xff, outfile);
	if (!filelen)
		return;

	size_t nsyms = 0;
	for (Code *cp = codes; cp < codes + countof(codes); cp++)
		nsyms += !!cp->count;

	/* create priority queue */
	Node *data[nsyms];
	Heap heap = {
		.data = data,
		.len  = 0,
	};

	/* allocate space for all nodes and push initial nodes */
	Node nodes[nsyms*2 - 1], *np = nodes;
	for (size_t i = 0; i < countof(codes); i++) {
		if (!codes[i].count)
			continue;
		np->weight = codes[i].count;
		np->symbol = i;
		push(&heap, np++);
	}

	/* build huffman tree */
	heapify(&heap);
	while (heap.len > 1) {
		np->left = dequeue(&heap);
		np->right = dequeue(&heap);
		np->weight = np->left->weight + np->right->weight;
		enqueue(&heap, np++);
	}
	Node *root = dequeue(&heap);

	/* write rest of header (filelen already written) */
	efputc(nsyms - 1, outfile);

	Bitbuf buf = { .fp = outfile };
	puttree(root, &buf);

	/* find code lengths, allocate space, generate */
	size_t maxbits = 0, totalbytes = 0;
	findcodelens(root, 0, codes, &maxbits, &totalbytes);

	uint8_t scratch[nbytes(maxbits)], codebits[totalbytes], *cbp = codebits;
	gencodes(root, 0, codes, scratch, &cbp);

	/* encode file */
	for (int c; (c = fgetc(infile)) != EOF; putbits(codes[c].bits, codes[c].count, &buf))
		;

	/* flush buffer */
	if (buf.nbits) {
		buf.bits <<= 8 - buf.nbits;
		efputc(buf.bits, buf.fp);
	}
}

static void
decode(FILE *infile, FILE *outfile)
{
	size_t filelen = 0;
	for (size_t i = 0; i < sizeof(filelen); i++) {
		int c = fgetc(infile);
		if (c == EOF)
			die("fgetc:");
		filelen |= c << i*8;
	}
	if (!filelen)
		return;

	int nsyms = fgetc(infile);
	if (nsyms == EOF)
		die("fgetc:");
	nsyms++;
	if (nsyms <= 0 || nsyms > 256)
		die("Bad header: %d symbols (expected 1-256)", nsyms);

	Node nodes[nsyms*2 - 1], *np = nodes + 1;
	Bitbuf buf = { .fp = infile };
	gettree(nodes, &np, nodes + countof(nodes), &buf);

	while (filelen--) {
		for (np = nodes; np->left; np = getbit(&buf) ? np->right : np->left)
			;
		efputc(np->symbol, outfile);
	}
}

int
main(int argc, char *argv[])
{
	if (argc == 2 && !strcmp(argv[1], "-d"))
		decode(stdin, stdout);
	else if (argc == 1)
		encode(stdin, stdout);
	else
		die("USAGE: huffman [-d]\n"
		    "       no argument encodes\n"
		    "       -d decodes\n"
		    "       reads from stdin writes to stdout\n"
		    "       for encoding stdin must be seekable (use < not |)");
	return 0;
}
