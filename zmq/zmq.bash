mktemp() ( # mktemp is not POSIX, so include my own simple implementation
	export LC_CTYPE=C
	td=${TMPDIR:-/tmp}
	[[ -d $td && -r $td && -w $td && -x $td ]] || return 1
	set -C
	until f=$td/$(tr -dc '[:alnum:]' < /dev/urandom | dd bs=8 count=1); >"$f";
	do :; done 2>/dev/null
	printf %s "$f"
)

zmq_push() { # USAGE: zmq_push host port < message_file
	trap '[[ $fd ]] && exec {fd}>&-; [[ -f $msg ]] && rm "$msg"; trap - RETURN' RETURN
	local fd msg len pre="\x1\x0\xff\x0\x0\x0\x0"

	(($# == 2)) && (($2 > 0 && $2 < 65536)) || return 1
	exec {fd}>"/dev/tcp/$1/$2"              || return 1
	msg=$(mktemp)                           || return 1
	cat > "$msg"                            || return 1
	len=$(wc -c "$msg")                     || return 1

	for ((len = ${len% *} + 1, mask = 0xff000000; mask; mask >>= 8)); do
		pre+="\x$(printf %x $((len & mask)))"
	done
	pre+="\x0"

	{ printf %b "$pre"; cat "$msg"; } >&$fd || return 1
}
