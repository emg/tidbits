#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>

#define zcheck_err(eval, condition, fmt, args...)                                                                     \
((void)({                                                                                                             \
    errno = 0;                                                                                                        \
    if (!(condition)) {                                                                                               \
        fprintf(stderr, "%s: " fmt "%s%s\n", prog_name, ##args, errno ? ": " : "", errno ? zmq_strerror(errno) : ""); \
        exit(eval);                                                                                                   \
    }                                                                                                                 \
}))

char *prog_name;

/*
 * Create ZMQ_ROUTER socket, bind, echo messages.
 */
int main(int argc, char **argv)
{
	int       con;
	void     *ctx, *soc;
	zmq_msg_t msg;

	prog_name = argv[0];

	zcheck_err(EXIT_FAILURE,   2 == argc                       , "Supply endpoint as only argument"     );
	zcheck_err(EXIT_FAILURE, ctx  = zmq_ctx_new()              , "Error creating context"               );
	zcheck_err(EXIT_FAILURE, soc  = zmq_socket(ctx, ZMQ_DEALER), "Error creating socket"                );
	zcheck_err(EXIT_FAILURE,   0 == zmq_bind(soc, argv[1])     , "Error binding to endpoint %s", argv[1]);
	zcheck_err(EXIT_FAILURE,   0 == zmq_msg_init(&msg)         , "Error initialise mesage"              );

	setbuf(stdout, NULL);
	for (con = 0;;) {
		int    more, i;
		size_t size = sizeof(more);
		zcheck_err(EXIT_FAILURE, -1 != zmq_recvmsg(soc, &msg, 0)                     , "Error receiving message"       );
		zcheck_err(EXIT_FAILURE,  0 == zmq_getsockopt(soc, ZMQ_RCVMORE, &more, &size), "Error checking for ZMQ_RCVMORE");
		//zcheck_err(EXIT_FAILURE, -1 != zmq_sendmsg(soc, &msg, more ? ZMQ_SNDMORE : 0), "Error echoing message"         );
		printf("received %d bytes: ", zmq_msg_size(&msg));
		for (i = 0; i < zmq_msg_size(&msg); i++)
			printf("%c", ((char*)zmq_msg_data(&msg))[i]);
		printf("\n");
		if (!more)
			printf("End of message\n");
	}

	return 0;
}
