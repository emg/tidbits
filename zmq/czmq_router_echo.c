#include <czmq.h>
#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>

#define zcheck_err(eval, condition, fmt, args...)                                                                     \
((void)({                                                                                                             \
    errno = 0;                                                                                                        \
    if (!(condition)) {                                                                                               \
        fprintf(stderr, "%s: " fmt "%s%s\n", prog_name, ##args, errno ? ": " : "", errno ? zmq_strerror(errno) : ""); \
        exit(eval);                                                                                                   \
    }                                                                                                                 \
}))

char *prog_name;

int echo(zloop_t *loop, zmq_pollitem_t *item, void *arg)
{
	zmsg_t    *msg;
	static int con = 0;

	zcheck_err(EXIT_FAILURE, msg  = zmsg_recv(item->socket)      , "Error receiving message");
	zcheck_err(EXIT_FAILURE,   0 == zmsg_send(&msg, item->socket), "Error sending message"  );
	printf("Received: %d ", ++con);

	return 0;
}

/*
 * Create ZMQ_ROUTER socket, bind, echo messages.
 */
int main(int argc, char **argv)
{
	zctx_t         *ctx;
	void           *soc;
	zloop_t        *loop;
	zmq_pollitem_t  poller;

	prog_name = argv[0];

	zcheck_err(EXIT_FAILURE, argc == 2                           , "Supply endpoint as only argument"   );
	zcheck_err(EXIT_FAILURE, ctx   = zctx_new()                  , "Error creating context"             );
	zcheck_err(EXIT_FAILURE, soc   = zsocket_new(ctx, ZMQ_ROUTER), "Error creating socket"              );
	zcheck_err(EXIT_FAILURE,    0 <= zsocket_bind(soc, argv[1])  , "Error binding socket to %s", argv[1]);
	zcheck_err(EXIT_FAILURE, loop  = zloop_new()                 , "Error creating zloop"               );

	poller = (zmq_pollitem_t){ .socket = soc, .fd = 0, .events = ZMQ_POLLIN, .revents = 0 };

	zcheck_err(EXIT_FAILURE, 0 == zloop_poller(loop, &poller, echo, NULL), "Error registering poller");

	setbuf(stdout, NULL);
	zloop_start(loop);

	return 0;
}
