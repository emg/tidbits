#ifndef ZERR_H
#define ZERR_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE // for program_invocation_name
#endif

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <zmq.h>

/*
 * I measured a 5-10% increase in performance with this...
 */
#define likely(  x) __builtin_expect ((x), 1)
#define unlikely(x) __builtin_expect ((x), 0)

/*
 * our own versions of err(3) and warn(3) because we need to use zmq_strerror(3)
 * check for errno and decide whether or not to zmq_strerror(3) instead of
 * having separate errx(3) and warnx(3) variants
 * NOTE: void cast for fprintf(3) so we don't accidently use the return value anywhere
 *       exit(3), and goto are already void
 */
#ifdef NDEBUG
#define zwarn(fmt, args...) ((void)0)
#define CLEAR_ERRNO()       ((void)0)
#else // NDEBUG

extern char *program_invocation_short_name;
#define zwarn(fmt, args...) ((void)fprintf(stderr, "%s: " fmt "%s%s\n", program_invocation_short_name, ##args, errno ? ": " : "", errno ? zmq_strerror(errno) : ""))
#define CLEAR_ERRNO()       ((void)(errno = 0))

#endif // NDEBUG

#define zerr(eval, fmt, args...) ({ zwarn(fmt, ##args); exit(eval); })

#define zcheck_warn(       condition, fmt, args...) ({ CLEAR_ERRNO(); if (unlikely(!(condition)))   zwarn(      fmt, ##args);               })
#define zcheck_err( eval , condition, fmt, args...) ({ CLEAR_ERRNO(); if (unlikely(!(condition)))   zerr (eval, fmt, ##args);               })
#define zcheck_goto(label, condition, fmt, args...) ({ CLEAR_ERRNO(); if (unlikely(!(condition))) { zwarn(      fmt, ##args); goto label; } })

#endif // ZERR_H
