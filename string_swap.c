#include <stdio.h>
#include <string.h>

typedef struct {
    char *find;
    char *repl;
    char *p;
} Foo;

#define LEN(x)   (sizeof(x)/sizeof(*(x)))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

int main(int argc, char **argv)
{
    if (!argc || (argc - 1) % 2)
        return 1;

    size_t max_len = 0;
    Foo foos[argc / 2 + 1];
    foos[LEN(foos) - 1].p = NULL;

    for (Foo *f = foos; *++argv; ++f) {
        max_len = MAX(max_len, strlen(*argv));
        f->find = f->p = *argv;
        f->repl = *++argv;
    }

    char found[max_len + 1], *p = found;
    for (int c; (c = getchar()) != EOF;) {
        int state = 1;
        for (Foo *f = foos; f->p; ++f) {
            if (c == *f->p) {
                ++f->p;
                state = 2;
            } else
                f->p = f->find;
            if (!*f->p) {
                p = found;
                fputs(f->repl, stdout);
                state = 0;
                for (f = foos; f->p; ++f)
                    f->p = f->find;
                break;
            }
        }
        if (state)
            *(p++) = c;
        if (state == 1) {
            *(p++) = '\0';
            fputs(p = found, stdout);
        }
    }
    return 0;
}
