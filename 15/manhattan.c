/* 15 puzzle solver
 * example input: (16 is hole)
 *  5 12  3 13
 * 15  2 16  7
 * 11  9 14  8
 *  6 10  4  1
 */
#include <stdio.h>
#include <stdlib.h>

enum {
	SIZE = 4,
	NNUM = SIZE*SIZE,
	HOLE = NNUM-1, /* 0 based internal represenation */
	SMAX = 80,
};

static int board[NNUM];
static int mandist[NNUM][NNUM]; /* mandist[i][j] = manhattan distance for tile i in position j */
static int solution[SMAX]; /* TODO: better way to store solution for different sizes */
static int jump[] = { -SIZE, +SIZE, -1, +1 }; /* move hole up, down, left, right */

static void
initmandist(void)
{
	for (int i = 0; i < NNUM; i++)
		for (int j = 0; j < NNUM; j++)
			mandist[i][j] = abs(i / SIZE - j / SIZE) + abs(i % SIZE - j % SIZE);
}

static int
dist(void)
{
	int sum = 0;
	for (int *p = board; p < board + NNUM; p++)
		if (*p != HOLE)
			sum += mandist[*p][p - board];
	return sum;
}

static int
issolved(void)
{
	for (int i = 0; i < NNUM - 1; i++)
		if (board[i] != i)
			return 0;
	return 1;
}

static int
solve(int depth, int *h, int last)
{
	if (!depth)
		return issolved();
	if (dist() > depth)
		return 0;

	for (int *j = jump; j < jump + sizeof(jump) / sizeof(*jump); j++) {
		/* make sure before and after are in same row or column */
		int b = h - board, a = h + *j - board;
		if (a < 0 || a >= NNUM || ((b / SIZE != a / SIZE) && (b % SIZE != a % SIZE)))
			continue;

		if (h[*j] == last)
			continue;

		int n = *h = h[*j];
		h[*j] = HOLE;
		if (solve(depth - 1, h + *j, n)) {
			solution[depth - 1] = n;
			return 1;
		}
		h[*j] = n;
		*h = HOLE;
	}
	return 0;
}

static int
parity(void)
{
	int p = 0, used[NNUM] = { 0 };

	for (int i = 0; i < NNUM; i++) {
		if (!used[i])
			p--; /* fix overcount in next loop */
		for (int j = i; !used[j]++; j = board[j])
			p++;
	}
	return p % 2;
}

static int *
input(void)
{
	int *h, used[NNUM] = { 0 };

	for (int *p = board; p < board + NNUM; p++) {
		if (scanf("%d", p) != 1 || *p < 1 || *p > NNUM || used[--*p]++) {
			fprintf(stderr, "bad input\n");
			exit(1);
		}
		if (*p == HOLE)
			h = p;
	}
	return h;
}

int
main(void)
{
	initmandist();

	int *h = input(), d = dist(), p = parity();

	if (mandist[h - board][*h] % 2 != p) {
		fprintf(stderr, "impossible board\n");
		exit(1);
	}

	if (d % 2 != p)
		d++; /* d--? any cases d++ overestimates? */

	for (; d < SMAX + 1; d += 2) {
		fprintf(stderr, "trying depth %d\n", d);
		if (solve(d, h, -1)) {
			for (int i = d - 1; i >= 0; i--)
				printf("%d ", solution[i] + 1);
			putchar('\n');
			return 0;
		}
	}
	return 2;
}
