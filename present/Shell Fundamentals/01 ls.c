#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void)
{
	char *cmd[] = { "ls", "-l", "thatfile", 0 };
	if (!fork())
		execvp(*cmd, cmd);
	else
		wait(0);
	return 0;
}
