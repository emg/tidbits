package main

import (
	"os/exec"
	"os"
)

func main() {
	cmd := exec.Command(`ls`, `-l`, `thatfile`)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	_ = cmd.Run()
}
