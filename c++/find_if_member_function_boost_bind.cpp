#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

#include <boost/bind.hpp>

class Sim {
public:
    Sim(std::string aName) : mName(aName) {}
    std::string getName() const { return mName; }

private:
    std::string mName;
};

std::ostream& operator<<(std::ostream &os, const Sim &aSim) {
    os << aSim.getName();
    return os;
}

class Acc {
public:
    Acc(std::vector<Sim> aSims) : mSims(aSims) {}
    void addSim(Sim aSim) {
        std::vector<Sim>::iterator it = std::find_if(mSims.begin(), mSims.end(), boost::bind(&Sim::getName, _1) == aSim.getName());
        if (it == mSims.end()) std::cout << "end" << std::endl;
        else                   std::cout << *it   << std::endl;
        mSims.push_back(aSim);
    }
    void print() const {
        std::copy(mSims.begin(), mSims.end(), std::ostream_iterator<Sim>(std::cout, ","));
        std::cout << std::endl;
    }

private:
    std::vector<Sim> mSims;
};

int main()
{
    std::vector<Sim> sims;
    sims.push_back(Sim("foo")); sims.push_back(Sim("bar")); sims.push_back(Sim("baz"));

    Acc acc(sims);
    acc.print();
    acc.addSim(Sim("bar"));
    acc.print();
    return 0;
}
