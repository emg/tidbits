#include <algorithm>
#include <iostream>
#include <vector>

#include <boost/bind.hpp>

class D {
public:
    D(const std::vector<int> &v)
    {
        std::for_each(v.begin(), v.end(), boost::bind(&D::print, this, _1));
    }

    void print(int i)
    {
        std::cout << i << std::endl;
    }
};

int main()
{
    std::vector<int> v;
    v.push_back(1); v.push_back(2); v.push_back(3);

    D d(v);

    return 0;
}
