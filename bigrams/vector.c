/*
 * create a table of bigrams with frequencies and probabilities
 *
 * data structure:
 * two dimensional array of counts, array[a][b] represents bigram ab
 * simple with C locale, more difficult with other locales
 *
 * solution: vector of vectors using Count
 * elements of primary vector:
 *     c is first character in bigram (A)
 *     n is number of times that character appears
 *     v is secondary vector
 *     p is unused
 *     f is frequency of A (after completion)
 * elements of secondary vector:
 *     c is second character in bigram (B)
 *     n is number of times bigram appears
 *     v is unused
 *     p is probability of AB given A (after completion)
 *     f is frequency of AB (after completion)
 *
 * currently linear search, plenty fast for the size inputs I have (see update)
 * could try binary search, but that means in order insertion or sorting
 * could also try different data structures
 * hash or binary tree of bigram -> count
 * what's a good hash function for wchar_t[2] ?
 * binarytree sounds like a fun/good idea
 *
 * update
 * implemented with a splay tree, it was faster for larger numbers of distinct
 * bigrams, greater than 4000 ish
 * then changed this to binary search instead of linear and it's much faster
 * that was fun though...
 *
 * perhaps try libc binary trees next? (see tdelete(3p))
 * POSIX doesn't guarantee the implementation is balanced
 * musl uses avl trees, glibc uses red black trees
 */

#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <locale.h>
#include <search.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wchar.h>
#include <wctype.h>

typedef struct Vec Vec;
typedef struct {
    union {
        Vec   *v; /* vector of Count */
        double p; /* probability     */
    };
    size_t  n;    /* number/count    */
    double  f;    /* frequency       */
    wchar_t c;    /* character       */
} Count;

struct Vec {
    size_t size;
    size_t cap;
    Count  data[];
};

typedef struct {
    char *name;
    int (*func)(wint_t);
} Test;

Test tests[] = {
    { "alnum" , iswalnum  },
    { "alpha" , iswalpha  },
    { "blank" , iswblank  },
    { "cntrl" , iswcntrl  },
    { "digit" , iswdigit  },
    { "graph" , iswgraph  },
    { "lower" , iswlower  },
    { "print" , iswprint  },
    { "punct" , iswpunct  },
    { "space" , iswspace  },
    { "upper" , iswupper  },
    { "xdigit", iswxdigit },
    { NULL    , NULL      }
};

void err(char *fmt, ...)
{
    va_list ap;
    int     e = errno;

    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    if (fmt[strlen(fmt) - 1] == ':')
        fprintf(stderr, " %s\n", strerror(e));

    exit(1);
}

Count *find(Vec **v, Count *key)
{
    ssize_t min, mid, max;
    Count  *cp;

    if (!*v) { /* create */
        if (!(*v = malloc(sizeof(**v) + sizeof(*(*v)->data))))
            err("malloc:");
        (*v)->cap     = 1;
        (*v)->size    = 1;
        (*v)->data[0] = *key;
        return (*v)->data;
    }
    for (min = 0, max = (*v)->size - 1; max >= min;) { /* bsearch */
        mid = min + (max - min) / 2;
        cp = (*v)->data + mid;
        switch ((cp->c > key->c) - (cp->c < key->c)) {
            case  0: min = max + 1; break;
            case  1: max = mid - 1; break;
            case -1: min = mid + 1; break;
        }
    }
    if (cp->c != key->c) { /* insert */
        if ((*v)->size == (*v)->cap) {
            if (!(*v = realloc(*v, sizeof(**v) + sizeof(*(*v)->data) * (*v)->cap * 2)))
                err("realloc:");
            (*v)->cap *= 2;
            cp = (*v)->data + mid;
        }
        if (key->c > cp->c) {
            cp++;
            mid++;
        }
        memmove(cp + 1, cp, sizeof(*cp) * ((*v)->size - mid));
        *cp = *key;
        (*v)->size++;
    }
    return cp;
}

Vec *makebgt(FILE *f, wint_t (*to)(wint_t), int (*is)(wint_t))
{
    wint_t c;
    size_t nc = 0, nb = 0;
    Count *last = NULL;
    Vec   *bgt  = NULL;

    while ((c = fgetwc(f)) != WEOF) {
        if (to)
            c = to(c);

        if (is && !is(c)) {
            last = NULL;
            continue;
        }
        if (last) { /* increment bigram count */
            find(&last->v, &(Count){ .c = c })->n++;
            nb++;
        }
        /* increment character count */
        last = find(&bgt, &(Count){ .c = c });
        last->n++;
        nc++;
    }
    if (ferror(f))
        err("fgetwc:");
    if (!bgt)
        return NULL;

    /* calculate frequencies and probabilities */
    for (Count *p = bgt->data; p < bgt->data + bgt->size; p++) {
        if (p->v) {
            for (Count *q = p->v->data; q < p->v->data + p->v->size; q++) {
                q->p = (double)q->n / p->n;
                q->f = (double)q->n / nb;
            }
        }
        p->f = (double)p->n / nc;
    }
    return bgt;
}

void freebgt(Vec *bgt)
{
    for (Count *p = bgt->data; p < bgt->data + bgt->size; p++)
        free(p->v);
    free(bgt);
}

void print(Vec *bgt)
{
    for (Count *p = bgt->data; p < bgt->data + bgt->size; p++)
        if (p->v)
            for (Count *q = p->v->data; q < p->v->data + p->v->size; q++)
                printf("%lc%lc\t%f\t%f\t%zu\n", p->c, q->c, q->p, q->f, q->n);
}

int main(int argc, char **argv)
{
    Vec   *bgt;
    int    (*is)(wint_t) = NULL, c;
    wint_t (*to)(wint_t) = NULL;

    setlocale(LC_ALL, "");

    while ((c = getopt(argc, argv, ":lut:")) != -1) {
        switch (c) {
        case ':': err("Option -%c missing argument\n", optopt);
        case '?': err("Unrecognized option -%c\n", optopt);
        case 'l': to = towlower; break;
        case 'u': to = towupper; break;
        case 't':
            for (Test *t = tests; t->name; t++)
                if (!strcmp(optarg, t->name))
                    is = t->func;
            if (!is)
                err("Unrecognized test '%s'\n", optarg);
            break;
        }
    }
    bgt = makebgt(stdin, to, is);
    print(bgt);
    freebgt(bgt);
    return 0;
}
