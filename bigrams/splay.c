/*
 * create list of bigrams
 * use a splay tree to store bigrams and counts
 * read vector.c for more information
 */
#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <locale.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wchar.h>
#include <wctype.h>

typedef struct {
    char *name;
    int (*func)(wint_t);
} Test;

Test tests[] = {
    { "alnum" , iswalnum  },
    { "alpha" , iswalpha  },
    { "blank" , iswblank  },
    { "cntrl" , iswcntrl  },
    { "digit" , iswdigit  },
    { "graph" , iswgraph  },
    { "lower" , iswlower  },
    { "print" , iswprint  },
    { "punct" , iswpunct  },
    { "space" , iswspace  },
    { "upper" , iswupper  },
    { "xdigit", iswxdigit },

    { NULL, NULL }
};

typedef struct Node Node;
struct Node {
    Node   *p, *l, *r; /* parent, left, right    */
    double  fr, pr;    /* frequency, probability */
    wchar_t bg[2];     /* bigram                 */
    size_t  n;         /* number/count           */
};

void err(char *fmt, ...)
{
    int e = errno;
    va_list ap;

    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    if (fmt[strlen(fmt) - 1] == ':')
        fprintf(stderr, " %s\n", strerror(e));

    exit(1);
}

/* rotate on edge connecting n to parent */
void rotate(Node *n)
{
    Node *p = n->p, *g = p->p;

    if (g) { /* grandparent becomes parent, point to n as new child */
        if (g->l == p)
            g->l = n;
        else
            g->r = n;
    }
    if (p->l == n) { /* parent adopts child, becomes child */
        if ((p->l = n->r))
            p->l->p = p;
        n->r = p;
    } else {
        if ((p->r = n->l))
            p->r->p = p;
        n->l = p;
    }
    p->p = n; /* parent becomes child, point to n as new parent */
    n->p = g; /* grandparent becomes parent */
}

int cmp(const void *p, const void *q)
{
    const Node *a = p, *b = q;
    return 2*((a->bg[0] > b->bg[0]) - (a->bg[0] < b->bg[0])) + (a->bg[1] > b->bg[1]) - (a->bg[1] < b->bg[1]);
}

Node *find(Node *n, Node *key, int (*cmp)(const void *, const void *))
{
    int   r;
    Node *p = NULL;

    while (n && (r = cmp(key, n))) { /* find */
        p = n;
        if (r < 0)
            n = n->l;
        else
            n = n->r;
    }
    if (!n) { /* insert */
        if (!(n = malloc(sizeof(*n))))
            err("malloc:");

        *n = *key;
        n->l = n->r = NULL;
        if ((n->p = p)) {
            if (r < 0)
                p->l = n;
            else
                p->r = n;
        }
    }
    while (n->p) { /* splay */
        Node *p = n->p, *g = p->p;

        if (!g) { /* zig */
            rotate(n);
        } else if ((n == p->l && p == g->l) || (n == p->r && p == g->r)) { /* zig zig */
            rotate(p);
            rotate(n);
        } else { /* zig zag */
            rotate(n);
            rotate(n);
        }
    }
    return n;
}

void calc(Node *bgt, Node *ct, size_t nc, size_t nb)
{
    if (!bgt)
        return;
    ct = find(ct, &(Node){ .bg = { bgt->bg[0] } }, cmp);
    bgt->pr = (double)bgt->n / ct->n;
    bgt->fr = (double)bgt->n / nb;
    calc(bgt->l, ct, nc, nb);
    calc(bgt->r, ct, nc, nb);
}

void freebgt(Node *n)
{
    if (!n)
        return;
    freebgt(n->l);
    freebgt(n->r);
    free(n);
}

Node *makebgt(FILE *f, wint_t (*to)(wint_t), int (*is)(wint_t))
{
    wint_t c, last = WEOF;
    size_t nb = 0, nc = 0;
    Node  *bgt = NULL, *ct = NULL;

    while ((c = fgetwc(f)) != WEOF) {
        if (to)
            c = to(c);
        if (is && !is(c)) {
            last = WEOF;
            continue;
        }
        if (last != WEOF) { /* increment bigram count */
            bgt = find(bgt, &(Node){ .bg = { last, c }, .n = 0 }, cmp);
            bgt->n++;
            nb++;
        }
        /* increment character count */
        ct = find(ct, &(Node){ .bg = { c }, .n = 0 }, cmp);
        ct->n++;
        nc++;
        last = c;
    }
    if (ferror(f))
        err("fgetwc:");

    calc(bgt, ct, nc, nb);
    freebgt(ct);
    return bgt;
}

void print(Node *n)
{
    if (!n)
        return;
    print(n->l);
    printf("%lc%lc\t%f\t%f\t%zu\n", n->bg[0], n->bg[1], n->pr, n->fr, n->n);
    print(n->r);
}

int main(int argc, char **argv)
{
    Node   *bgt;
    int    (*is)(wint_t) = NULL, c;
    wint_t (*to)(wint_t) = NULL;

    setlocale(LC_ALL, "");

    while ((c = getopt(argc, argv, ":lut:")) != -1) {
        switch (c) {
        case ':': err("Option -%c missing argument\n", optopt);
        case '?': err("Unrecognized option -%c\n", optopt);
        case 'l': to = towlower; break;
        case 'u': to = towupper; break;
        case 't':
            for (Test *t = tests; t->name; t++)
                if (!strcmp(optarg, t->name))
                    is = t->func;
            if (!is)
                err("Unrecognized test '%s'\n", optarg);
            break;
        }
    }
    bgt = makebgt(stdin, to, is);
    print(bgt);
    freebgt(bgt);
    return 0;
}
