#include <stdio.h>

void foo(int *array, int *stride, int *coord)
{
    int (*p)[stride[0]][stride[1]][stride[2]] = array;
    printf("%d\n", (*p)[coord[0]][coord[1]][coord[2]]);
}

int main(void)
{
    int a[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
    int s[] = { 0, 0, 5 };
    int c[] = { 0, 2, 4 };

    foo(a, s, c);
    return 0;
}
