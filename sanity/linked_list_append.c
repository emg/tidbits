#include <stdio.h>
#include <stdlib.h>


typedef struct node_t Node;
struct node_t {
    Node *next;
    int   val;
};

void append(Node **head, Node *n)
{
    Node **p;
    for (p = head; *p; p = &(*p)->next)
        ;
    *p = n;
}

void print_all(Node *n)
{
    while (n) {
        printf("%d,",n->val);
        n = n->next;
    }
    printf("\n");
}

Node *make_node(int val)
{
    Node *n = calloc(1, sizeof(Node));
    n->val = val;
    return n;
}


int main(void)
{
    Node *head = NULL;

    print_all(head);

    append(&head, make_node(1));
    print_all(head);

    append(&head, make_node(2));
    print_all(head);

    append(&head, make_node(3));
    print_all(head);

    return 0;
}
