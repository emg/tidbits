#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

int main(int argc, char **argv)
{
	char *end;
	long base;
	long long n;

	if (argc != 3)
		return 1;

	errno = 0;
	base = strtol(argv[2], &end, 0);
	if (errno == EINVAL)
		return 2;
	if (*end)
		return 3;
	if (base < 0 || base > 36)
		return 4;

	n = strtoll(argv[1], &end, base);
	if (errno == EINVAL)
		return 5;
	if (errno == ERANGE)
		return 6;
	if (*end)
		return 7;

	printf("%lld\n", n);
	return 0;
}
