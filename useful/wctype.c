#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <wchar.h>
#include <wctype.h>

typedef struct {
    char *name;
    int (*func)(wint_t);
} Test;

Test tests[] = {
    { "alnum" , iswalnum  },
    { "alpha" , iswalpha  },
    { "blank" , iswblank  },
    { "cntrl" , iswcntrl  },
    { "digit" , iswdigit  },
    { "graph" , iswgraph  },
    { "lower" , iswlower  },
    { "print" , iswprint  },
    { "punct" , iswpunct  },
    { "space" , iswspace  },
    { "upper" , iswupper  },
    { "xdigit", iswxdigit },

    { NULL, NULL }
};

int main(int argc, char **argv)
{
    wchar_t wc;
    unsigned f = 0, n = 0;
    Test *t;

    setlocale(LC_ALL, "");

    /* wctype
     * print names of all tests */
    if (argc == 1) {
        for (t = tests; t->name; t++)
            puts(t->name);
        return 0;
    }

    for (t = tests; t->name; t++)
        if (!strcmp(argv[1], t->name))
            break;
    if (!t->name)
        return 2;

    /* wctype class character
     * check if character is part of class */
    if (argc == 3) {
        if (mbtowc(&wc, argv[2], strlen(argv[2])) <= 0)
            return 2;
        return !t->func(wc);
    }
    if (argc != 2)
        return 2;

    /* wctype class
     * print all character ranges that belong to class
     * FIXME: not checking WCHAR_MAX, but <= may cause rollover, and wint_t may
     *        be of different signedness */
    fprintf(stderr, "checking isw%s %d -> %d\n", argv[1], WCHAR_MIN, WCHAR_MAX);
    for (wc = WCHAR_MIN; wc < WCHAR_MAX; wc++) {
        if (t->func(wc)) {
            n++;
            if (!f++)
                printf("%lc(%d)", wc, wc);
        } else if (f) {
            if (f > 1)
                printf("-%lc(%d)\n", wc - 1, wc - 1);
            else
                printf("\n");
            f = 0;
        }
    }
    printf("\n%u %s\n", n, t->name);
    return 0;
}
