#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* from /usr/include/asm-generic/errno.h */
#define MAX_ERRNO 133

/* ignore further errors when printing error messages */
static void
printerr(char *s, long e)
{
    char *p;

    errno = 0;
    if (s) {
        e = strtol(s, &p, 0);
        if (errno || !*s || *p) {
            fprintf(stderr, "invalid error number `%s': %s\n", s, errno ? strerror(errno) : p);
            return;
        }
    }

    s = strerror(e);
    if (errno) {
        fprintf(stderr, "failure to retrieve error string for errno %d: %s\n", e, strerror(errno));
        return;
    }

    if (printf("%3d: %s\n", e, s) < 0)
        fprintf(stderr, "printf failed: %s\n", strerror(errno));
}

int
main(int argc, char **argv)
{
    long i;

    if (argc > 1)
        while (*++argv)
            printerr(*argv, 0);
    else
        for (i = 0; i <= MAX_ERRNO; i++)
            printerr(NULL, i);

    return 0;
}
