#!/usr/bin/awk -f
function swap(i, j,    tmp) {
    tmp = $i
    $i  = $j
    $j  = tmp
}
function perm(beg,    i) {
    if (beg > NF) {
        print
        return
    }
    for (i = beg; i <= NF; i++) {
        swap(beg, i)
        perm(beg + 1)
        swap(beg, i)
    }
}
{ perm(1) }
