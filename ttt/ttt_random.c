#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LEN(x) (sizeof(x) / sizeof(*x))
typedef enum { X = -1, E = 0, O = 1, C } P;
char glyphs[] = "X O";
P match[] = { ['X'] = X, [' '] = E, ['O'] = O };
P board[9];

int read_board(P *t)
{
    int c = getchar();
    if (c == EOF) return 1;
    *t = match[c];
    for (size_t i = 0; i < LEN(board) && (c = getchar()) != EOF; i++)
        board[i] = match[c];
    return c == EOF || getchar() != '\n';
}

int main(int argc, char **argv)
{
    P t, p = X;

    srand(time(NULL));

    if (argc > 1 && **++argv == 'O')
        p = O;

    for (;;) {
        int r;
        if (read_board(&t)) break;
        if (t != p) continue;
        do r = rand() % LEN(board);
        while (board[r] != E);
        fprintf(stdout, "%c%d\n", glyphs[p + 1], r);
        fflush(stdout);
    }
    return 0;
}
