#include <stdio.h>

#define LEN(x) (sizeof(x) / sizeof(*x))
typedef enum { X = -1, E = 0, O = 1, C } P;
char glyphs[] = "X O";
P match[] = { ['X'] = X, [' '] = E, ['O'] = O };
int rows[][2] = { // start-increment pairs to check for wins
    { 0, 1 }, { 0, 3 }, { 0, 4 }, { 1, 3 }, { 2, 2 }, { 2, 3 }, { 3, 1 }, { 6, 1 },
};
P board[9];

int read_board(P *t)
{
    int c = getchar();
    if (c == EOF) return 1;
    *t = match[c];
    for (size_t i = 0; i < LEN(board) && (c = getchar()) != EOF; i++)
        board[i] = match[c];
    return c == EOF || getchar() != '\n';
}

P whose_row(int beg, int inc)
{
    return (board[beg] + board[beg + inc] + board[beg + 2 * inc]) / 3;
}

P state(void)
{
    P w;
    for (int i = LEN(rows); i--;)
        if ((w = whose_row(rows[i][0], rows[i][1])) != E)
            return w; // we have a winner
    for (int i = LEN(board); i--;)
        if (board[i] == E)
            return E; // still empty spaces
    return C;         // cat's game
}

int negamax(P turn, int depth, int a, int b)
{
    int s = -1, m = 0, t;
    P   w = state();

    if (w != E)
        return w == C ? 0 : w * turn;

    for (int i = LEN(board); i--;) {
        if (board[i] != E) continue;
        board[i] = turn;
        if ((t = -negamax(-turn, depth + 1, -b, -a)) > s) {
            s = t;
            m = i;
            if (t > a) a = t;
        }
        board[i] = E;
        if (a > b) break;
    }
    if (depth == 0)
        fprintf(stdout, "%c%d\n", glyphs[turn + 1], m);
    return s;
}

int main(int argc, char **argv)
{
    P t, p = X;

    if (argc > 1 && **++argv == 'O')
        p = O;

    for (;;) {
        if (read_board(&t)) break;
        if (t != p) continue;
        negamax(p, 0, -1, 1);
        fflush(stdout);
    }
    return 0;
}
