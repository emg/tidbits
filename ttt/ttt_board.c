#include <stdio.h>
#include <string.h>

#define LEN(x) (sizeof(x) / sizeof(*x))
typedef enum { X = -1, E = 0, O = 1, C } P;
char glyphs[] = "X O";
P match[] = { ['X'] = X, ['O'] = O };
int rows[][2] = { // start-increment pairs to check for wins
    { 0, 1 }, { 0, 3 }, { 0, 4 }, { 1, 3 }, { 2, 2 }, { 2, 3 }, { 3, 1 }, { 6, 1 },
};
P board[9], turn;

void pretty_print_board(void)
{
    for (size_t i = 0; i < LEN(board); i++)
        fprintf(stderr, " %c %s", glyphs[board[i] + 1],
                                  i + 1 == LEN(board) ? "\n\n" :
                                  i % 3 != 2          ? "|"    : "\n---+---+---\n");
}

void print_board(void)
{
    putc(glyphs[turn + 1], stdout);
    for (size_t i = 0; i < LEN(board); i++)
        putc(glyphs[board[i] + 1], stdout);
    putc('\n', stdout);
    fflush(stdout);
}

int read_move(void)
{
    int p, m, n;
    p = getchar();
    m = getchar();
    n = getchar();
    if (p == EOF || m == EOF || n == EOF)                 { fprintf(stderr, "read EOF\n");                 return 0; }
    if (p != 'O' && p != 'X')                             { fprintf(stderr, "invalid piece %c\n", p);      return 0; }
    if (match[p] != turn)                                 { fprintf(stderr, "out of turn %c\n", p);        return 0; }
    m -= '0';
    if (m < 0 || (size_t)m > LEN(board) || board[m] != E) { fprintf(stderr, "invalid move %c\n", m + '0'); return 0; }
    if (n != '\n')                                        { fprintf(stderr, "extra characters\n");         return 0; }
    board[m] = turn;
    return 1;
}

P whose_row(int beg, int inc)
{
    return (board[beg] + board[beg + inc] + board[beg + 2 * inc]) / 3;
}

P state(void)
{
    P w;
    for (int i = LEN(rows); i--;)
        if ((w = whose_row(rows[i][0], rows[i][1])) != E)
            return w; // we have a winner
    for (int i = LEN(board); i--;)
        if (board[i] == E)
            return E; // still empty spaces
    return C;         // cat's game
}

int main(void)
{
    for (int i = 1000; i--;) {
        memset(board, 0, sizeof(board));
        turn = X;

        do {
            print_board();
            //pretty_print_board();
            if (!read_move()) break;
            turn = -turn;
        } while (state() == E);
        //pretty_print_board();

        switch (state()) {
            case X: fputs("X wins\n"    , stderr); break;
            case O: fputs("O wins\n"    , stderr); break;
            case C: fputs("Cat's game\n", stderr); break;
            case E: fputs("Failure\n "  , stderr); return 0;
        }
    }
    return 0;
}
