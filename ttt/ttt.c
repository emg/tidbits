#include <stdio.h>

#define LEN(x) (sizeof(x) / sizeof(*x))
typedef enum { X = -1, E = 0, O = 1, C } P;
char glyphs[] = "X O";
char *outcomes[] = { "X wins", "", "O wins", "Cat's game" };
int rows[][2] = { // start-increment pairs to check for wins
    { 0, 1 }, { 0, 3 }, { 0, 4 }, { 1, 3 }, { 2, 2 }, { 2, 3 }, { 3, 1 }, { 6, 1 },
};
P board[9];

void print_board(void)
{
    for (size_t i = 0; i < LEN(board); i++)
        printf(" %c %s", board[i] == E ? '0' + (int)i : glyphs[board[i] + 1],
                         i + 1 == LEN(board) ? "\n\n" :
                         i % 3 != 2          ? "|"    : "\n---+---+---\n");
}

P whose_row(int beg, int inc)
{
    return (board[beg] + board[beg + inc] + board[beg + 2 * inc]) / 3;
}

P state(void)
{
    P w;
    for (int i = LEN(rows); i--;)
        if ((w = whose_row(rows[i][0], rows[i][1])) != E)
            return w; // we have a winner
    for (int i = LEN(board); i--;)
        if (board[i] == E)
            return E; // still empty spaces
    return C;         // cat's game
}

int negamax(P turn, int depth, int a, int b)
{
    int s = -1, m = 0, t;
    P   w = state();

    if (w != E)
        return w == C ? 0 : w * turn;

    for (int i = LEN(board); i--;) {
        if (board[i] != E) continue;
        board[i] = turn;
        if ((t = -negamax(-turn, depth + 1, -b, -a)) > s) {
            s = t;
            m = i;
            if (t > a) a = t;
        }
        board[i] = E;
        if (a > b) break;
    }
    if (depth == 0)
        board[m] = turn;
    return s;
}

int main(int argc, char **argv)
{
    P p = X;

    if (argc > 1 && **++argv == 'O') {
        p = O;
        negamax(-p, 0, -1, 1);
    }
    print_board();

    do {
        size_t m;
        if (scanf("%zu", &m) < 1 || m >= LEN(board) || board[m] != E) {
            puts("Illegal move. Try again.");
            continue;
        }
        board[m] = p;
        print_board();
        negamax(-p, 0, -1, 1);
        print_board();
    } while (state() == E);

    puts(outcomes[state() + 1]);
    return 0;
}
