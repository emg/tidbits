#include <stdio.h>
void swap(char **p, char **q) { char *t = *p; *p = *q; *q = t; }
void perm(char **s, char **p) {
	if (*p) for (char **q = p; *q; q++) swap(p, q), perm(s, p + 1), swap(p, q);
	else    for (char **q = s; *q; q++) printf("%s%c", *q, q[1] ? ' ' : '\n'); }
int main(int argc, char **argv) { perm(argv + 1, argv + 1); return 0; }
