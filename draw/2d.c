#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <linux/fb.h>

#define MAX(a, b) ((a) > (b) ?  (a) : (b))
#define MIN(a, b) ((a) < (b) ?  (a) : (b))
#define ABS(a)    ((a) <  0  ? -(a) : (a))

typedef union {
    uint32_t val;
    struct {
        uint8_t b, g, r, t;
    };
} Pixel;

typedef struct {
    Pixel *buf;
    int xres, yres;
} Buffer;

typedef struct {
    int x, y;
} Point;

void push(void *fbm, Buffer buf)
{
    memcpy(fbm, buf.buf, buf.xres * buf.yres * sizeof(*buf.buf));
}

void draw_point(Buffer buf, Point p, Pixel color)
{
    if (p.y < 0 || p.y > buf.yres ||
        p.x < 0 || p.x > buf.xres)
        return;
    *(buf.buf + p.y * buf.xres + p.x) = color;
}

int Point_eq(Point a, Point b)
{
    return a.x == b.x && a.y == b.y;
}

void draw_line(Buffer buf, Point a, Point b, Pixel color)
{
    Point d = { ABS(b.x - a.x)      , ABS(b.y - a.y)       };
    Point s = { (a.x < b.x ? 1 : -1), (a.y < b.y ? 1 : -1) };
    int err = d.x - d.y;
    int e2;

    for (;;) {
        draw_point(buf, a, color);
        if (Point_eq(a, b)) break;
        e2 = 2 * err;
        if (e2 > -d.y) { err -= d.y; a.x += s.x; }
        if (e2 <  d.x) { err += d.x; a.y += s.y; }
    }
}

int main(void)
{
    int fd;
    uint8_t *fbm;
    struct fb_var_screeninfo vi;
    struct fb_fix_screeninfo fi;
    Buffer buf;

    if ((fd = open("/dev/fb0", O_RDWR))     < 0) return 1;
    if (ioctl(fd, FBIOGET_FSCREENINFO, &fi) < 0) return 2;
    if (ioctl(fd, FBIOGET_VSCREENINFO, &vi) < 0) return 3;
    if (vi.bits_per_pixel != 32)                 return 4;
    if ((fbm = mmap(NULL, vi.xres * vi.yres * sizeof(Pixel), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) == MAP_FAILED) return 5;

    buf.xres = vi.xres;
    buf.yres = vi.yres;
    buf.buf  = calloc(sizeof(Pixel), buf.xres * buf.yres);

    draw_line(buf, (Point){ buf.xres, buf.yres }, (Point){ 0, 0 }, (Pixel){ 0xffffff0 });
    push(fbm, buf);

    munmap(fbm, sizeof(buf));
    close(fd);

    return 0;
}
