abs() {
    local _d=$1
    printf %d $((_d >= 0 ? _d : -_d))
}

put_point() {
    local _canvas _xres _yres
    local _x _y _i _c
    _canvas=$1 _x=$2 _y=$3
    _c=$4
    ((_xres = $_canvas[0], _yres = $_canvas[1]))
    ((_x < 0 || _x >= _xres || _y < 0 || _y >= _yres)) && return
    ((_i = _y * _xres + _x))
    printf -v "$_canvas[_i + 2]" %d "$_c"
}

draw_line() {
    echo draw_line >&2
    local _canvas _xres _yres _c
    local _x1 _y1 _x2 _y2 _dx _dy _sx _sy _e1 _e2
    _canvas=$1
    ((_xres = $_canvas[0], _yres = $_canvas[1]))
    _x1=$2 _y1=$3
    _x2=$4 _y2=$5
    _c=$6

    ((_dx = $(abs $((_x2 - _x1))),
      _dy = $(abs $((_y2 - _y1))),
      _sx = _x1 < _x2 ? 1 : -1   ,
      _sy = _y1 < _y2 ? 1 : -1   ,
      _e1 = _dx - _dy            ))

    while :; do
        put_point "$_canvas" "$_x1" "$_y1" "$_c"
        ((_x1 == _x2 && _y1 == _y2)) && break
        ((_e2 = 2 * _e1))
        ((_e1 > -_dy && (_e1 -= _dy, _x1 += _sx)))
        ((_e2 <  _dx && (_e1 += _dx, _y1 += _sy)))
    done
}

draw_circle() {
    echo draw_circle >&2
    local _canvas _xres _yres _c
    local _x _y _r _x0 _y0 _e
    _canvas=$1 _x0=$2 _y0=$3 _r=$4 _c=$5
    ((_x = _r, _y = 0, _e = 1 - _x))

    while ((_x >= _y)); do
        put_point "$_canvas" $((_x0 + _x)) $((_y0 + _y)) "$_c"
        put_point "$_canvas" $((_x0 - _x)) $((_y0 + _y)) "$_c"
        put_point "$_canvas" $((_x0 + _x)) $((_y0 - _y)) "$_c"
        put_point "$_canvas" $((_x0 - _x)) $((_y0 - _y)) "$_c"
        put_point "$_canvas" $((_x0 + _y)) $((_y0 + _x)) "$_c"
        put_point "$_canvas" $((_x0 - _y)) $((_y0 + _x)) "$_c"
        put_point "$_canvas" $((_x0 + _y)) $((_y0 - _x)) "$_c"
        put_point "$_canvas" $((_x0 - _y)) $((_y0 - _x)) "$_c"
        ((_y++))
        if ((_e < 0)); then
            ((_e += 2 * _y + 1));
        else
            ((--_x, _e += 2 * (_y - _x + 1)))
        fi
    done
}

draw_canvas() {
    echo draw_canvas >&2
    local _canvas _xres _yres _i _c
    _canvas=$1
    ((_xres = $_canvas[0], _yres = $_canvas[1]))

    printf "P3\n%d %d\n255\n" "$_xres" "$_yres"

    for ((_i = 0; _i < _xres * _yres; _i++)); do
        ((_c = $_canvas[_i + 2]))
        printf '%d ' $((_c >> 16 & 0xff)) $((_c >> 8 & 0xff)) $((_c & 0xff))
    done
}

make_canvas() {
    echo make_canvas >&2
    local _canvas _xres _yres _c
    _canvas=$1 _xres=$2 _yres=$3 _c=$4

    (($_canvas[0] = _xres, $_canvas[1] = _yres))
    for ((_i = 0; _i < _xres * _yres; _i++)); do
        printf -v "$_canvas[_i + 2]" %d "$_c"
    done
}

example() {
    local canvas
    #                                    rrggbb
    make_canvas canvas 500 500         0x555555
    draw_line   canvas   0   0 499 499 0xffffff
    draw_line   canvas   0 200 499 200 0xff0000
    draw_line   canvas 450  50 120 400 0x00ff00
    draw_line   canvas 300 490 300   0 0x0000ff
    draw_line   canvas  50 450 200  70 0xffff00
    draw_circle canvas 250 250 150     0xff00ff
    draw_circle canvas   0   0 250     0x00ffff

    draw_canvas canvas > example.ppm
    display example.ppm
    rm example.ppm
}
